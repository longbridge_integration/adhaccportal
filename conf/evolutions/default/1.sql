# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table alert (
  id                        bigint auto_increment not null,
  alert_type                integer,
  message                   TEXT,
  status                    varchar(7),
  status_message            varchar(255),
  status_time               datetime(6),
  created_on                datetime(6),
  receiver                  varchar(255),
  receiver_name             varchar(255),
  subject                   varchar(255),
  last_update               datetime(6) not null,
  constraint ck_alert_alert_type check (alert_type in (0,1)),
  constraint ck_alert_status check (status in ('PENDING','SENDING','SENT','FAILED')),
  constraint pk_alert primary key (id))
;

create table app_property (
  id                        bigint auto_increment not null,
  prop_key                  varchar(255),
  prop_value                varchar(255),
  last_update               datetime(6) not null,
  constraint pk_app_property primary key (id))
;

create table login_audit_trail (
  id                        bigint auto_increment not null,
  event_type                varchar(16),
  event_date                datetime(6),
  ip_address                varchar(255),
  username                  varchar(255),
  user_id                   bigint,
  constraint ck_login_audit_trail_event_type check (event_type in ('LOGIN_OK','USER_NOT_FOUND','USER_NOT_ENABLED','LOGOUT_OK')),
  constraint pk_login_audit_trail primary key (id))
;

create table message_template (
  id                        bigint auto_increment not null,
  title                     varchar(255),
  code                      varchar(255),
  message_text              TEXT,
  arguments                 TEXT,
  has_attachment            tinyint(1) default 0,
  subject                   varchar(255),
  alert_type                integer,
  constraint ck_message_template_alert_type check (alert_type in (0,1)),
  constraint uq_message_template_title unique (title),
  constraint uq_message_template_code unique (code),
  constraint pk_message_template primary key (id))
;

create table security_question (
  id                        bigint auto_increment not null,
  question_text             varchar(255),
  security_question_category varchar(5),
  constraint ck_security_question_security_question_category check (security_question_category in ('ONE','TWO','THREE')),
  constraint uq_security_question_question_text unique (question_text),
  constraint pk_security_question primary key (id))
;

create table term_deposit_scheme (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  title                     varchar(255),
  scheme_description        TEXT,
  scheme_code               varchar(255),
  template_name             varchar(14),
  rate_type                 varchar(24),
  conclusion_text           TEXT,
  intro_text                TEXT,
  purpose_text              TEXT,
  file_name_prefix          varchar(255),
  modified_by_id            bigint,
  constraint ck_term_deposit_scheme_template_name check (template_name in ('BOND_NOTES','TREASURY_NOTES','CALL_DEPOSIT','GOVERNMENT')),
  constraint ck_term_deposit_scheme_rate_type check (rate_type in ('BACKEND_PRODUCT','UPFRONT_INTEREST_PRODUCT')),
  constraint uq_term_deposit_scheme_scheme_code unique (scheme_code),
  constraint pk_term_deposit_scheme primary key (id))
;

create table user_table (
  id                        bigint auto_increment not null,
  username                  varchar(255),
  customer_id               varchar(255),
  fullname                  varchar(255),
  email                     varchar(255),
  password_hash             varchar(255),
  is_enabled                tinyint(1) default 0,
  is_first_login            tinyint(1) default 0,
  role                      varchar(8),
  last_successful_login     datetime(6),
  last_unsuccessful_attempt datetime(6),
  security_answer_one       varchar(255),
  security_answer_two       varchar(255),
  security_answer_three     varchar(255),
  security_question_one     varchar(255),
  security_question_two     varchar(255),
  security_question_three   varchar(255),
  constraint ck_user_table_role check (role in ('STANDARD','ADMIN')),
  constraint uq_user_table_username unique (username),
  constraint pk_user_table primary key (id))
;

create table user_history (
  id                        bigint auto_increment not null,
  user_id                   bigint,
  initiator_id              bigint,
  event_date                datetime(6),
  event                     integer,
  constraint ck_user_history_event check (event in (0,1,2,3,4,5)),
  constraint pk_user_history primary key (id))
;

alter table login_audit_trail add constraint fk_login_audit_trail_user_1 foreign key (user_id) references user_table (id) on delete restrict on update restrict;
create index ix_login_audit_trail_user_1 on login_audit_trail (user_id);
alter table term_deposit_scheme add constraint fk_term_deposit_scheme_modifiedBy_2 foreign key (modified_by_id) references user_table (id) on delete restrict on update restrict;
create index ix_term_deposit_scheme_modifiedBy_2 on term_deposit_scheme (modified_by_id);
alter table user_history add constraint fk_user_history_user_3 foreign key (user_id) references user_table (id) on delete restrict on update restrict;
create index ix_user_history_user_3 on user_history (user_id);
alter table user_history add constraint fk_user_history_initiator_4 foreign key (initiator_id) references user_table (id) on delete restrict on update restrict;
create index ix_user_history_initiator_4 on user_history (initiator_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table alert;

drop table app_property;

drop table login_audit_trail;

drop table message_template;

drop table security_question;

drop table term_deposit_scheme;

drop table user_table;

drop table user_history;

SET FOREIGN_KEY_CHECKS=1;

