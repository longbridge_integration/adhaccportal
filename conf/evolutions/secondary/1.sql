# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table TBAADM.GAM (
  FORACID                   varchar2(255),
  ACID                      varchar2(255),
  ACCT_NAME                 varchar2(255),
  CIF_ID                    varchar2(255),
  CLR_BAL_AMT               varchar2(255),
  SCHM_TYPE                 varchar2(255),
  SCHM_CODE                 varchar2(255),
  CRNCY_CODE                varchar2(255),
  ACCT_CRNCY_CODE           varchar2(255))
;

create table CRMUSER.ACCOUNTS (
  ORGKEY                    varchar2(255),
  CUST_LAST_NAME            varchar2(255),
  CUST_FIRST_NAME           varchar2(255),
  CUST_MIDDLE_NAME          varchar2(255),
  PREFERREDNAME             varchar2(255))
;

create table CRMUSER.PHONEEMAIL (
  ORGKEY                    varchar2(255),
  PHONENO                   varchar2(255),
  EMAIL                     varchar2(255),
  PHONEOREMAIL              varchar2(255),
  PREFERREDFLAG             varchar2(255))
;

create table TBAADM.DTD (
  TRAN_ID                   varchar2(255),
  TRAN_DATE                 timestamp,
  RCRE_TIME                 timestamp,
  ACID                      varchar2(255),
  CUST_ID                   varchar2(255),
  PART_TRAN_TYPE            varchar2(255),
  TRAN_AMT                  varchar2(255),
  TRAN_PARTICULAR           varchar2(255),
  PSTD_FLG                  varchar2(255),
  PSTD_DATE                 timestamp,
  TRAN_TYPE                 varchar2(255),
  TRAN_SUB_TYPE             varchar2(255))
;

create table TBAADM.HTD (
  ACID                      varchar2(255),
  PSTD_FLG                  varchar2(255),
  CUST_ID                   varchar2(255),
  TRAN_PARTICULAR           varchar2(255),
  PSTD_DATE                 timestamp,
  TRAN_AMT                  varchar2(255),
  PART_TRAN_TYPE            varchar2(255),
  REF_CRNCY_CODE            varchar2(255))
;

create table TBAADM.DTD (
  ACID                      varchar2(255),
  PSTD_FLG                  varchar2(255),
  CUST_ID                   varchar2(255),
  TRAN_PARTICULAR           varchar2(255),
  PSTD_DATE                 timestamp,
  TRAN_AMT                  varchar2(255),
  PART_TRAN_TYPE            varchar2(255),
  REF_CRNCY_CODE            varchar2(255))
;




# --- !Downs

drop table TBAADM.GAM cascade constraints purge;

drop table CRMUSER.ACCOUNTS cascade constraints purge;

drop table CRMUSER.PHONEEMAIL cascade constraints purge;

drop table TBAADM.DTD cascade constraints purge;

drop table TBAADM.HTD cascade constraints purge;

drop table TBAADM.DTD cascade constraints purge;

