import external.CustomerContact;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.junit.Test;
import sharedfunctions.TimeUtil;
import utils.Hash;
import utils.NumberToWords;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import static org.junit.Assert.*;


/**
*
* Simple (JUnit) tests that can call all parts of a play app.
* If you are interested in mocking a whole application, see the wiki for more details.
*
*/
public class ApplicationTest{

    @Test
    public void testFormat(){
        int count = 1;
        String format = String.format("%03d",count);
        String sql = "Freestyle" + format;
        System.out.println(sql);
        assertEquals(3, format.length());
    }
    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertEquals(2, a);
    }

    @Test
    public void getSQL(){
        final String rateSQL = "  select distinct \n" +
                "(CUST_CR_PREF_PCNT + ID_CR_PREF_PCNT + NRML_PCNT_CR + CHNL_CR_PREF_PCNT+ BASE_PCNT_CR) from tbaadm.itc a, (select entity_id, max(INT_TBL_CODE_SRL_NUM) itc_max_INT_TBL_CODE_SRL_NUM from tbaadm.itc\n" +
                "where entity_cre_flg = 'Y' and del_flg = 'N'\n" +
                "group by entity_id) b, tbaadm.icv c,\n" +
                "(select int_tbl_code, crncy_code, max(INT_TBL_VER_NUM) icv_max_INT_TBL_VER_NUM from tbaadm.icv\n" +
                "where entity_cre_flg = 'Y' and del_flg = 'N'\n" +
                "group by int_tbl_code, crncy_code) d, tbaadm.tvs e,\n" +
                "(select int_tbl_code, INT_SLAB_DR_CR_FLG, crncy_code, max(INT_TBL_VER_NUM) ivs_max_INT_TBL_VER_NUM from tbaadm.tvs\n" +
                "where entity_cre_flg = 'Y' and del_flg = 'N'\n" +
                "group by int_tbl_code, INT_SLAB_DR_CR_FLG, crncy_code) f, tbaadm.gam g\n" +
                "where a.entity_id = b.entity_id\n" +
                "and a.entity_id = g.acid\n" +
                "and a.INT_TBL_CODE_SRL_NUM = b.itc_max_INT_TBL_CODE_SRL_NUM\n" +
                "and a.int_tbl_code = c.int_tbl_code\n" +
                "and c.INT_TBL_VER_NUM = d.icv_max_INT_TBL_VER_NUM\n" +
                "and c.crncy_code = e.crncy_code\n" +
                "and c.int_tbl_code = e.int_tbl_code\n" +
                "and e.INT_TBL_VER_NUM = f.ivs_max_INT_TBL_VER_NUM\n" +
                "and e.INT_SLAB_DR_CR_FLG = f.INT_SLAB_DR_CR_FLG\n" +
                "and e.int_tbl_code = f.int_tbl_code\n" +
                "and e.crncy_code = f.crncy_code\n" +
                "and foracid = ?\n" +
                "and e.INT_SLAB_DR_CR_FLG = 'C'";
        final String fetchSqlSingle = "SELECT * FROM (select t.deposit_period_mths, t.deposit_period_days, t.open_effective_date, t.maturity_date, t.deposit_amount," +
                "            t.tam_crncy_code,t.maturity_amount,I.ID_CR_PREF_PCNT, g.rcre_time, g.cif_id, g.foracid,g.acct_name, g.acid, " +
                "            d.tran_id, g.schm_code, t.lchg_time from tbaadm.tam t, tbaadm.eit e, tbaadm.gam g, tbaadm.dtd d, tbaadm.itc i" +
                " where t.acid=e.entity_id and g.acid=t.acid     " +
                "                and g.acid=e.entity_id and g.acid=i.entity_id and g.clr_bal_amt > 0 and g.entity_cre_flg!='N' " +
                "and to_date(substr(g.rcre_time,1,10), 'dd-mm-yy') = to_date(sysdate, 'dd-mm-yy') and g.acid=d.acid and" +
                " d.tran_particular like 'Tran. For Principal%' and g.foracid = ? ORDER BY t.lchg_time DESC ) " +
                "WHERE ROWNUM <= ? order by lchg_time asc  ";

        System.out.println(rateSQL);
        System.out.println(fetchSqlSingle);
    }

//    @Test
//    public void renderTemplate() {
//        Content html = views.html.index.render("Your new application is ready.");
//        assertEquals("text/html", contentType(html));
//        assertTrue(contentAsString(html).contains("Your new application is ready."));
//    }



    @Test
    public void testFee(){

        String [] contents =StringUtils.split("wale.afolabi@gmail.com", ".@");

        System.out.println(contents);
    }


    @Test
    public  void test(){

        BigDecimal amount =  new BigDecimal(4000);

        System.out.println("" + amount);
    }

    @Test
    public void testFormatBigDecimal(){
        System.out.printf("number: %,.2f%n", new BigDecimal("234234.24662344").setScale(2, RoundingMode.UP));
    }

    @Test
    public void testGetAllCustomers(){
        Set<CustomerContact> emails = CustomerContact.getAllCustomerEmails();
        Set<CustomerContact> phones = CustomerContact.getAllCustomerPhones();

        play.Logger.info(emails.size()+" the size of emails");
        play.Logger.info(phones.size() + " the size of phones");
    }

    @Test
    public void testCreateUser(){
//        UserForm data =  userForm.get();


        String passwordHash = null;
        try {
            passwordHash = Hash.createPassword("password123");
            play.Logger.info("password123 =  " + passwordHash);
            assertNotNull(passwordHash);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        return;// redirect(routes.UserController.index());
    }

    @Test
    public void testNumberToWords(){
        System.out.println(NumberToWords.noToWords(3));
        assertTrue(NumberToWords.noToWords(3).equals("Three"));
        System.out.println(NumberToWords.noToWords(65000000));
        assertTrue(NumberToWords.noToWords(65000000).trim().equals("Sixty Five Million"));
        System.out.println(NumberToWords.noToWords(23500));
        assertTrue(NumberToWords.noToWords(23500).trim().equals("Twenty Three Thousand Five Hundred"));
        System.out.println(NumberToWords.noToWords(34567));
        assertTrue(NumberToWords.noToWords(34567).trim().equals("Thirty Four Thousand Five Hundred and Sixty Seven"));
        System.out.println(NumberToWords.noToWords(new BigDecimal(32432.23)));
        System.out.println(NumberToWords.noToWords(5678));
        assertTrue(NumberToWords.noToWords(5678).trim().equals("Five Thousand Six Hundred and Seventy Eight"));
        System.out.println(NumberToWords.noToWords(234));
        assertTrue(NumberToWords.noToWords(234).trim().equals("Two Hundred and Thirty Four"));
        System.out.println(NumberToWords.noToWords(22));

    }

    @Test
    public void testRelativeTime(){
        TimeUtil timeUtil = TimeUtil.getInstance();
        //this should return now
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.printf("Expected: %s, Got: %s%n", "now", timeUtil.getRelativeTimeString(dateTime));
        assertTrue(timeUtil.getRelativeTimeString(dateTime).contains("now"));

        dateTime = LocalDateTime.now().minusMinutes(30);
        System.out.printf("Expected: %s, Got: %s%n", "30 minutes ago", timeUtil.getRelativeTimeString(dateTime));
        assertTrue(timeUtil.getRelativeTimeString(dateTime).contains("30 minute"));

        dateTime = LocalDateTime.now().minusMinutes(1);
        System.out.printf("Expected: %s, Got: %s%n", "1 minute ago", timeUtil.getRelativeTimeString(dateTime));
        assertTrue(timeUtil.getRelativeTimeString(dateTime).contains("1 minute"));

        dateTime = LocalDateTime.now().minusHours(1);
        System.out.printf("Expected: %s, Got: %s%n", "1 hour ago", timeUtil.getRelativeTimeString(dateTime));
        assertTrue(timeUtil.getRelativeTimeString(dateTime).contains("1 hour"));

        dateTime = LocalDateTime.now().minusDays(1);
        System.out.printf("Expected: %s, Got: %s%n", "Yesterday", timeUtil.getRelativeTimeString(dateTime));
        assertTrue(timeUtil.getRelativeTimeString(dateTime).contains("yesterday"));

        dateTime = LocalDateTime.now().minusMonths(5);
        System.out.printf("Expected: %s, Got: %s%n", "Jun 4", timeUtil.getRelativeTimeString(dateTime));
        assertTrue(timeUtil.getRelativeTimeString(dateTime).contains("Jun 4"));

        dateTime = LocalDateTime.now().minusYears(4);
        System.out.printf("Expected: %s, Got: %s%n", "Nov 04 2012", timeUtil.getRelativeTimeString(dateTime));
        assertTrue(timeUtil.getRelativeTimeString(dateTime).contains("Nov 04 2012"));

        /////////////
         dateTime = LocalDateTime.now();
        System.out.printf("Expected: %s, Got: %s%n", "now", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME));

        dateTime = LocalDateTime.now().minusMinutes(30);
        System.out.printf("Expected: %s, Got: %s%n", "30 minutes ago", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME));

        dateTime = LocalDateTime.now().minusMinutes(1);
        System.out.printf("Expected: %s, Got: %s%n", "1 minute ago", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME));

        dateTime = LocalDateTime.now().minusHours(1);
        System.out.printf("Expected: %s, Got: %s%n", "1 hour ago", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME));


        dateTime = LocalDateTime.now().minusDays(1);
        System.out.printf("Expected: %s, Got: %s%n", "Yesterday", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME));

        dateTime = dateTime.minusMonths(5);
        System.out.printf("Expected: %s, Got: %s%n", "03-06", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME));


         /////////////
         dateTime = LocalDateTime.now();
        System.out.printf("Expected: %s, Got: %s%n", "now", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME_DETAILED));

        dateTime = LocalDateTime.now().minusMinutes(30);
        System.out.printf("Expected: %s, Got: %s%n", "30 minutes ago", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME_DETAILED));

        dateTime = LocalDateTime.now().minusMinutes(1);
        System.out.printf("Expected: %s, Got: %s%n", "1 minute ago", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME_DETAILED));

        dateTime = LocalDateTime.now().minusHours(1);
        System.out.printf("Expected: %s, Got: %s%n", "1 hour ago", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME_DETAILED));


        dateTime = LocalDateTime.now().minusDays(1);
        System.out.printf("Expected: %s, Got: %s%n", "Yesterday", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME_DETAILED));

        dateTime = LocalDateTime.now().minusMonths(5);
        System.out.printf("Expected: %s, Got: %s%n", "03-06", timeUtil.getRelativeTimeString(dateTime, TimeUtil.TimeDetail.DATE_AND_TIME_DETAILED));

    }

//   public void testEmailAttachment(){
//        // Create the attachment
//        EmailAttachment attachment = new EmailAttachment();
//        PdfGenerator pdfGenerator = new PdfGenerator();
//        TermDeposit termDeposit = new TermDeposit();
//        termDeposit.accountId = "sfd";
//        termDeposit.fundingAccountName = "Trench";
//        termDeposit.openEffectiveDate = new Date();
//        termDeposit.maturityDate = new Date();
//        FileOutputStream outputFile = null;
//
//        try{
//            outputFile = new FileOutputStream("./testfile.pdf");
//        }catch(FileNotFoundException exc){
//            exc.printStackTrace();
//            fail();
//        }
//        byte[] pdfFileBytes =  pdfGenerator.toBytes(views.html.emailalert.cpdf3.render(termDeposit), "http://localhost:9000");
//        try {
//            outputFile.write(pdfFileBytes);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                outputFile.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        attachment.setPath("./testFile.pdf");
//        attachment.setDisposition(EmailAttachment.ATTACHMENT);
//        attachment.setDescription("TD Certificate");
//        attachment.setName("Certificate");
//        // Create the email message
//        MultiPartEmail email = new MultiPartEmail();
//        email.setHostName("smtp.googlemail.com");
//        try {
//            email.addTo("nnasino2008@live.com", "Chigozirim Torti");
//            email.setFrom("tortichigozirim@gmail.com", "Alert system test");
//            email.setSubject("The Certificate");
//            email.setMsg("Here is the certificate of your TD");
//            email.setSmtpPort(465);
//            email.setAuthenticator(new DefaultAuthenticator("tortichigozirim@gmail.com", "Ch1goz1r1m"));
//            email.setSSLOnConnect(true);
//            email.attach(attachment);
//            // send the email
//            email.send();
//        } catch (EmailException e) {
//            e.printStackTrace();
//        }
//        // add the attachment
//
//    }
    @Test
    public void testDateFormat(){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
        System.out.println(df.format(new Date()));
    }
}
