-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "coronation-alerts" ---------------------
CREATE DATABASE IF NOT EXISTS `coronation-alerts` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `coronation-alerts`;
-- ---------------------------------------------------------


-- CREATE TABLE "alert_permission" -------------------------
CREATE TABLE `alert_permission` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`customer_id` VarChar( 255 ) NULL,
	`account_number` VarChar( 255 ) NULL,
	`account_id` VarChar( 255 ) NULL,
	`allow_sms` TinyInt( 1 ) NULL DEFAULT '0',
	`allow_email` TinyInt( 1 ) NULL DEFAULT '0',
	`customer_name` VarChar( 255 ) NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 39;
-- ---------------------------------------------------------


-- CREATE TABLE "alert_transaction" ------------------------
CREATE TABLE `alert_transaction` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`TRAN_ID` VarChar( 255 ) NULL,
	`TRAN_DATE` DateTime NULL,
	`ACID` VarChar( 255 ) NULL,
	`FORACID` VarChar( 255 ) NULL,
	`CUST_ID` VarChar( 255 ) NULL,
	`CUST_EMAIL` VarChar( 255 ) NULL,
	`CUST_PHONE` VarChar( 255 ) NULL,
	`PART_TRAN_TYPE` VarChar( 255 ) NULL,
	`AMOUNT` VarChar( 255 ) NULL,
	`TRAN_PARTICULARS` VarChar( 255 ) NULL,
	`sms_alert_id` BigInt( 20 ) NULL,
	`email_alert_id` BigInt( 20 ) NULL,
	`RCRE_TIME` DateTime NULL,
	`last_update` DateTime NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `uq_alert_transaction_email_alert_id` UNIQUE( `email_alert_id` ),
	CONSTRAINT `uq_alert_transaction_sms_alert_id` UNIQUE( `sms_alert_id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- ---------------------------------------------------------


-- CREATE TABLE "app_property" -----------------------------
CREATE TABLE `app_property` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`prop_key` VarChar( 255 ) NULL,
	`prop_value` VarChar( 255 ) NULL,
	`last_update` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- ---------------------------------------------------------


-- CREATE TABLE "auth_signature" ---------------------------
CREATE TABLE `auth_signature` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) NULL,
	`sign_path` VarChar( 255 ) NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- ---------------------------------------------------------


-- CREATE TABLE "bulk_alert" -------------------------------
CREATE TABLE `bulk_alert` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`is_sms` TinyInt( 1 ) NULL DEFAULT '0',
	`sms_message` VarChar( 255 ) NULL,
	`email_message` LongText NULL,
	`comma_sep_list` VarChar( 255 ) NULL,
	`subject` VarChar( 255 ) NULL,
	`time_initiated` DateTime NULL,
	`initiated_by_id` BigInt( 20 ) NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- ---------------------------------------------------------


-- CREATE TABLE "bulk_alert_list" --------------------------
CREATE TABLE `bulk_alert_list` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`is_sms` TinyInt( 1 ) NULL DEFAULT '0',
	`comma_sep_list` Text NOT NULL,
	`list_name` VarChar( 255 ) NULL,
	`created_by_id` BigInt( 20 ) NULL,
	`creation_date` DateTime NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `uq_bulk_alert_list_list_name` UNIQUE( `list_name` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- ---------------------------------------------------------


-- CREATE TABLE "email_alert" ------------------------------
CREATE TABLE `email_alert` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`alert_message` LongText NULL,
	`subject` VarChar( 255 ) NULL,
	`time_sent` DateTime NULL,
	`tran_id` VarChar( 255 ) NULL,
	`tran_date` DateTime NULL,
	`receiver_email` VarChar( 255 ) NULL,
	`receiver_name` VarChar( 255 ) NULL,
	`account_balance` Decimal( 38, 0 ) NULL,
	`is_sent` TinyInt( 1 ) NULL DEFAULT '0',
	`last_update` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 69;
-- ---------------------------------------------------------


-- CREATE TABLE "email_alert_attachment" -------------------
CREATE TABLE `email_alert_attachment` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`attached_file` LongBlob NULL,
	`description` VarChar( 255 ) NULL,
	`name` VarChar( 255 ) NULL,
	`disposition` VarChar( 255 ) NULL,
	`is_attachment` TinyInt( 1 ) NULL DEFAULT '0',
	`email_id` BigInt( 20 ) NULL,
	`last_update` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- ---------------------------------------------------------


-- CREATE TABLE "login_audit_trail" ------------------------
CREATE TABLE `login_audit_trail` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`event_type` VarChar( 16 ) NULL,
	`event_date` DateTime NULL,
	`ip_address` VarChar( 255 ) NULL,
	`username` VarChar( 255 ) NULL,
	`user_id` BigInt( 20 ) NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 34;
-- ---------------------------------------------------------


-- CREATE TABLE "sms_alert" --------------------------------
CREATE TABLE `sms_alert` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`alert_message` VarChar( 255 ) NULL,
	`time_sent` DateTime NULL,
	`tran_id` VarChar( 255 ) NULL,
	`tran_date` DateTime NULL,
	`receiver_phone_number` VarChar( 255 ) NULL,
	`is_sent` TinyInt( 1 ) NULL DEFAULT '0',
	`account_balance` Decimal( 38, 0 ) NULL,
	`failed` TinyInt( 1 ) NULL DEFAULT '0',
	`fail_message` VarChar( 255 ) NULL,
	`last_update` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- ---------------------------------------------------------


-- CREATE TABLE "term_deposit" -----------------------------
CREATE TABLE `term_deposit` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`deposit_period_months` Int( 11 ) NULL,
	`deposit_period_days` Int( 11 ) NULL,
	`open_effective_date` DateTime NULL,
	`maturity_date` DateTime NULL,
	`deposit_amount` Decimal( 38, 0 ) NULL,
	`currency_code` VarChar( 255 ) NULL,
	`maturity_amount` Decimal( 38, 0 ) NULL,
	`interest_rate` Double( 22, 0 ) NULL,
	`creation_date` DateTime NULL,
	`customer_id` VarChar( 255 ) NULL,
	`account_number` VarChar( 255 ) NULL,
	`account_id` VarChar( 255 ) NULL,
	`tran_id` VarChar( 255 ) NULL,
	`scheme_code` VarChar( 255 ) NULL,
	`funding_account_name` VarChar( 255 ) NULL,
	`account_name` VarChar( 255 ) NULL,
	`address_line1` VarChar( 255 ) NULL,
	`address_line2` VarChar( 255 ) NULL,
	`contract_rate` VarChar( 255 ) NULL,
	`signed_by1_id` BigInt( 20 ) NULL,
	`signed_by2_id` BigInt( 20 ) NULL,
	`email_alert_id` BigInt( 20 ) NULL,
	`last_update` DateTime NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `uq_term_deposit_email_alert_id` UNIQUE( `email_alert_id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 67;
-- ---------------------------------------------------------


-- CREATE TABLE "user_history" -----------------------------
CREATE TABLE `user_history` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`user_id` BigInt( 20 ) NULL,
	`initiator_id` BigInt( 20 ) NULL,
	`event_date` DateTime NULL,
	`event` Int( 11 ) NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- ---------------------------------------------------------


-- CREATE TABLE "user_table" -------------------------------
CREATE TABLE `user_table` ( 
	`id` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`username` VarChar( 255 ) NULL,
	`password_hash` VarChar( 255 ) NULL,
	`is_enabled` TinyInt( 1 ) NULL DEFAULT '0',
	`is_first_login` TinyInt( 1 ) NULL DEFAULT '0',
	`role` VarChar( 16 ) NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `uq_user_table_username` UNIQUE( `username` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- ---------------------------------------------------------


-- Dump data of "alert_permission" -------------------------
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '1', 'R000000011111', '0000000111', '12345678', '0', '0', 'TORTI CHIGOZIRIM' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '2', 'R001758', NULL, NULL, '0', '0', 'ABIODUN Q SANUSI' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '3', 'R000526', NULL, NULL, '0', '0', 'ABOLAJI F ODUJOBI' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '4', 'R000138', NULL, NULL, '1', '1', 'ADELEKE A ADEMOLA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '5', 'R000295', NULL, NULL, '0', '0', 'ADEYEMI O SAHEED' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '6', 'R001625', NULL, NULL, '0', '0', 'AJIBOLA S RONKE' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '7', 'R000219', NULL, NULL, '0', '0', 'AKINTOLA A TITUS' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '8', 'R001631', NULL, NULL, '0', '0', 'ALASOADURA O OLUWAGBEMIGA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '9', 'R001640', NULL, NULL, '0', '0', 'AYI N NSA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '10', 'R000233', NULL, NULL, '0', '0', 'AZEEZ A ADEMOLA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '11', 'R001713', NULL, NULL, '0', '0', 'BABATUNDE S SHODOLAMU' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '12', 'R001621', NULL, NULL, '0', '0', 'BAKARE S OLUJIDE' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '13', 'R001718', NULL, NULL, '0', '0', 'CHINEDU S IHUOMA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '14', 'R001633', NULL, NULL, '0', '0', 'EBADAN C ADEBOLA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '15', 'R001710', NULL, NULL, '1', '1', 'ECHEZONA K EZEOKA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '16', 'R001594', NULL, NULL, '0', '0', 'EJEMAI C ONORIODE' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '17', 'R001627', NULL, NULL, '0', '0', 'EKUNDAYO O FEYISAYO' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '18', 'R000225', NULL, NULL, '0', '0', 'ENIAIYEYE S FADEKE' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '19', 'R000487', NULL, NULL, '0', '0', 'FUNKE  OKOYA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '20', 'R000231', NULL, NULL, '0', '0', 'IBRAHIM O AZEEZ' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '21', 'R001717', NULL, NULL, '0', '0', 'IFEOMA R OKEKE' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '22', 'R000141', NULL, NULL, '0', '0', 'IGBINOSUN V OGHOGHO' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '23', 'R001751', NULL, NULL, '0', '0', 'JOHN O OGUNPOLU' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '24', 'R000200', NULL, NULL, '0', '0', 'MALOMO K BASHIRU' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '25', 'R000237', NULL, NULL, '0', '0', 'OBIORA A SAMUEL' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '26', 'R000269', NULL, NULL, '0', '0', 'OLABODE A OLAOBA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '27', 'R000195', NULL, NULL, '0', '0', 'OLAWEPO  OLABISI' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '28', 'R000224', NULL, NULL, '0', '0', 'OLUKOYA  KIKELOMO' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '29', 'R001691', NULL, NULL, '0', '0', 'OLULEYE D ESOMOJUMI' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '30', 'R000208', NULL, NULL, '0', '0', 'OLURINOLA B BUKOLA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '31', 'R000395', NULL, NULL, '0', '0', 'OLUWAGBENGA I ADEKOLA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '32', 'R001632', NULL, NULL, '0', '0', 'OMOSEBI V OMOTOLA' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '33', 'R000068', NULL, NULL, '0', '0', 'OMOTOSO  OLUWEMIMO' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '34', 'R001754', NULL, NULL, '0', '0', 'ONOME H KOMOLAFE' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '35', 'R000202', NULL, NULL, '0', '0', 'OTENSON  NCHOLAS' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '36', 'R000471', NULL, NULL, '1', '1', 'OTIEDE O MATTHEW' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '37', 'R02934324', '', '', '1', '0', 'Chigozirim Torti' );
INSERT INTO `alert_permission`(`id`,`customer_id`,`account_number`,`account_id`,`allow_sms`,`allow_email`,`customer_name`) VALUES ( '38', 'R023423432', '0054652312', '', '1', '1', 'Olujimi Otulana' );
-- ---------------------------------------------------------


-- Dump data of "alert_transaction" ------------------------
INSERT INTO `alert_transaction`(`id`,`TRAN_ID`,`TRAN_DATE`,`ACID`,`FORACID`,`CUST_ID`,`CUST_EMAIL`,`CUST_PHONE`,`PART_TRAN_TYPE`,`AMOUNT`,`TRAN_PARTICULARS`,`sms_alert_id`,`email_alert_id`,`RCRE_TIME`,`last_update`) VALUES ( '1', '     0183', '2015-11-11 00:00:00', '0119407', '1990003911', 'R000225', 'lotad@yahoo.com', NULL, 'C', '8000000', 'TRSF IFO AMAS SIMON IGBINOBA-ENIAIYEYE SIMILOLA', NULL, '3', '2015-11-11 14:59:27', '2015-11-11 16:48:54' );
INSERT INTO `alert_transaction`(`id`,`TRAN_ID`,`TRAN_DATE`,`ACID`,`FORACID`,`CUST_ID`,`CUST_EMAIL`,`CUST_PHONE`,`PART_TRAN_TYPE`,`AMOUNT`,`TRAN_PARTICULARS`,`sms_alert_id`,`email_alert_id`,`RCRE_TIME`,`last_update`) VALUES ( '2', '  Q112604', '2015-11-11 00:00:00', '0119315', '1990003311', 'R000138', 'DEMOLA.ADELEKE@GMAIL.COM', NULL, 'D', '200000', 'TRF FRM ADELEKE A ADEMOLA to ESOMOJUMI, OLULEYE "0', NULL, '4', '2015-11-11 15:42:09', '2015-11-11 16:48:54' );
INSERT INTO `alert_transaction`(`id`,`TRAN_ID`,`TRAN_DATE`,`ACID`,`FORACID`,`CUST_ID`,`CUST_EMAIL`,`CUST_PHONE`,`PART_TRAN_TYPE`,`AMOUNT`,`TRAN_PARTICULARS`,`sms_alert_id`,`email_alert_id`,`RCRE_TIME`,`last_update`) VALUES ( '3', '  Q112604', '2015-11-11 00:00:00', '0119315', '1990003311', 'R000138', 'DEMOLA.ADELEKE@GMAIL.COM', NULL, 'D', '70', 'NIBSS TRANSFER CHARGE', NULL, '5', '2015-11-11 15:42:09', '2015-11-11 16:48:55' );
INSERT INTO `alert_transaction`(`id`,`TRAN_ID`,`TRAN_DATE`,`ACID`,`FORACID`,`CUST_ID`,`CUST_EMAIL`,`CUST_PHONE`,`PART_TRAN_TYPE`,`AMOUNT`,`TRAN_PARTICULARS`,`sms_alert_id`,`email_alert_id`,`RCRE_TIME`,`last_update`) VALUES ( '4', '    01110', '2015-11-11 00:00:00', '0119315', '1990003311', 'R000138', 'DEMOLA.ADELEKE@GMAIL.COM', NULL, 'C', '220591.22', '9920151023000153 : Closure Proceeds', NULL, '6', '2015-11-11 15:31:06', '2015-11-11 16:48:55' );
-- ---------------------------------------------------------


-- Dump data of "app_property" -----------------------------
INSERT INTO `app_property`(`id`,`prop_key`,`prop_value`,`last_update`) VALUES ( '1', 'countOfTransactions', '37', '2015-11-12 09:35:16' );
INSERT INTO `app_property`(`id`,`prop_key`,`prop_value`,`last_update`) VALUES ( '2', 'countOfTDs', '0', '2015-11-14 13:55:00' );
-- ---------------------------------------------------------


-- Dump data of "auth_signature" ---------------------------
INSERT INTO `auth_signature`(`id`,`name`,`sign_path`) VALUES ( '1', 'Olujimi Otulana', 'public/signature/jimisign.jpg' );
INSERT INTO `auth_signature`(`id`,`name`,`sign_path`) VALUES ( '2', 'Torti Chigozirim', 'public/signature/tortisign1.jpg' );
INSERT INTO `auth_signature`(`id`,`name`,`sign_path`) VALUES ( '3', 'Ademola Adeleke', 'public/signature/201511121124AdemolaAdeleke.jpg' );
-- ---------------------------------------------------------


-- Dump data of "bulk_alert" -------------------------------
INSERT INTO `bulk_alert`(`id`,`is_sms`,`sms_message`,`email_message`,`comma_sep_list`,`subject`,`time_initiated`,`initiated_by_id`) VALUES ( '1', '0', NULL, 'Testing the bulk email sender.', 'tortichigozirim@gmail.com,nnasino@icloud.com,tortichigozirim@live.com,chigozirim.torti@longbridgetech.com', 'Real analysis', '2015-11-17 15:00:38', '1' );
INSERT INTO `bulk_alert`(`id`,`is_sms`,`sms_message`,`email_message`,`comma_sep_list`,`subject`,`time_initiated`,`initiated_by_id`) VALUES ( '2', '1', 'Testing the sms bulk alerts', NULL, '08063263879,08033220129', '', '2015-11-17 15:04:58', '1' );
-- ---------------------------------------------------------


-- Dump data of "bulk_alert_list" --------------------------
INSERT INTO `bulk_alert_list`(`id`,`is_sms`,`comma_sep_list`,`list_name`,`created_by_id`,`creation_date`) VALUES ( '1', '0', 'nnasino2008@live.com,tortichigozirim@gmail.com', 'Torti emails', '2', '2015-10-10 00:00:00' );
INSERT INTO `bulk_alert_list`(`id`,`is_sms`,`comma_sep_list`,`list_name`,`created_by_id`,`creation_date`) VALUES ( '2', '1', '08063263879,08033220129', 'Torti Numbers', '1', '2015-11-15 13:40:48' );
INSERT INTO `bulk_alert_list`(`id`,`is_sms`,`comma_sep_list`,`list_name`,`created_by_id`,`creation_date`) VALUES ( '3', '0', 'tortichigozirim@gmail.com,nnasino@icloud.com,tortichigozirim@live.com, chigozirim.torti@longbridgetech.com', 'All customers', '1', '2015-11-16 12:55:30' );
-- ---------------------------------------------------------


-- Dump data of "email_alert" ------------------------------
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '1', '
Dear Sir, Madam,
        Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us.

', 'Term Deposit', '2015-11-12 14:28:58', 'NG554', '2015-10-10 00:00:00', 'eezeoka@coronationmb.com', 'Echezona Ezeoka', '23432423', '1', '2015-11-12 14:28:58' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '2', 'Email Certificate Test', 'Email Test', '2015-11-12 14:28:23', 'NG2342', '2015-10-10 00:00:00', 'eezeoka@coronationmb.com', 'Echezona Ezeoka', '2342', '1', '2015-11-12 14:28:23' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '3', '<html><div style=\'text-align:center\'>Coronation Merchant Bank<br/>
This is a summary of a transaction that has occurred on your account.
<table style=\'width: 50%;margin-left: auto; margin-right: auto;\'>
   <tbody>
	<tr>
	   <td>Date/Time</td><td>11-11-2015 02:59:27</td>
	</tr>
	<tr>
	   <td>Account</td><td>1990003911</td>
	</tr>
	<tr>
	   <td>Description</td><td>TRSF IFO AMAS SIMON IGBINOBA-ENIAIYEYE SIMILOLA</td>
	</tr>	
	<tr>
	   <td>Amount</td><td>8,000,000.00</td>
	</tr>
	<tr>
	   <td>Credit/Debit</td><td>Credit</td>
	</tr>	
	<tr>
	   <td>Current Balance</td><td>8,000,000.00</td>
	</tr>
   </tbody>
</table>

<p>For more information, Please call us on 01-4614892 Send an email to info@coronationmb.com

Copyright © 2015 Coronation Merchant Bank Limited | www.coronationmb.com</p></div></html>', 'Credit Alert', '2015-11-11 16:49:16', '     0183', '2015-11-11 00:00:00', 'lotad@yahoo.com', 'ENIAIYEYE', '8000000', '1', '2015-11-11 16:49:16' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '4', '<html><div style=\'text-align:center\'>Coronation Merchant Bank<br/>
This is a summary of a transaction that has occurred on your account.
<table style=\'width: 50%;margin-left: auto; margin-right: auto;\'>
   <tbody>
	<tr>
	   <td>Date/Time</td><td>11-11-2015 03:42:09</td>
	</tr>
	<tr>
	   <td>Account</td><td>1990003311</td>
	</tr>
	<tr>
	   <td>Description</td><td>TRF FRM ADELEKE A ADEMOLA to ESOMOJUMI, OLULEYE "0</td>
	</tr>	
	<tr>
	   <td>Amount</td><td>200,000.00</td>
	</tr>
	<tr>
	   <td>Credit/Debit</td><td>Debit</td>
	</tr>	
	<tr>
	   <td>Current Balance</td><td>22,440.33</td>
	</tr>
   </tbody>
</table>

<p>For more information, Please call us on 01-4614892 Send an email to info@coronationmb.com

Copyright © 2015 Coronation Merchant Bank Limited | www.coronationmb.com</p></div></html>', 'Debit Alert', '2015-11-11 16:49:15', '  Q112604', '2015-11-11 00:00:00', 'DEMOLA.ADELEKE@GMAIL.COM', 'ADELEKE', '22440', '1', '2015-11-11 16:49:15' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '5', '<html><div style=\'text-align:center\'>Coronation Merchant Bank<br/>
This is a summary of a transaction that has occurred on your account.
<table style=\'width: 50%;margin-left: auto; margin-right: auto;\'>
   <tbody>
	<tr>
	   <td>Date/Time</td><td>11-11-2015 03:42:09</td>
	</tr>
	<tr>
	   <td>Account</td><td>1990003311</td>
	</tr>
	<tr>
	   <td>Description</td><td>NIBSS TRANSFER CHARGE</td>
	</tr>	
	<tr>
	   <td>Amount</td><td>70.00</td>
	</tr>
	<tr>
	   <td>Credit/Debit</td><td>Debit</td>
	</tr>	
	<tr>
	   <td>Current Balance</td><td>22,440.33</td>
	</tr>
   </tbody>
</table>

<p>For more information, Please call us on 01-4614892 Send an email to info@coronationmb.com

Copyright © 2015 Coronation Merchant Bank Limited | www.coronationmb.com</p></div></html>', 'Debit Alert', '2015-11-11 16:49:15', '  Q112604', '2015-11-11 00:00:00', 'DEMOLA.ADELEKE@GMAIL.COM', 'ADELEKE', '22440', '1', '2015-11-11 16:49:15' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '6', '<html><div style=\'text-align:center\'>Coronation Merchant Bank<br/>
This is a summary of a transaction that has occurred on your account.
<table style=\'width: 50%;margin-left: auto; margin-right: auto;\'>
   <tbody>
	<tr>
	   <td>Date/Time</td><td>11-11-2015 03:31:06</td>
	</tr>
	<tr>
	   <td>Account</td><td>1990003311</td>
	</tr>
	<tr>
	   <td>Description</td><td>9920151023000153 : Closure Proceeds</td>
	</tr>	
	<tr>
	   <td>Amount</td><td>220,591.22</td>
	</tr>
	<tr>
	   <td>Credit/Debit</td><td>Credit</td>
	</tr>	
	<tr>
	   <td>Current Balance</td><td>22,440.33</td>
	</tr>
   </tbody>
</table>

<p>For more information, Please call us on 01-4614892 Send an email to info@coronationmb.com

Copyright © 2015 Coronation Merchant Bank Limited | www.coronationmb.com</p></div></html>', 'Credit Alert', '2015-11-11 16:49:15', '    01110', '2015-11-11 00:00:00', 'DEMOLA.ADELEKE@GMAIL.COM', 'ADELEKE', '22440', '1', '2015-11-11 16:49:15' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '7', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0118', '2015-11-12 09:33:01', 'FRANCISOJIAH@YAHOO.COM', 'OJIAH F OZOVEHE', '9064', '0', '2015-11-12 15:13:20' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '8', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0148', '2015-11-12 12:33:33', 'UBI.BENJAMIN1@YAHOO.COM', 'BENJAMIN E UBI', '970000', '0', '2015-11-12 15:13:20' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '9', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0167', '2015-11-12 13:54:47', 'MHSADAUKI@GMAIL.COM', 'MANSIR H SADAUKI', '3500000', '0', '2015-11-12 15:13:20' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '10', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0169', '2015-11-12 14:14:47', 'NO_EMAIL_1160000@ADH.COM', 'AYOMIDE O TEMITOPE', '1800000', '0', '2015-11-12 15:13:20' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '11', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-12 14:24:45', 'UPDATE_THIS_EMAIL@ADH.COM', 'LEVITE MICROFINANCE BANK LIMITED', '4902280', '0', '2015-11-12 15:13:20' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '12', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-12 14:24:45', 'UPDATE_THIS_EMAIL@ADH.COM', 'LEVITE MICROFINANCE BANK LIMITED', '4902280', '0', '2015-11-12 15:13:20' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '13', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '      015', '2015-11-12 09:24:31', 'olabisi.adekola@africaalliance.com', 'AFRICAN ALLIANCE INSURANCE CO. LTD', '80381535', '0', '2015-11-12 15:13:20' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '14', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0118', '2015-11-12 09:33:01', 'FRANCISOJIAH@YAHOO.COM', 'OJIAH F OZOVEHE', '9064', '0', '2015-11-12 16:41:52' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '15', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0148', '2015-11-12 12:33:33', 'UBI.BENJAMIN1@YAHOO.COM', 'BENJAMIN E UBI', '970000', '0', '2015-11-12 16:41:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '16', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0167', '2015-11-12 13:54:47', 'MHSADAUKI@GMAIL.COM', 'MANSIR H SADAUKI', '3500000', '0', '2015-11-12 16:41:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '17', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01100', '2015-11-12 15:49:58', 'INFO@NASDNG.COM', 'NASD PLC', '100986301', '0', '2015-11-12 16:41:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '18', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01104', '2015-11-12 15:27:24', 'DEMOLA.ADELEKE@GMAIL.COM', 'ADELEKE A ADEMOLA', '200000', '0', '2015-11-12 16:41:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '19', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0169', '2015-11-12 14:14:47', 'NO_EMAIL_1160000@ADH.COM', 'AYOMIDE O TEMITOPE', '1800000', '0', '2015-11-12 16:41:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '20', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-12 14:24:45', 'UPDATE_THIS_EMAIL@ADH.COM', 'LEVITE MICROFINANCE BANK LIMITED', '4902280', '0', '2015-11-12 16:41:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '21', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-12 14:24:45', 'UPDATE_THIS_EMAIL@ADH.COM', 'LEVITE MICROFINANCE BANK LIMITED', '4902280', '0', '2015-11-12 16:41:54' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '22', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '      015', '2015-11-12 09:24:31', 'olabisi.adekola@africaalliance.com', 'AFRICAN ALLIANCE INSURANCE CO. LTD', '80381535', '0', '2015-11-12 16:41:54' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '23', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01117', '2015-11-12 16:27:59', 'omotolajoseph1@gmail.com', 'OLOLADE O JOSEPH', '1100000', '0', '2015-11-12 16:41:54' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '24', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0118', '2015-11-12 09:33:01', 'FRANCISOJIAH@YAHOO.COM', 'OJIAH F OZOVEHE', '9064', '0', '2015-11-12 16:42:52' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '25', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0148', '2015-11-12 12:33:33', 'UBI.BENJAMIN1@YAHOO.COM', 'BENJAMIN E UBI', '970000', '0', '2015-11-12 16:42:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '26', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0167', '2015-11-12 13:54:47', 'MHSADAUKI@GMAIL.COM', 'MANSIR H SADAUKI', '3500000', '0', '2015-11-12 16:42:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '27', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01100', '2015-11-12 15:49:58', 'INFO@NASDNG.COM', 'NASD PLC', '100986301', '0', '2015-11-12 16:42:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '28', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01104', '2015-11-12 15:27:24', 'DEMOLA.ADELEKE@GMAIL.COM', 'ADELEKE A ADEMOLA', '200000', '0', '2015-11-12 16:42:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '29', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0169', '2015-11-12 14:14:47', 'NO_EMAIL_1160000@ADH.COM', 'AYOMIDE O TEMITOPE', '1800000', '0', '2015-11-12 16:42:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '30', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-12 14:24:45', 'UPDATE_THIS_EMAIL@ADH.COM', 'LEVITE MICROFINANCE BANK LIMITED', '4902280', '0', '2015-11-12 16:42:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '31', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-12 14:24:45', 'UPDATE_THIS_EMAIL@ADH.COM', 'LEVITE MICROFINANCE BANK LIMITED', '4902280', '0', '2015-11-12 16:42:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '32', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '      015', '2015-11-12 09:24:31', 'olabisi.adekola@africaalliance.com', 'AFRICAN ALLIANCE INSURANCE CO. LTD', '80381535', '0', '2015-11-12 16:42:53' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '33', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01117', '2015-11-12 16:27:59', 'omotolajoseph1@gmail.com', 'OLOLADE O JOSEPH', '1100000', '0', '2015-11-12 16:42:54' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '34', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01119', '2015-11-12 16:33:24', 'INFO@NASDNG.COM', 'NASD PLC', '50000000', '0', '2015-11-12 16:42:54' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '35', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0118', '2015-11-12 09:33:01', 'FRANCISOJIAH@YAHOO.COM', 'OJIAH F OZOVEHE', '9064', '0', '2015-11-12 16:51:16' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '36', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0148', '2015-11-12 12:33:33', 'UBI.BENJAMIN1@YAHOO.COM', 'BENJAMIN E UBI', '970000', '0', '2015-11-12 16:51:16' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '37', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0167', '2015-11-12 13:54:47', 'MHSADAUKI@GMAIL.COM', 'MANSIR H SADAUKI', '3500000', '0', '2015-11-12 16:51:16' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '38', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01100', '2015-11-12 15:49:58', 'INFO@NASDNG.COM', 'NASD PLC', '100986301', '0', '2015-11-12 16:51:16' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '39', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01104', '2015-11-12 15:27:24', 'DEMOLA.ADELEKE@GMAIL.COM', 'ADELEKE A ADEMOLA', '200000', '0', '2015-11-12 16:51:17' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '40', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0169', '2015-11-12 14:14:47', 'NO_EMAIL_1160000@ADH.COM', 'AYOMIDE O TEMITOPE', '1800000', '0', '2015-11-12 16:51:17' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '41', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-12 14:24:45', 'UPDATE_THIS_EMAIL@ADH.COM', 'LEVITE MICROFINANCE BANK LIMITED', '4902280', '0', '2015-11-12 16:51:17' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '42', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-12 14:24:45', 'UPDATE_THIS_EMAIL@ADH.COM', 'LEVITE MICROFINANCE BANK LIMITED', '4902280', '0', '2015-11-12 16:51:17' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '43', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '      015', '2015-11-12 09:24:31', 'olabisi.adekola@africaalliance.com', 'AFRICAN ALLIANCE INSURANCE CO. LTD', '80381535', '0', '2015-11-12 16:51:17' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '44', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01117', '2015-11-12 16:27:59', 'omotolajoseph1@gmail.com', 'OLOLADE O JOSEPH', '1100000', '0', '2015-11-12 16:51:17' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '45', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01119', '2015-11-12 16:33:24', 'INFO@NASDNG.COM', 'NASD PLC', '50000000', '0', '2015-11-12 16:51:17' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '46', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01121', '2015-11-12 16:04:25', 'omotolajoseph1@gmail.com', 'OLOLADE O JOSEPH', '200000', '0', '2015-11-12 16:51:17' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '47', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0169', '2015-11-13 15:15:22', 'info@mayfairbank.com', 'MAYFAIR MICROFINANCE BANK LIMITED', '20501642', '0', '2015-11-13 17:22:03' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '48', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0170', '2015-11-13 15:00:01', 'INFO@ARM.COM.NG', 'ASSET RESOURCE MANAGEMENT', '400012329', '0', '2015-11-13 17:22:03' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '49', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0171', '2015-11-13 14:56:14', 'INFO@ARM.COM.NG', 'ASSET RESOURCE MANAGEMENT', '28704799', '0', '2015-11-13 17:22:03' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '50', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0180', '2015-11-13 14:44:38', 'INFO@ARM.COM.NG', 'ASSET RESOURCE MANAGEMENT', '201635404', '0', '2015-11-13 17:22:03' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '51', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01114', '2015-11-13 16:16:15', 'NOEMAIL@CORONATIONMERCHANTBANK.COM', 'PAULIBNE CKUKWUNENYE CHIBUTUTU', '100000', '0', '2015-11-13 17:22:03' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '52', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01116', '2015-11-13 16:21:14', 'NOEMAIL@CORONATIONMERCHANTBANK.COM', 'PAULIBNE CKUKWUNENYE CHIBUTUTU', '50000', '0', '2015-11-13 17:22:04' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '53', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01118', '2015-11-13 16:06:52', 'islandmicrofinancebankltd@yahoo.com', 'ISLAND MICROFINANCE BANK LIMITED', '3000000', '0', '2015-11-13 17:22:04' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '54', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0166', '2015-11-13 15:05:32', 'SIMONOBA@GMAIL.COM', 'EKLOSIM LIMITED', '18561730', '0', '2015-11-13 17:22:04' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '55', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0168', '2015-11-13 14:51:53', 'INFO@ARM.COM.NG', 'ASSET RESOURCE MANAGEMENT', '104295647', '0', '2015-11-13 17:22:04' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '56', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01103', '2015-11-13 15:58:41', 'NO_EMAIL_10214@ADH.COM', 'ADEKEYE J ROTIMI', '7179022', '0', '2015-11-13 17:22:04' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '57', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0126', '2015-11-13 11:57:00', 'NO_EMAIL_2337@ADH.COM', 'AKUJIEZE  NKEMAKONAM', '15320609', '0', '2015-11-13 17:22:04' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '58', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '     0127', '2015-11-13 12:04:37', 'EJIOL@YAHOO.COM', 'EJIO B LATEEF', '1999994', '0', '2015-11-13 17:22:04' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '59', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01124', '2015-11-13 16:49:55', 'NO_EMAIL_4877@ADH.COM', 'TELLA H OLUBUNMI', '1000000', '0', '2015-11-13 17:22:04' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '60', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01125', '2015-11-13 16:55:44', 'NO_EMAIL_4877@ADH.COM', 'TELLA H OLUBUNMI', '5394171', '0', '2015-11-13 17:22:05' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '61', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01101', '2015-11-13 15:51:36', 'NO_EMAIL_99800@ADH.COM', 'UMEZURIKE H OKAFOR', '73115026', '0', '2015-11-13 17:22:05' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '62', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', NULL, '    01106', '2015-11-13 16:13:27', 'islandmicrofinancebankltd@yahoo.com', 'ISLAND MICROFINANCE BANK LIMITED', '5000000', '0', '2015-11-13 17:22:05' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '63', 'Dear Sir, Madam, Please find attached the Investment certificate for your newly created investment with us. Thanks for banking with us', 'Term Deposit Certificate', '2015-11-20 12:14:31', '    01106', '2015-11-13 16:13:27', 'chigozirim.torti@longbridgetech.com', 'Chigozirim Torti', '43444444', '1', '2015-11-20 12:14:32' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0000-00-00 00:00:00' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '65', 'Testing the bulk email sender.', 'Real analysis', NULL, '', '2015-11-17 15:00:38', 'tortichigozirim@gmail.com', '', NULL, '0', '2015-11-17 15:00:38' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '66', 'Testing the bulk email sender.', 'Real analysis', NULL, '', '2015-11-17 15:00:38', 'nnasino@icloud.com', '', NULL, '0', '2015-11-17 15:00:38' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '67', 'Testing the bulk email sender.', 'Real analysis', NULL, '', '2015-11-17 15:00:38', 'tortichigozirim@live.com', '', NULL, '0', '2015-11-17 15:00:38' );
INSERT INTO `email_alert`(`id`,`alert_message`,`subject`,`time_sent`,`tran_id`,`tran_date`,`receiver_email`,`receiver_name`,`account_balance`,`is_sent`,`last_update`) VALUES ( '68', 'Testing the bulk email sender.', 'Real analysis', NULL, '', '2015-11-17 15:00:38', 'chigozirim.torti@longbridgetech.com', '', NULL, '0', '2015-11-17 15:00:38' );
-- ---------------------------------------------------------


-- Dump data of "email_alert_attachment" -------------------
INSERT INTO `email_alert_attachment`(`id`,`attached_file`,`description`,`name`,`disposition`,`is_attachment`,`email_id`,`last_update`) VALUES ( '1', 0x0255044462, NULL, NULL, NULL, '1', '2', '2015-11-12 14:28:21' );
INSERT INTO `email_alert_attachment`(`id`,`attached_file`,`description`,`name`,`disposition`,`is_attachment`,`email_id`,`last_update`) VALUES ( '2', 0x0255044462, NULL, NULL, NULL, '1', '1', '2015-11-12 14:28:57' );
INSERT INTO `email_alert_attachment`(`id`,`attached_file`,`description`,`name`,`disposition`,`is_attachment`,`email_id`,`last_update`) VALUES ( '3', 0x255044462E312F340B25, NULL, NULL, NULL, '1', '63', '2015-11-20 12:14:26' );
-- ---------------------------------------------------------


-- Dump data of "login_audit_trail" ------------------------
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '1', 'LOGIN_OK', '2015-11-11 15:10:32', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '2', 'LOGIN_OK', '2015-11-12 09:26:15', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '3', 'LOGIN_OK', '2015-11-12 10:04:49', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '4', 'USER_NOT_FOUND', '2015-11-12 11:26:15', '127.0.0.1', 'torti', NULL );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '5', 'LOGIN_OK', '2015-11-12 11:26:22', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '6', 'LOGIN_OK', '2015-11-12 14:21:17', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '7', 'LOGIN_OK', '2015-11-12 15:53:09', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '8', 'LOGIN_OK', '2015-11-12 15:54:53', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '9', 'LOGIN_OK', '2015-11-13 17:28:19', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '10', 'LOGIN_OK', '2015-11-13 17:30:51', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '11', 'LOGIN_OK', '2015-11-14 13:09:45', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '12', 'LOGIN_OK', '2015-11-14 13:55:21', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '13', 'LOGIN_OK', '2015-11-14 15:17:04', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '14', 'USER_NOT_FOUND', '2015-11-14 15:58:51', '127.0.0.1', 'torti', NULL );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '15', 'LOGIN_OK', '2015-11-14 15:58:57', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '16', 'LOGIN_OK', '2015-11-15 13:31:23', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '17', 'LOGIN_OK', '2015-11-15 14:07:29', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '18', 'USER_NOT_FOUND', '2015-11-16 10:16:36', '127.0.0.1', 'torti', NULL );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '19', 'LOGIN_OK', '2015-11-16 10:16:43', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '20', 'LOGIN_OK', '2015-11-16 11:57:21', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '21', 'LOGIN_OK', '2015-11-16 12:54:31', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '22', 'LOGIN_OK', '2015-11-17 10:13:10', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '23', 'USER_NOT_FOUND', '2015-11-17 11:08:25', '127.0.0.1', 'torti', NULL );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '24', 'LOGIN_OK', '2015-11-17 11:08:32', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '25', 'LOGIN_OK', '2015-11-17 11:46:45', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '26', 'LOGIN_OK', '2015-11-17 15:05:26', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '27', 'LOGIN_OK', '2015-11-18 10:15:13', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '28', 'LOGIN_OK', '2015-11-19 10:29:27', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '29', 'USER_NOT_FOUND', '2015-11-19 15:26:23', '127.0.0.1', 'torti', NULL );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '30', 'LOGIN_OK', '2015-11-19 15:26:29', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '31', 'LOGIN_OK', '2015-11-19 16:20:25', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '32', 'LOGIN_OK', '2015-11-20 11:27:58', '127.0.0.1', NULL, '1' );
INSERT INTO `login_audit_trail`(`id`,`event_type`,`event_date`,`ip_address`,`username`,`user_id`) VALUES ( '33', 'LOGIN_OK', '2015-11-20 12:14:12', '127.0.0.1', NULL, '1' );
-- ---------------------------------------------------------


-- Dump data of "sms_alert" --------------------------------
INSERT INTO `sms_alert`(`id`,`alert_message`,`time_sent`,`tran_id`,`tran_date`,`receiver_phone_number`,`is_sent`,`account_balance`,`failed`,`fail_message`,`last_update`) VALUES ( '1', 'Testing the sms bulk alerts', NULL, '', '2015-11-17 15:04:58', '+2348063263879', '0', NULL, '0', NULL, '2015-11-17 15:04:58' );
INSERT INTO `sms_alert`(`id`,`alert_message`,`time_sent`,`tran_id`,`tran_date`,`receiver_phone_number`,`is_sent`,`account_balance`,`failed`,`fail_message`,`last_update`) VALUES ( '2', 'Testing the sms bulk alerts', NULL, '', '2015-11-17 15:04:58', '+2348033220129', '0', NULL, '0', NULL, '2015-11-17 15:04:58' );
-- ---------------------------------------------------------


-- Dump data of "term_deposit" -----------------------------
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '1', '324', '323', '2015-10-10 00:00:00', '2018-10-10 00:00:00', '23500', 'NGN', '35500', '23', '2015-10-10 00:00:00', 'R000234', '999923425235', '2344566', 'NG235', '301', 'TORTI CHIGOZIRIM', 'TORTI CHIGOZIRIM', 'SAINT NICHOLAS HOSPITAL', 'MARINA, LAGOS', '12', '3', '1', '1', '2015-11-12 14:28:57' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '2', '23', '234', '2015-10-10 00:00:00', '2017-10-10 00:00:00', '4568349', 'NGN', '5734623', '12', '2015-10-10 00:00:00', 'R002323', 'TD8238472934', '2343523', 'NG2355', '304', 'TORTI CHIGOZIRIM', 'TORTI CHIGOZIRIM', 'OGUNLANA DRIVE', 'SURULERE, LAGOS', '11', '1', '2', '2', '2015-11-11 13:54:59' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '10', '999', '30', '2015-11-02 00:00:00', '2099-03-04 00:00:00', '9064', 'NGN', '24171', '0', '2015-11-12 09:33:01', 'R001510', '9920151112000031', '0130421', '     0118', '305', 'OJIAH F OZOVEHE', 'OJIAH F OZOVEHE', 'BLK 6 FLT 3 YENOGOA ST GARKI', NULL, '0', NULL, NULL, '7', '2015-11-12 15:13:20' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '11', '0', '90', '2015-11-11 00:00:00', '2016-02-09 00:00:00', '970000', 'NGN', '986723', '0', '2015-11-12 12:33:33', 'R001750', '9920151112000048', '0130422', '     0148', '303', 'BENJAMIN E UBI', 'BENJAMIN E UBI', 'PLOT 795, CRD LUGBE', 'AMAC,', '0', NULL, NULL, '8', '2015-11-12 15:13:20' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '12', '0', '32', '2015-11-12 00:00:00', '2015-12-14 00:00:00', '3500000', 'NGN', '3519945', '0', '2015-11-12 13:54:47', 'R001764', '9920151112000055', '0130423', '     0167', '303', 'MANSIR H SADAUKI', 'MANSIR H SADAUKI', 'HOUSE 2, 13 WINSTON CHURCHILLSTREET,', 'GARKI AREA 2 PROTEA HOTEL, ASOKORO ABUJA, FCT', '0', NULL, NULL, '9', '2015-11-12 15:13:20' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '13', '0', '90', '2015-11-05 00:00:00', '2016-02-03 00:00:00', '1800000', 'NGN', '1835471', '0', '2015-11-12 14:14:47', 'R001021', '9920151112000062', '0130424', '     0169', '303', 'AYOMIDE O TEMITOPE', 'AYOMIDE O TEMITOPE', '17, LANE H, 4TH AVE,PHASE 2,', NULL, '0', NULL, NULL, '10', '2015-11-12 15:13:20' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '14', '2', '30', '2015-11-04 00:00:00', '2016-02-03 00:00:00', '4902280', 'NGN', '5014690', '0', '2015-11-12 14:24:45', 'C000438', '9920151112000079', '0130425', '     0171', '302', 'LEVITE MICROFINANCE BANK LIMITED', 'LEVITE MICROFINANCE BANK LIMITED', '192, ABA ROAD,', 'PORT HARCOURT', '9', NULL, NULL, '11', '2015-11-12 15:13:20' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '15', '2', '30', '2015-11-04 00:00:00', '2016-02-03 00:00:00', '4902280', 'NGN', '5014690', '0', '2015-11-12 14:24:45', 'C000438', '9920151112000079', '0130425', '     0171', '302', 'LEVITE MICROFINANCE BANK LIMITED', 'LEVITE MICROFINANCE BANK LIMITED', '192, ABA ROAD,', 'PORT HARCOURT', '9', NULL, NULL, '12', '2015-11-12 15:13:20' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '16', '999', '30', '2015-10-09 00:00:00', '2099-02-08 00:00:00', '80381535', 'NGN', '214352228', '0', '2015-11-12 09:24:31', 'C000037', '9920151112000024', '0130420', '      015', '306', 'AFRICAN ALLIANCE INSURANCE CO. LTD', 'AFRICAN ALLIANCE INSURANCE CO. LTD', '61, MARINA LAGOS NIGERIA', 'NIGERIA', '0', NULL, NULL, '13', '2015-11-12 15:13:20' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '17', '999', '30', '2015-11-02 00:00:00', '2099-03-04 00:00:00', '9064', 'NGN', '24171', '0', '2015-11-12 09:33:01', 'R001510', '9920151112000031', '0130421', '     0118', '305', 'OJIAH F OZOVEHE', 'OJIAH F OZOVEHE', 'BLK 6 FLT 3 YENOGOA ST GARKI', NULL, '0', NULL, NULL, '14', '2015-11-12 16:41:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '18', '0', '90', '2015-11-11 00:00:00', '2016-02-09 00:00:00', '970000', 'NGN', '986723', '0', '2015-11-12 12:33:33', 'R001750', '9920151112000048', '0130422', '     0148', '303', 'BENJAMIN E UBI', 'BENJAMIN E UBI', 'PLOT 795, CRD LUGBE', 'AMAC,', '0', NULL, NULL, '15', '2015-11-12 16:41:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '19', '0', '32', '2015-11-12 00:00:00', '2015-12-14 00:00:00', '3500000', 'NGN', '3519945', '0', '2015-11-12 13:54:47', 'R001764', '9920151112000055', '0130423', '     0167', '303', 'MANSIR H SADAUKI', 'MANSIR H SADAUKI', 'HOUSE 2, 13 WINSTON CHURCHILLSTREET,', 'GARKI AREA 2 PROTEA HOTEL, ASOKORO ABUJA, FCT', '0', NULL, NULL, '16', '2015-11-12 16:41:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '20', '0', '90', '2015-11-12 00:00:00', '2016-02-10 00:00:00', '100986301', 'NGN', '103597703', '0', '2015-11-12 15:49:58', 'C000493', '9920151112000093', '0130429', '    01100', '304', 'NASD', 'NASD PLC', '57, MARINA', 'LAGOS', '0', NULL, NULL, '17', '2015-11-12 16:41:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '21', '999', '30', '2015-11-11 00:00:00', '2099-03-13 00:00:00', '200000', 'NGN', '450003', '0', '2015-11-12 15:27:24', 'R000138', '9920151112000086', '0130426', '    01104', '305', 'ADELEKE A ADEMOLA', 'ADELEKE A ADEMOLA', '7, OYEDELE CLOSE, OFF BAJULAIYE RD,', 'MOROCCO, SHOMOLU.', '0', NULL, NULL, '18', '2015-11-12 16:41:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '22', '0', '90', '2015-11-05 00:00:00', '2016-02-03 00:00:00', '1800000', 'NGN', '1835471', '0', '2015-11-12 14:14:47', 'R001021', '9920151112000062', '0130424', '     0169', '303', 'AYOMIDE O TEMITOPE', 'AYOMIDE O TEMITOPE', '17, LANE H, 4TH AVE,PHASE 2,', NULL, '0', NULL, NULL, '19', '2015-11-12 16:41:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '23', '2', '30', '2015-11-04 00:00:00', '2016-02-03 00:00:00', '4902280', 'NGN', '5014690', '0', '2015-11-12 14:24:45', 'C000438', '9920151112000079', '0130425', '     0171', '302', 'LEVITE MICROFINANCE BANK LIMITED', 'LEVITE MICROFINANCE BANK LIMITED', '192, ABA ROAD,', 'PORT HARCOURT', '9', NULL, NULL, '20', '2015-11-12 16:41:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '24', '2', '30', '2015-11-04 00:00:00', '2016-02-03 00:00:00', '4902280', 'NGN', '5014690', '0', '2015-11-12 14:24:45', 'C000438', '9920151112000079', '0130425', '     0171', '302', 'LEVITE MICROFINANCE BANK LIMITED', 'LEVITE MICROFINANCE BANK LIMITED', '192, ABA ROAD,', 'PORT HARCOURT', '9', NULL, NULL, '21', '2015-11-12 16:41:54' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '25', '999', '30', '2015-10-09 00:00:00', '2099-02-08 00:00:00', '80381535', 'NGN', '214352228', '0', '2015-11-12 09:24:31', 'C000037', '9920151112000024', '0130420', '      015', '306', 'AFRICAN ALLIANCE INSURANCE CO. LTD', 'AFRICAN ALLIANCE INSURANCE CO. LTD', '61, MARINA LAGOS NIGERIA', 'NIGERIA', '0', NULL, NULL, '22', '2015-11-12 16:41:54' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '26', '1', '1', '2015-11-06 00:00:00', '2015-12-07 00:00:00', '1100000', 'NGN', '1104671', '0', '2015-11-12 16:27:59', 'R001686', '9920151112000114', '0130431', '    01117', '303', 'OLOLADE O JOSEPH', 'OLOLADE O JOSEPH', '14,EFUNTIDE STREET,AGUDA', 'OFF BROWN ROAD. SURULERE', '0', NULL, NULL, '23', '2015-11-12 16:41:54' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '27', '999', '30', '2015-11-02 00:00:00', '2099-03-04 00:00:00', '9064', 'NGN', '24171', '0', '2015-11-12 09:33:01', 'R001510', '9920151112000031', '0130421', '     0118', '305', 'OJIAH F OZOVEHE', 'OJIAH F OZOVEHE', 'BLK 6 FLT 3 YENOGOA ST GARKI', NULL, '0', NULL, NULL, '24', '2015-11-12 16:42:52' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '28', '0', '90', '2015-11-11 00:00:00', '2016-02-09 00:00:00', '970000', 'NGN', '986723', '0', '2015-11-12 12:33:33', 'R001750', '9920151112000048', '0130422', '     0148', '303', 'BENJAMIN E UBI', 'BENJAMIN E UBI', 'PLOT 795, CRD LUGBE', 'AMAC,', '0', NULL, NULL, '25', '2015-11-12 16:42:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '29', '0', '32', '2015-11-12 00:00:00', '2015-12-14 00:00:00', '3500000', 'NGN', '3519945', '0', '2015-11-12 13:54:47', 'R001764', '9920151112000055', '0130423', '     0167', '303', 'MANSIR H SADAUKI', 'MANSIR H SADAUKI', 'HOUSE 2, 13 WINSTON CHURCHILLSTREET,', 'GARKI AREA 2 PROTEA HOTEL, ASOKORO ABUJA, FCT', '0', NULL, NULL, '26', '2015-11-12 16:42:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '30', '0', '90', '2015-11-12 00:00:00', '2016-02-10 00:00:00', '100986301', 'NGN', '103597703', '0', '2015-11-12 15:49:58', 'C000493', '9920151112000093', '0130429', '    01100', '304', 'NASD', 'NASD PLC', '57, MARINA', 'LAGOS', '0', NULL, NULL, '27', '2015-11-12 16:42:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '31', '999', '30', '2015-11-11 00:00:00', '2099-03-13 00:00:00', '200000', 'NGN', '450003', '0', '2015-11-12 15:27:24', 'R000138', '9920151112000086', '0130426', '    01104', '305', 'ADELEKE A ADEMOLA', 'ADELEKE A ADEMOLA', '7, OYEDELE CLOSE, OFF BAJULAIYE RD,', 'MOROCCO, SHOMOLU.', '0', NULL, NULL, '28', '2015-11-12 16:42:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '32', '0', '90', '2015-11-05 00:00:00', '2016-02-03 00:00:00', '1800000', 'NGN', '1835471', '0', '2015-11-12 14:14:47', 'R001021', '9920151112000062', '0130424', '     0169', '303', 'AYOMIDE O TEMITOPE', 'AYOMIDE O TEMITOPE', '17, LANE H, 4TH AVE,PHASE 2,', NULL, '0', NULL, NULL, '29', '2015-11-12 16:42:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '33', '2', '30', '2015-11-04 00:00:00', '2016-02-03 00:00:00', '4902280', 'NGN', '5014690', '0', '2015-11-12 14:24:45', 'C000438', '9920151112000079', '0130425', '     0171', '302', 'LEVITE MICROFINANCE BANK LIMITED', 'LEVITE MICROFINANCE BANK LIMITED', '192, ABA ROAD,', 'PORT HARCOURT', '9', NULL, NULL, '30', '2015-11-12 16:42:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '34', '2', '30', '2015-11-04 00:00:00', '2016-02-03 00:00:00', '4902280', 'NGN', '5014690', '0', '2015-11-12 14:24:45', 'C000438', '9920151112000079', '0130425', '     0171', '302', 'LEVITE MICROFINANCE BANK LIMITED', 'LEVITE MICROFINANCE BANK LIMITED', '192, ABA ROAD,', 'PORT HARCOURT', '9', NULL, NULL, '31', '2015-11-12 16:42:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '35', '999', '30', '2015-10-09 00:00:00', '2099-02-08 00:00:00', '80381535', 'NGN', '214352228', '0', '2015-11-12 09:24:31', 'C000037', '9920151112000024', '0130420', '      015', '306', 'AFRICAN ALLIANCE INSURANCE CO. LTD', 'AFRICAN ALLIANCE INSURANCE CO. LTD', '61, MARINA LAGOS NIGERIA', 'NIGERIA', '0', NULL, NULL, '32', '2015-11-12 16:42:53' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '36', '1', '1', '2015-11-06 00:00:00', '2015-12-07 00:00:00', '1100000', 'NGN', '1104671', '0', '2015-11-12 16:27:59', 'R001686', '9920151112000114', '0130431', '    01117', '303', 'OLOLADE O JOSEPH', 'OLOLADE O JOSEPH', '14,EFUNTIDE STREET,AGUDA', 'OFF BROWN ROAD. SURULERE', '0', NULL, NULL, '33', '2015-11-12 16:42:54' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '37', '999', '30', '2015-11-12 00:00:00', '2099-03-14 00:00:00', '50000000', 'NGN', '112500685', '0', '2015-11-12 16:33:24', 'C000493', '9920151112000121', '0130432', '    01119', '306', 'NASD', 'NASD PLC', '57, MARINA', 'LAGOS', '0', NULL, NULL, '34', '2015-11-12 16:42:54' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '38', '999', '30', '2015-11-02 00:00:00', '2099-03-04 00:00:00', '9064', 'NGN', '24171', '0', '2015-11-12 09:33:01', 'R001510', '9920151112000031', '0130421', '     0118', '305', 'OJIAH F OZOVEHE', 'OJIAH F OZOVEHE', 'BLK 6 FLT 3 YENOGOA ST GARKI', NULL, '0', NULL, NULL, '35', '2015-11-12 16:51:16' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '39', '0', '90', '2015-11-11 00:00:00', '2016-02-09 00:00:00', '970000', 'NGN', '986723', '0', '2015-11-12 12:33:33', 'R001750', '9920151112000048', '0130422', '     0148', '303', 'BENJAMIN E UBI', 'BENJAMIN E UBI', 'PLOT 795, CRD LUGBE', 'AMAC,', '0', NULL, NULL, '36', '2015-11-12 16:51:16' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '40', '0', '32', '2015-11-12 00:00:00', '2015-12-14 00:00:00', '3500000', 'NGN', '3519945', '0', '2015-11-12 13:54:47', 'R001764', '9920151112000055', '0130423', '     0167', '303', 'MANSIR H SADAUKI', 'MANSIR H SADAUKI', 'HOUSE 2, 13 WINSTON CHURCHILLSTREET,', 'GARKI AREA 2 PROTEA HOTEL, ASOKORO ABUJA, FCT', '0', NULL, NULL, '37', '2015-11-12 16:51:16' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '41', '0', '90', '2015-11-12 00:00:00', '2016-02-10 00:00:00', '100986301', 'NGN', '103597703', '0', '2015-11-12 15:49:58', 'C000493', '9920151112000093', '0130429', '    01100', '304', 'NASD', 'NASD PLC', '57, MARINA', 'LAGOS', '0', NULL, NULL, '38', '2015-11-12 16:51:16' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '42', '999', '30', '2015-11-11 00:00:00', '2099-03-13 00:00:00', '200000', 'NGN', '450003', '0', '2015-11-12 15:27:24', 'R000138', '9920151112000086', '0130426', '    01104', '305', 'ADELEKE A ADEMOLA', 'ADELEKE A ADEMOLA', '7, OYEDELE CLOSE, OFF BAJULAIYE RD,', 'MOROCCO, SHOMOLU.', '0', NULL, NULL, '39', '2015-11-12 16:51:17' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '43', '0', '90', '2015-11-05 00:00:00', '2016-02-03 00:00:00', '1800000', 'NGN', '1835471', '0', '2015-11-12 14:14:47', 'R001021', '9920151112000062', '0130424', '     0169', '303', 'AYOMIDE O TEMITOPE', 'AYOMIDE O TEMITOPE', '17, LANE H, 4TH AVE,PHASE 2,', NULL, '0', NULL, NULL, '40', '2015-11-12 16:51:17' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '44', '2', '30', '2015-11-04 00:00:00', '2016-02-03 00:00:00', '4902280', 'NGN', '5014690', '0', '2015-11-12 14:24:45', 'C000438', '9920151112000079', '0130425', '     0171', '302', 'LEVITE MICROFINANCE BANK LIMITED', 'LEVITE MICROFINANCE BANK LIMITED', '192, ABA ROAD,', 'PORT HARCOURT', '9', NULL, NULL, '41', '2015-11-12 16:51:17' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '45', '2', '30', '2015-11-04 00:00:00', '2016-02-03 00:00:00', '4902280', 'NGN', '5014690', '0', '2015-11-12 14:24:45', 'C000438', '9920151112000079', '0130425', '     0171', '302', 'LEVITE MICROFINANCE BANK LIMITED', 'LEVITE MICROFINANCE BANK LIMITED', '192, ABA ROAD,', 'PORT HARCOURT', '9', NULL, NULL, '42', '2015-11-12 16:51:17' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '46', '999', '30', '2015-10-09 00:00:00', '2099-02-08 00:00:00', '80381535', 'NGN', '214352228', '0', '2015-11-12 09:24:31', 'C000037', '9920151112000024', '0130420', '      015', '306', 'AFRICAN ALLIANCE INSURANCE CO. LTD', 'AFRICAN ALLIANCE INSURANCE CO. LTD', '61, MARINA LAGOS NIGERIA', 'NIGERIA', '0', NULL, NULL, '43', '2015-11-12 16:51:17' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '47', '1', '1', '2015-11-06 00:00:00', '2015-12-07 00:00:00', '1100000', 'NGN', '1104671', '0', '2015-11-12 16:27:59', 'R001686', '9920151112000114', '0130431', '    01117', '303', 'OLOLADE O JOSEPH', 'OLOLADE O JOSEPH', '14,EFUNTIDE STREET,AGUDA', 'OFF BROWN ROAD. SURULERE', '0', NULL, NULL, '44', '2015-11-12 16:51:17' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '48', '999', '30', '2015-11-12 00:00:00', '2099-03-14 00:00:00', '50000000', 'NGN', '112500685', '0', '2015-11-12 16:33:24', 'C000493', '9920151112000121', '0130432', '    01119', '306', 'NASD', 'NASD PLC', '57, MARINA', 'LAGOS', '0', NULL, NULL, '45', '2015-11-12 16:51:17' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '49', '0', '26', '2015-11-11 00:00:00', '2015-12-07 00:00:00', '200000', 'NGN', '200712', '0', '2015-11-12 16:04:25', 'R001686', '9920151112000107', '0130430', '    01121', '303', 'OLOLADE O JOSEPH', 'OLOLADE O JOSEPH', '14,EFUNTIDE STREET,AGUDA', 'OFF BROWN ROAD. SURULERE', '0', NULL, NULL, '46', '2015-11-12 16:51:17' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '50', '0', '32', '2015-11-13 00:00:00', '2015-12-15 00:00:00', '20501642', 'NGN', '20640941', '0', '2015-11-13 15:15:22', 'C000118', '9920151113000090', '0130463', '     0169', '304', 'MAYFAIR MICROFINANCE BANK LIMITED', 'MAYFAIR MICROFINANCE BANK LIMITED', 'NO 26, FUNSHO WILLIAM AVENUE', 'OJUELEGBA LAGOS', '0', NULL, NULL, '47', '2015-11-13 17:22:03' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '51', '0', '10', '2015-11-13 00:00:00', '2015-11-23 00:00:00', '400012329', 'NGN', '400998661', '0', '2015-11-13 15:00:01', 'C000063', '9920151113000076', '0130461', '     0170', '304', 'FIRST TRUSSTEES/ARM MONEY MARKET FU', 'ASSET RESOURCE MANAGEMENT', 'OFF OYINKAN ABAYOMI DRIVE', 'IKOYI', '0', NULL, NULL, '48', '2015-11-13 17:22:03' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '52', '0', '60', '2015-11-13 00:00:00', '2016-01-12 00:00:00', '28704799', 'NGN', '29152841', '0', '2015-11-13 14:56:14', 'C000063', '9920151113000069', '0130460', '     0171', '304', 'ARM SECURITIES', 'ASSET RESOURCE MANAGEMENT', 'OFF OYINKAN ABAYOMI DRIVE', 'IKOYI', '0', NULL, NULL, '49', '2015-11-13 17:22:03' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '53', '0', '18', '2015-11-13 00:00:00', '2015-12-01 00:00:00', '201635404', 'NGN', '202530334', '0', '2015-11-13 14:44:38', 'C000063', '9920151113000045', '0130458', '     0180', '304', 'ARM TRUSTEES/BAUCHI STATE BOND FUND', 'ASSET RESOURCE MANAGEMENT', 'OFF OYINKAN ABAYOMI DRIVE', 'IKOYI', '0', NULL, NULL, '50', '2015-11-13 17:22:03' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '54', '1', '18', '2015-09-29 00:00:00', '2015-11-16 00:00:00', '100000', 'NGN', '100658', '0', '2015-11-13 16:16:15', 'R000084', '9920151113000173', '0130471', '    01114', '303', 'CHIBUTUTU P C.', 'PAULIBNE CKUKWUNENYE CHIBUTUTU', 'UACN PLC, NIGER HOUSE', '1-5 ODUNLAMI STREET, MARINA', '0', NULL, NULL, '51', '2015-11-13 17:22:03' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '55', '1', '0', '2015-10-16 00:00:00', '2015-11-16 00:00:00', '50000', 'NGN', '50212', '0', '2015-11-13 16:21:14', 'R000084', '9920151113000180', '0130472', '    01116', '303', 'CHIBUTUTU P C.', 'PAULIBNE CKUKWUNENYE CHIBUTUTU', 'UACN PLC, NIGER HOUSE', '1-5 ODUNLAMI STREET, MARINA', '0', NULL, NULL, '52', '2015-11-13 17:22:04' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '56', '0', '32', '2015-11-02 00:00:00', '2015-12-04 00:00:00', '3000000', 'NGN', '3017096', '0', '2015-11-13 16:06:52', 'C000234', '9920151113000159', '0130469', '    01118', '304', 'ISLAND MICROFINANCE BANK LIMITED', 'ISLAND MICROFINANCE BANK LIMITED', '33,MOLONEY STREET OPP.KANM SELEM HOUSE', 'LAGOS', '0', NULL, NULL, '53', '2015-11-13 17:22:04' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '57', '0', '7', '2015-11-13 00:00:00', '2015-11-20 00:00:00', '18561730', 'NGN', '18589318', '0', '2015-11-13 15:05:32', 'C000231', '9920151113000083', '0130462', '     0166', '304', 'EKLOSIM LIMITED', 'EKLOSIM LIMITED', '21B KAFAYAT ABDULRASAK STREET', 'LEKKI PHASE ONE', '0', NULL, NULL, '54', '2015-11-13 17:22:04' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '58', '0', '60', '2015-11-13 00:00:00', '2016-01-12 00:00:00', '104295647', 'NGN', '105923558', '0', '2015-11-13 14:51:53', 'C000063', '9920151113000052', '0130459', '     0168', '304', 'ARM TRUSTEES / GOMBE STATE BOND', 'ASSET RESOURCE MANAGEMENT', 'OFF OYINKAN ABAYOMI DRIVE', 'IKOYI', '0', NULL, NULL, '55', '2015-11-13 17:22:04' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '59', '0', '31', '2015-11-13 00:00:00', '2015-12-14 00:00:00', '7179022', 'NGN', '7233897', '0', '2015-11-13 15:58:41', 'R000746', '9920151113000135', '0130467', '    01103', '303', 'ADEKEYE J ROTIMI', 'ADEKEYE J ROTIMI', 'OHIA ELIJIJI ESTATE', NULL, '0', NULL, NULL, '56', '2015-11-13 17:22:04' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '60', '0', '90', '2015-11-13 00:00:00', '2016-02-11 00:00:00', '15320609', 'NGN', '15631880', '0', '2015-11-13 11:57:00', 'R000090', '9920151113000014', '0130455', '     0126', '303', 'AKUJIEZE  NKEMAKONAM', 'AKUJIEZE  NKEMAKONAM', 'C\'O ASSOCIATED DISCOUNT HOUSE LTD', '6TH FLOOR, ST. NICHOLAS HOUSE', '0', NULL, NULL, '57', '2015-11-13 17:22:04' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '61', '0', '32', '2015-11-13 00:00:00', '2015-12-15 00:00:00', '1999994', 'NGN', '2011391', '0', '2015-11-13 12:04:37', 'R000430', '9920151113000021', '0130456', '     0127', '303', 'EJIO B LATEEF', 'EJIO B LATEEF', 'P O BOX 02 IJOKO', 'ABEOKUTA', '0', NULL, NULL, '58', '2015-11-13 17:22:04' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '62', '999', '30', '2015-11-13 00:00:00', '2099-03-15 00:00:00', '1000000', 'NGN', '2250014', '0', '2015-11-13 16:49:55', 'R000279', '9920151113000197', '0130473', '    01124', '305', 'TELLA H OLUBUNMI', 'TELLA H OLUBUNMI', '13 ADEKUNLE AKALA STREET', 'AJAO ESTATE', '0', NULL, NULL, '59', '2015-11-13 17:22:04' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '63', '0', '32', '2015-11-13 00:00:00', '2015-12-15 00:00:00', '5394171', 'NGN', '5424910', '0', '2015-11-13 16:55:44', 'R000279', '9920151113000201', '0130474', '    01125', '303', 'TELLA H OLUBUNMI', 'TELLA H OLUBUNMI', '13 ADEKUNLE AKALA STREET', 'AJAO ESTATE', '0', NULL, NULL, '60', '2015-11-13 17:22:05' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '64', '0', '31', '2015-11-13 00:00:00', '2015-12-14 00:00:00', '73115026', 'NGN', '73736003', '0', '2015-11-13 15:51:36', 'R000948', '9920151113000104', '0130464', '    01101', '303', 'UMEZURIKE H OKAFOR', 'UMEZURIKE H OKAFOR', '2 EZENWO WIKE CLS, MILE 4', NULL, '0', NULL, NULL, '61', '2015-11-13 17:22:05' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '65', '0', '32', '2015-11-02 00:00:00', '2015-12-04 00:00:00', '5000000', 'NGN', '5028493', '0', '2015-11-13 16:13:27', 'C000234', '9920151113000166', '0130470', '    01106', '304', 'ISLAND MICROFINANCE BANK LIMITED', 'ISLAND MICROFINANCE BANK LIMITED', '33,MOLONEY STREET OPP.KANM SELEM HOUSE', 'LAGOS', '0', NULL, NULL, '62', '2015-11-13 17:22:05' );
INSERT INTO `term_deposit`(`id`,`deposit_period_months`,`deposit_period_days`,`open_effective_date`,`maturity_date`,`deposit_amount`,`currency_code`,`maturity_amount`,`interest_rate`,`creation_date`,`customer_id`,`account_number`,`account_id`,`tran_id`,`scheme_code`,`funding_account_name`,`account_name`,`address_line1`,`address_line2`,`contract_rate`,`signed_by1_id`,`signed_by2_id`,`email_alert_id`,`last_update`) VALUES ( '66', '0', '32', '2015-11-02 00:00:00', '2015-12-04 00:00:00', '50000004', 'NGN', '50284934', '5', '2015-11-13 16:13:27', 'C000234', '9920151113000166', '0130470', '    01106', '301', 'TEST MICROFINANCE BANK', 'TEST MICROFINANCE BANK LIMITED', 'SAINT NICHOLAS HOSPITAL', 'LAGOS', '0', '2', '1', '63', '2015-11-20 12:14:26' );
-- ---------------------------------------------------------


-- Dump data of "user_history" -----------------------------
INSERT INTO `user_history`(`id`,`user_id`,`initiator_id`,`event_date`,`event`) VALUES ( '1', '2', '1', '2015-11-14 15:29:18', '2' );
-- ---------------------------------------------------------


-- Dump data of "user_table" -------------------------------
INSERT INTO `user_table`(`id`,`username`,`password_hash`,`is_enabled`,`is_first_login`,`role`) VALUES ( '1', 'torti', '$2a$10$RQW.WBn2s81rK0FeY0OvH.yxDvE2JJTBRkiHgHBoCkMd5It2owP2q', '1', '0', 'ADMIN' );
INSERT INTO `user_table`(`id`,`username`,`password_hash`,`is_enabled`,`is_first_login`,`role`) VALUES ( '2', 'chigozirim@torti.com', '$2a$10$J0oHIRkc4UosBT33laU1u.m8Fk8Qj9l0.000jStpnFXc4dJQAQN5q', '1', '1', 'ADMIN' );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_alert_transaction_emailAlert_2" --------
CREATE INDEX `ix_alert_transaction_emailAlert_2` USING BTREE ON `alert_transaction`( `email_alert_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_alert_transaction_smsAlert_1" ----------
CREATE INDEX `ix_alert_transaction_smsAlert_1` USING BTREE ON `alert_transaction`( `sms_alert_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_bulk_alert_initiatedBy_3" --------------
CREATE INDEX `ix_bulk_alert_initiatedBy_3` USING BTREE ON `bulk_alert`( `initiated_by_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_bulk_alert_list_createdBy_4" -----------
CREATE INDEX `ix_bulk_alert_list_createdBy_4` USING BTREE ON `bulk_alert_list`( `created_by_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_email_alert_attachment_email_5" --------
CREATE INDEX `ix_email_alert_attachment_email_5` USING BTREE ON `email_alert_attachment`( `email_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_login_audit_trail_user_6" --------------
CREATE INDEX `ix_login_audit_trail_user_6` USING BTREE ON `login_audit_trail`( `user_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_term_deposit_emailAlert_7" -------------
CREATE INDEX `ix_term_deposit_emailAlert_7` USING BTREE ON `term_deposit`( `email_alert_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_user_history_initiator_9" --------------
CREATE INDEX `ix_user_history_initiator_9` USING BTREE ON `user_history`( `initiator_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "ix_user_history_user_8" -------------------
CREATE INDEX `ix_user_history_user_8` USING BTREE ON `user_history`( `user_id` );
-- ---------------------------------------------------------


-- CREATE LINK "fk_alert_transaction_emailAlert_2" ---------
ALTER TABLE `alert_transaction`
	ADD CONSTRAINT `fk_alert_transaction_emailAlert_2` FOREIGN KEY ( `email_alert_id` )
	REFERENCES `email_alert`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


-- CREATE LINK "fk_alert_transaction_smsAlert_1" -----------
ALTER TABLE `alert_transaction`
	ADD CONSTRAINT `fk_alert_transaction_smsAlert_1` FOREIGN KEY ( `sms_alert_id` )
	REFERENCES `sms_alert`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


-- CREATE LINK "fk_bulk_alert_initiatedBy_3" ---------------
ALTER TABLE `bulk_alert`
	ADD CONSTRAINT `fk_bulk_alert_initiatedBy_3` FOREIGN KEY ( `initiated_by_id` )
	REFERENCES `user_table`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


-- CREATE LINK "fk_bulk_alert_list_createdBy_4" ------------
ALTER TABLE `bulk_alert_list`
	ADD CONSTRAINT `fk_bulk_alert_list_createdBy_4` FOREIGN KEY ( `created_by_id` )
	REFERENCES `user_table`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


-- CREATE LINK "fk_email_alert_attachment_email_5" ---------
ALTER TABLE `email_alert_attachment`
	ADD CONSTRAINT `fk_email_alert_attachment_email_5` FOREIGN KEY ( `email_id` )
	REFERENCES `email_alert`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


-- CREATE LINK "fk_login_audit_trail_user_6" ---------------
ALTER TABLE `login_audit_trail`
	ADD CONSTRAINT `fk_login_audit_trail_user_6` FOREIGN KEY ( `user_id` )
	REFERENCES `user_table`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


-- CREATE LINK "fk_term_deposit_emailAlert_7" --------------
ALTER TABLE `term_deposit`
	ADD CONSTRAINT `fk_term_deposit_emailAlert_7` FOREIGN KEY ( `email_alert_id` )
	REFERENCES `email_alert`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


-- CREATE LINK "fk_user_history_initiator_9" ---------------
ALTER TABLE `user_history`
	ADD CONSTRAINT `fk_user_history_initiator_9` FOREIGN KEY ( `initiator_id` )
	REFERENCES `user_table`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


-- CREATE LINK "fk_user_history_user_8" --------------------
ALTER TABLE `user_history`
	ADD CONSTRAINT `fk_user_history_user_8` FOREIGN KEY ( `user_id` )
	REFERENCES `user_table`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


