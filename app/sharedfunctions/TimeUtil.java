package sharedfunctions;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.HashMap;

/**
 * Created by chigozirim on 10/31/16.
 */
public class TimeUtil {
    public enum TimeDetail{
        DATE_ONLY, DATE_AND_TIME, DATE_AND_TIME_DETAILED
    }
    private static TimeUtil instance = new TimeUtil();
    private final HashMap<TimeString, String> TIMESTRINGS_MAP = new HashMap<TimeString, String>();
    public static enum TimeString {TIMEBEFORE_NOW, TIMEBEFORE_MINUTE, TIMEBEFORE_MINUTES,
    TIMEBEFORE_HOUR, TIMEBEFORE_HOURS, TIMEBEFORE_YESTERDAY, TIMEBEFORE_FORMAT, TIMEBEFORE_FORMAT_YEAR, TIMEBEFORE_TIMEAMPM, TIMEBEFORE_DATE_TIME}

    private TimeUtil(){TIMESTRINGS_MAP.put(TimeString.TIMEBEFORE_NOW, "now");
        TIMESTRINGS_MAP.put(TimeString.TIMEBEFORE_MINUTE, "%d minute%s ago");
        TIMESTRINGS_MAP.put(TimeString.TIMEBEFORE_HOUR, "%d hour%s ago");
        TIMESTRINGS_MAP.put(TimeString.TIMEBEFORE_YESTERDAY, "yesterday");
        TIMESTRINGS_MAP.put(TimeString.TIMEBEFORE_FORMAT, "MMM d");
        TIMESTRINGS_MAP.put(TimeString.TIMEBEFORE_FORMAT_YEAR, "MMM dd yyyy");
        TIMESTRINGS_MAP.put(TimeString.TIMEBEFORE_TIMEAMPM, "hh:mm aaa");
        TIMESTRINGS_MAP.put(TimeString.TIMEBEFORE_DATE_TIME, "dd-MM-yyyy 'at' hh:mm aaa");
    }

    public static TimeUtil getInstance(){
        if(instance == null){
            instance = new TimeUtil();
        }
        return instance;
    }


    public String getRelativeTimeString(LocalDateTime time){
        String output = "";
        long now = LocalDateTime.now().toDate().getTime();
        long timeLong = time.toDate().getTime();
        long diff = (now - timeLong) / 1000; //this returns the difference between the times in seconds

        return getRelativeTimeStringDateOnly(time, diff);
    }

    /** This method returns a pretty printed version of a date. For example,
     * it might return now, yesterday, 10 minutes ago etc depending on the time
     * contained in <b>time</b> in relation to the current time.
     * @param time The time we want to process
     * @return
     */
    public String getRelativeTimeString(LocalDateTime time, TimeDetail timeDetail){
        if(time ==null || timeDetail == null){
            throw new NullPointerException("Time or timeDetail is null");
        }
        String output = "";
        long now = LocalDateTime.now().toDate().getTime();
        long timeLong = time.toDate().getTime();
        long diff = (now - timeLong) / 1000; //this returns the difference between the times in seconds

        switch(timeDetail){
            case DATE_ONLY:
                return getRelativeTimeStringDateOnly(time,diff);
            case DATE_AND_TIME_DETAILED:
                return getRelativeTimeStringDateAndTime(time,diff);
            case DATE_AND_TIME:
                return getRelativeTimeStringDateAndTimeDetailed(time, diff);
            default:
                return getRelativeTimeStringDateOnly(time, diff);
        }


    }


    private String getRelativeTimeStringDateOnly(LocalDateTime time, long diff){
        long timeLong = time.toDate().getTime();
        if( diff < 60 ) { // it happened now
            return TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_NOW);
        }
        else if( diff < 3600 ) { // it happened X minutes ago
            int minutes = (int) Math.round(diff/60.0);
            return String.format(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_MINUTE), minutes, (minutes==1) ? "": "s");
        }
        else if( diff < 3600 * 24 ) { // it happened X hours ago
            int hours = (int) Math.round(diff / 3600);
            return String.format(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_HOUR), hours, (hours==1) ? "": "s");
        }
        else if( diff < 3600 * 24 * 2 ) { // it happened yesterday
            return TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_YESTERDAY);
        }
        else {// falling back on a usual date format as it happened later than yesterday
            if(time.getYear() == LocalDate.now().getYear()){
                return DateTimeFormat.forPattern(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_FORMAT)).print(timeLong);
            }else{
                return DateTimeFormat.forPattern(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_FORMAT_YEAR)).print(timeLong);
            }
        }
    }

    private String getRelativeTimeStringDateAndTime(LocalDateTime time, long diff){
        long timeLong = time.toDate().getTime();
        String formattedTime = DateTimeFormat.forPattern(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_TIMEAMPM)).print(time);
        if( diff < 60 ) { // it happened now
            return TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_NOW);
        }
        else if( diff < 3600 ) { // it happened X minutes ago
            int minutes = (int) Math.round(diff/60.0);
            return String.format(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_MINUTE), minutes, (minutes==1) ? "": "s");
        }
        else if( diff < 3600 * 24 ) { // it happened X hours ago
            int hours = (int) Math.round(diff / 3600);
            return String.format("%s at %s", "Today",  formattedTime);
        }
        else if( diff < 3600 * 24 * 2 ) { // it happened yesterday
            return String.format("%s at %s", "Yesterday", formattedTime);
        }
        else {// falling back on a usual date format as it happened later than yesterday
            if(time.getYear() == LocalDate.now().getYear()){
                return String.format("%s at %s", DateTimeFormat.forPattern(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_FORMAT)).print(timeLong), formattedTime);
            }else{
                return String.format("%s at %s", DateTimeFormat.forPattern(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_FORMAT_YEAR)).print(timeLong), formattedTime);
            }
        }
    }

    private String getRelativeTimeStringDateAndTimeDetailed(LocalDateTime time, long diff){
        long timeLong = time.toDate().getTime();
        String formattedDate = DateTimeFormat.forPattern(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_TIMEAMPM)).print(time);

        if( diff < 60 ) { // it happened now
            return TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_NOW);
        }
        else if( diff < 3600 ) { // it happened X minutes ago
            int minutes = (int) Math.round(diff/60.0);
            return String.format(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_MINUTE), minutes, (minutes==1) ? "": "s") + ": " + formattedDate;
        }
        else if( diff < 3600 * 24 ) { // it happened X hours ago
            int hours = (int) Math.round(diff / 3600);
            return String.format(TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_HOUR), hours, (hours==1) ? "": "s") + ": " + formattedDate;
        }
        else if( diff < 3600 * 24 * 2 ) { // it happened yesterday
            return TIMESTRINGS_MAP.get(TimeString.TIMEBEFORE_YESTERDAY) + ": " + formattedDate;
        }
        else {// falling back on a usual date format as it happened later than yesterday
            if(time.getYear() == LocalDate.now().getYear()){
                return formattedDate;
            }else{
                return formattedDate;
            }
        }
    }

    public static boolean isToday(LocalDateTime date) {
        LocalDateTime lastUpdate = date;
        LocalDateTime now = LocalDateTime.now();
        return lastUpdate.toLocalDate().equals(now.toLocalDate());
    }
}
