package sharedfunctions

import external.Account

/**
  * Created by chigozirim on 11/23/16.
  */
object FrontEndUtils {
  /** This produces the currency sign of the
    * currency code contained in the account object passed in
    * @param currencyCode
    * @return
    */
  def currency(currencyCode: String): String ={
    currencyCode match{
      case "NGN" =>
        "N";
      case "USD" =>
        "$";
      case "GBP" =>
        "£";
      case "EUR" =>
        "€";
      case "CAD" =>
        "$";
      case _ =>
        currencyCode;
    }
  }
}
