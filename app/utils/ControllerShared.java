package utils;

import models.User;
import play.mvc.Controller;

/**
 * Created by wale on 11/27/14.
 */
public abstract class ControllerShared extends Controller {
    public  HostMaster hm = new HostMaster();
    public User currentUser = hm.getCurrentUser();
}
