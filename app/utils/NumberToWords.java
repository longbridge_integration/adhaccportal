package utils;

import play.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

/**
 */
public class NumberToWords {

    static HashMap<Integer, String> numberMap = new HashMap();
//    static StringBuffer builder = new StringBuffer();

    static String get(int number){
        if(numberMap.containsKey(number)){
            return numberMap.get(number);
        }else{
            return noToWords(number).trim();
        }
    }
    static {
        numberMap.put(1, "One");
        numberMap.put(2, "Two");
        numberMap.put(3, "Three");
        numberMap.put(4, "Four");
        numberMap.put(5, "Five");
        numberMap.put(6, "Six");
        numberMap.put(7, "Seven");
        numberMap.put(8, "Eight");
        numberMap.put(9, "Nine");
        numberMap.put(10, "Ten");
        numberMap.put(11, "Eleven");
        numberMap.put(12, "Twelve");
        numberMap.put(13, "Thirteen");
        numberMap.put(14, "Fourteen");
        numberMap.put(15, "Fifteen");
        numberMap.put(16, "Sixteen");
        numberMap.put(17, "Seventeen");
        numberMap.put(18, "Eighteen");
        numberMap.put(19, "Nineteen");
        numberMap.put(20, "Twenty");
        numberMap.put(30, "Thirty");
        numberMap.put(40, "Forty");
        numberMap.put(50, "Fifty");
        numberMap.put(60, "Sixty");
        numberMap.put(70, "Seventy");
        numberMap.put(80, "Eighty");
        numberMap.put(90, "Ninety");
        numberMap.put(100, "Hundred");
        numberMap.put(1000, "Thousand");
        numberMap.put(1000000, "Million");
        numberMap.put(1000000000, "Billion");
//        numberMap.put(1000000000000, "trillion");
    }

    public static String noToWords(BigDecimal number){
        number = number.setScale(2, RoundingMode.DOWN);

        int naira = Integer.parseInt(number.toString().split("\\.")[0]);

        String output = noToWords(naira).concat(" Naira ");

        int kobo = Integer.parseInt(number.toString().split("\\.")[1]);

        Logger.info(" Found kobo {}",kobo);

        if(kobo !=0){
            output= output.concat(" and ").concat(noToWords(kobo)).concat(" Kobo ");
        }
        output = output.concat(" only ").replace("Zero","").toLowerCase();

        return output.substring(0,1).toUpperCase().concat(output.substring(1));
    }

    public static String noToWords(int number) {
        StringBuilder builder = new StringBuilder();
        builder.delete(0, builder.capacity() - 1);
        int rem = number;
        int quotient = 0;
        //small numbers
        if (number <= 20) {
            return get(number);
        } else if (number < 100) {
            quotient = rem / 10;
            rem = rem % 10;
            return String.format("%s %s ", get(quotient * 10), ((rem > 0) ? get(rem) : ""));
        }
        //larger numbers

        //billion
        quotient = rem / 1000000000;
        rem = rem % 1000000000;
        if(quotient > 0){
            builder.append(String.format("%s %s ", get(quotient), get(1000000000)));
        }

        //million
        quotient = rem / 1000000;
        rem = rem % 1000000;
        if(quotient > 0){
            builder.append(String.format("%s %s ", get(quotient), get(1000000)));
        }
        quotient = rem / 1000;
        rem = rem % 1000;
        if (quotient > 0) {
            builder.append(String.format("%s %s ", get(quotient), get(1000)));
        }
        quotient = rem / 100;
        rem = rem % 100;
        if (quotient > 0) {
            builder.append(String.format("%s %s ", get(quotient), get(100)));
        }
        if (rem == 0) {
            return builder.toString();
        } else {
            if (rem <= 20) {
                builder.append(String.format("and %s ", get(rem)));
            } else {
                quotient = rem / 10;
                rem = rem % 10;
                builder.append(String.format("and %s %s ", get(quotient * 10), ((rem > 0) ? get(rem) : "")));
            }
        }
        return builder.toString().trim();
    }

}