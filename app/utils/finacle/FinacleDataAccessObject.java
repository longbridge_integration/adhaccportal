package utils.finacle;

import com.avaje.ebean.Expr;
import external.Account;
import external.Customer;
import external.CustomerContact;
import models.Address;
import models.Transaction;
import play.Logger;
import play.db.DB;
import utils.AccountStatementsPDFHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by chigozirim on 19/04/2016.
 */
public class FinacleDataAccessObject {
    private static final String transactionSql = "select h.tran_date, h.tran_particular,h.tran_amt, h.part_tran_type, h.value_date, h.tran_id, h.tran_date\n" +
            "\n" +
            "from tbaadm.htd h where h.RCRE_TIME BETWEEN TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS') \n" +
            "AND TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS')\n" +
            " h.acid = '%s' and g.cif_id is not null";

//
//    private static final String singleDayTransactionSql = "select c.pstd_date, c.tran_particular,h.tran_amt, c.part_tran_type, c.value_date, c.tran_id, c.tran_date, c.TRAN_RMKS \n" +
//            "       from tbaadm.ctd_dtd_acli_view c where c.acid = '%s' and c.pstd_date like TO_DATE('%s', 'MM/DD/YYYY') and c.pstd_flg = 'Y' order by c.pstd_date asc";


    private static final String singleDayTransactionSql = "select h.tran_date, h.tran_particular,h.tran_amt, h.part_tran_type, h.value_date, h.tran_id, h.tran_date, h.TRAN_RMKS \n" +
            "       from tbaadm.htd h where h.acid = '%s' and h.tran_date like TO_DATE('%s', 'MM/DD/YYYY') and h.pstd_flg = 'Y' order by h.tran_date asc";


//    private static final String singleDayTransactionSqlToday = "select h.pstd_date, h.tran_particular,h.tran_amt, h.part_tran_type, h.value_date, h.tran_id, h.tran_date, h.TRAN_RMKS \n" +
//            "       from tbaadm.dtd h where h.acid = '%s' and h.pstd_flg = 'Y' order by h.lchg_time asc";

    private static final String singleDayTransactionSqlToday = "select h.tran_date, h.tran_particular,h.tran_amt, h.part_tran_type, h.value_date, h.tran_id, h.tran_date, h.TRAN_RMKS \n" +
            "from tbaadm.dtd h where h.acid = '%s' and h.pstd_flg = 'Y' order by h.tran_date asc";


    private static final String lastBalanceSQL = "select * from (select tran_date_bal, eod_date from tbaadm.eab where acid = '%s' " +
            "and eod_date\n" +
            " < TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS') order by eod_date desc) where rownum <=1";
    private static final String accountBalanceSql = "select * from (select e.tran_date_bal, g.acct_name, g.foracid, g.schm_type from tbaadm.eab e," +
            "tbaadm.gam g where e.acid = g.acid and g.acid=%s and g.schm_type in ('CAA', 'SBA', 'ODA') and schm_code <> '112' " +
            "and e.EOD_DATE <= TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS') order by e.eod_date desc) where rownum <= 1";

    //    private static final String accountBalanceSql = "select clr_bal_amt, acct_name, foracid, schm_type from tbaadm.gam where cif_id = ? and schm_type in ('CAA', 'SBA', 'ODA')";
    public static final String fetchAccountsSql = "select foracid, acct_name, acid, schm_type" +
            " from tbaadm.gam where cif_id = ? and schm_type in ('CAA', 'SBA', 'ODA') and schm_code <> '112' ";
    public static final String fetchAccountsSqlAccountNumber = "select foracid, acct_name, acid, schm_type " +
            "from tbaadm.gam where foracid = ? and schm_type in ('CAA', 'SBA', 'ODA') and schm_code <> '112' ";
    public static final String fetchAccountSqlNewAccountNumber = "select foracid, acct_name, acid, schm_type from tbaadm.gam where " +
            " foracid in (select newacct from tbaadm.acctmap where oldacct = '%s' ) and schm_type in ('CAA', 'SBA', 'ODA') and schm_code <> '112' ";

    //get customer details
    public static final String fetchCustomerSql = "select cust_last_name, cust_first_name, orgkey " +
            " from crmuser.accounts where orgkey = ?";

    public static final String fetchCustomerSqlWithAccountNumber = "select cust_last_name, cust_first_name, orgkey from " +
            " crmuser.accounts where orgkey in (select distinct cif_id from tbaadm.gam " +
            " where foracid = ? and schm_type in ('CAA', 'SBA', 'ODA') and schm_code <> '112' )";
    //new account implementation
    public static final String fetchCustomerSqlNewAccountNumber = "select cust_last_name, cust_first_name, orgkey from " +
            " crmuser.accounts where orgkey in (select cif_id from tbaadm.gam where " +
            " foracid in (select newacct from tbaadm.acctmap where oldacct = '%s' ) and schm_type in ('CAA', 'SBA', 'ODA') and schm_code <> '112' )";


    public static final SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");

    // 9920160219000154

    public static String getDateString(String date) {
        return String.format("%s/%s/%s", date.substring(5, 7), date.substring(8, 10), date.substring(0, 4));
    }

    public static class CustomerDAO {

        private static final String addressSql = "SELECT ADDRESS_LINE1, ADDRESS_LINE2, ADDRESS_LINE3 FROM CRMUSER.ADDRESS WHERE ORGKEY = ? and preferredaddress='Y'";
        private static final String addressSqlAccountName = "SELECT ADDRESS_LINE1, ADDRESS_LINE2, ADDRESS_LINE3, ORGKEY FROM CRMUSER.ADDRESS WHERE ORGKEY = (select CIF_ID " +
                " FROM TBAADM.GAM WHERE ACCT_NAME = ?) and preferredaddress='Y'";

        /**
         * This returns the Address of the customer with the specified customerId.
         * The three address lines are fetched from finacle db and used to create an
         * <b>Address</b>
         *
         * @param customerId The customer id of the Customer
         * @return
         */
        public static Address getCustomerAddress(String customerId) {
            Address address = null;
            try (Connection connection = DB.getConnection("secondary");
                 PreparedStatement preparedStatement = connection.prepareStatement(addressSql)) {
                preparedStatement.setString(1, customerId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        address = new Address(resultSet.getString(1),
                                resultSet.getString(2), resultSet.getString(3), "", customerId, "");
                    }
                }
            } catch (Exception ex) {
                Logger.error("An error occurred while fetching the customer address: aborting");
                ex.printStackTrace();
            }
            return address;
        }

        /**
         * This returns the Address of the customer with the specified account name.
         * The three address lines are fetched from finacle db and used to create an
         * <b>Address</b>
         *
         * @param accountName The account name of the account
         * @return
         */
        public static Address getCustomerAddressFromAccountName(String accountName) {
            Address address = null;
            try (Connection connection = DB.getConnection("secondary");
                 PreparedStatement preparedStatement = connection.prepareStatement(addressSqlAccountName)) {
                preparedStatement.setString(1, accountName);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        address = new Address(resultSet.getString(1),
                                resultSet.getString(2), resultSet.getString(3), "", resultSet.getString(4), "");
                    }
                }
            } catch (Exception ex) {
                Logger.error("An error occurred while fetching the customer address: aborting", ex);
            }
            return address;
        }

        public static List<Customer> getAssociatedCustomer(String searchBy) {
            Logger.info("In side get associated customer");
            searchBy = searchBy.toUpperCase();
            //check if it is an account number
            if (searchBy.charAt(0) == '-') {
                //it is an account number
                //remove the "-"
                searchBy = searchBy.substring(1);
                List<Customer> returned = new ArrayList<>();

                try (Connection connection = DB.getConnection("secondary");
                     PreparedStatement accountsStatement = connection.prepareStatement(fetchCustomerSqlWithAccountNumber);) {
                    accountsStatement.setString(1, searchBy.toUpperCase());

                    try (ResultSet accountStatementsResultSet = accountsStatement.executeQuery()) {
                        Logger.info(fetchCustomerSqlWithAccountNumber);
                        boolean accountFound = false;
                        //Builder stringbuilder
                        while (accountStatementsResultSet.next()) {
                            accountFound = true;
                            Customer customer = new Customer();
                            customer.lastName = accountStatementsResultSet.getString(1);
                            customer.firstName = accountStatementsResultSet.getString(2);
                            customer.customerId = accountStatementsResultSet.getString(3);
                            Logger.info("Customer name : {} Customer cif:{}", customer.lastName, customer.customerId);
                            returned.add(customer);
                        }

                        if (!accountFound) {
                            //check if it's an old account
                            accountStatementsResultSet.close();
                            accountsStatement.close();
                            String sql1 = String.format(fetchCustomerSqlNewAccountNumber, searchBy.toUpperCase());
                            Logger.info(sql1);
                            try (PreparedStatement statement = connection.prepareStatement(sql1)) {
                                Logger.info(sql1);
                                accountFound = false;
                                Logger.info("Account was not found in gam table");

                                try (ResultSet statementResultSet = statement.executeQuery()) {
                                    while (statementResultSet.next()) {
                                        accountFound = true;
                                        Customer customer = new Customer();
                                        customer.lastName = accountStatementsResultSet.getString(1);
                                        customer.firstName = accountStatementsResultSet.getString(2);
                                        customer.customerId = accountStatementsResultSet.getString(3);
                                        Logger.info("Customer name : {} Customer cif:{}", customer.lastName, customer.customerId);
                                        returned.add(customer);
                                        Logger.info("Account was found in new table map");
                                    }
                                }

                                if (!accountFound) {
                                    Logger.info("Account was not found in the new table map");

                                    Customer customer = new Customer();
                                    customer.firstName = "Account doesn't";
                                    customer.lastName = "exist";
                                    customer.customerId = "null";
                                    returned.add(customer);
                                }
                            }
                        }
                    }
                } catch (Exception exc) {
                    Logger.error("Error", exc);
                    Customer customer = new Customer();
                    customer.firstName = "Database ";
                    customer.lastName = "Error";
                    customer.customerId = "null";
                    returned.add(customer);
                }
                return returned;
            } else {
                List<Customer> returned = new ArrayList<>();
                try (Connection connection = DB.getConnection("secondary");
                     PreparedStatement accountsStatement = connection.prepareStatement(fetchCustomerSql)) {
                    accountsStatement.setString(1, searchBy.toUpperCase());
                    try (ResultSet accountStatementsResultSet = accountsStatement.executeQuery()) {
                        Logger.info(fetchAccountsSqlAccountNumber);
                        boolean cifIdFound = false;
                        //Builder stringbuilder
                        while (accountStatementsResultSet.next()) {
                            cifIdFound = true;
                            Customer customer = new Customer();
                            customer.lastName = accountStatementsResultSet.getString(1);
                            customer.firstName = accountStatementsResultSet.getString(2);
                            customer.customerId = accountStatementsResultSet.getString(3);
                            returned.add(customer);
                        }
                        if (!cifIdFound) {
                            Customer customer = new Customer();
                            customer.firstName = "CIF ID";
                            customer.lastName = " doesn't exist";
                            customer.customerId = "null";
                            returned.add(customer);
                        }
                    }
                } catch (Exception exc) {
                    Logger.error("No accounts fetched");
                    Customer customer = new Customer();
                    customer.firstName = "Error fetching";
                    customer.lastName = "Customer";
                    customer.customerId = "null";
                    returned.add(customer);
                }
                return returned;
            }
        }

        public static List<Account> getAllAssociatedAccounts(String searchBy) {
            Logger.info("In side get all associated accounts");
            searchBy = searchBy.toUpperCase();
            Logger.info("customer id: {}", searchBy);
            //check if it is an account number
            if (searchBy.charAt(0) == '-') {
                //it is an account number
                //remove the "-"
                searchBy = searchBy.substring(1);
                Logger.info("{}", searchBy);
                List<Account> returned = new ArrayList<Account>();
                try (Connection connection = DB.getConnection("secondary");
                     PreparedStatement accountsStatement = connection.prepareStatement(fetchAccountsSqlAccountNumber);) {

                    accountsStatement.setString(1, searchBy.toUpperCase());
                    try (ResultSet accountStatementsResultSet = accountsStatement.executeQuery();) {
                        Logger.info(fetchAccountsSqlAccountNumber);
                        boolean accountFound = false;
                        //Builder stringbuilder
                        while (accountStatementsResultSet.next()) {
                            Logger.info("Data was found in accountstatementsresult set");
                            accountFound = true;
                            Account account = new Account();
                            account.accountNumber = accountStatementsResultSet.getString(1);
                            account.accountName = accountStatementsResultSet.getString(2);
                            account.accountId = accountStatementsResultSet.getString(3);
                            account.schemeType = accountStatementsResultSet.getString(4);
                            returned.add(account);
                        }

                        if (!accountFound) {
                            //check if it's an old account
                            accountStatementsResultSet.close();
                            accountsStatement.close();
                            String sql1 = String.format(fetchAccountSqlNewAccountNumber, searchBy.toUpperCase());
                            try (PreparedStatement statement = connection.prepareStatement(sql1)) {
                                Logger.info(sql1);
                                accountFound = false;
                                Logger.info("Account was not found in gam table");

                                try (ResultSet statementResultSet = statement.executeQuery()) {
                                    while (statementResultSet.next()) {
                                        accountFound = true;
                                        Account account = new Account();
                                        account.accountNumber = accountStatementsResultSet.getString(1);
                                        account.accountName = accountStatementsResultSet.getString(2);
                                        account.accountId = accountStatementsResultSet.getString(3);
                                        account.schemeType = accountStatementsResultSet.getString(4);
                                        returned.add(account);
                                        Logger.info("Account was found in new table map");
                                    }
                                }

                                if (!accountFound) {
                                    Logger.info("Account was not found in the new table map");
                                    Account account = new Account();
                                    account.accountName = "Account doesn't exist";
                                    returned.add(account);
                                }
                            }
                        }
                    }
                } catch (Exception exc) {
                    Logger.error("Error", exc);
                    Account account = new Account();
                    account.accountName = "Database Error";
                    returned.add(account);
                }
                return returned;
            } else {
                List<Account> returned = new ArrayList<>();
                try (Connection connection = DB.getConnection("secondary");
                     PreparedStatement accountsStatement = connection.prepareStatement(fetchAccountsSql)) {

                    accountsStatement.setString(1, searchBy.toUpperCase());
                    try (ResultSet accountStatementsResultSet = accountsStatement.executeQuery()) {
                        Logger.info(fetchAccountsSqlAccountNumber);
                        boolean cifIdFound = false;
                        //Builder stringbuilder
                        while (accountStatementsResultSet.next()) {
                            cifIdFound = true;
                            Account account = new Account();
                            account.accountName = accountStatementsResultSet.getString(2);
                            account.accountNumber = accountStatementsResultSet.getString(1);
                            account.accountId = accountStatementsResultSet.getString(3);
                            account.schemeType = accountStatementsResultSet.getString(4);
                            returned.add(account);
                        }
                        if (!cifIdFound) {
                            Account account = new Account();
                            account.accountName = "CIF ID doesn't exist";
                            returned.add(account);
                        }
                    }
                } catch (Exception exc) {
                    Logger.error("No accounts fetched");
                    Account account = new Account();
                    account.accountName = "Error fetching accounts";
                    returned.add(account);
                }
                return returned;
            }
        }


        public static List<String> getAllCustomersWithEmails() {
            List<CustomerContact> contacts = CustomerContact.find.where().and(Expr.eq("PHONEOREMAIL", "EMAIL"), Expr.eq("PREFERREDFLAG", "Y")).findList();
            List<String> customerIds = new ArrayList<String>();
            for (CustomerContact contact : contacts) {
                customerIds.add(contact.customerId);
            }
            return customerIds;
        }
    }

    public static class AccountStatementDAO {

        //new account implementation
        public static final String fetchTDAccountsSql = "select foracid, acct_name, acid from tbaadm.gam where cif_id = '%s' and schm_type in ('TDA') " +
                "   and (acct_cls_date >= " +
                " TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS') or acct_cls_date is null )";
        //    private static final String rateSQL = "  select distinct \n" +
        //            "(CUST_CR_PREF_PCNT + ID_CR_PREF_PCNT + NRML_PCNT_CR + CHNL_CR_PREF_PCNT+ NRML_INT_PCNT+ BASE_PCNT_CR) from tbaadm.itc a, (select entity_id, max(INT_TBL_CODE_SRL_NUM) itc_max_INT_TBL_CODE_SRL_NUM from tbaadm.itc\n" +
        //            "where entity_cre_flg = 'Y' and del_flg = 'N'\n" +
        //            "group by entity_id) b, tbaadm.icv c,\n" +
        //            "(select int_tbl_code, crncy_code, max(INT_TBL_VER_NUM) icv_max_INT_TBL_VER_NUM from tbaadm.icv\n" +
        //            "where entity_cre_flg = 'Y' and del_flg = 'N'\n" +
        //            "group by int_tbl_code, crncy_code) d, tbaadm.tvs e,\n" +
        //            "(select int_tbl_code, INT_SLAB_DR_CR_FLG, crncy_code, max(INT_TBL_VER_NUM) ivs_max_INT_TBL_VER_NUM from tbaadm.tvs\n" +
        //            "where entity_cre_flg = 'Y' and del_flg = 'N'\n" +
        //            "group by int_tbl_code, INT_SLAB_DR_CR_FLG, crncy_code) f, tbaadm.gam g\n" +
        //            "where a.entity_id = b.entity_id\n" +
        //            "and a.entity_id = g.acid\n" +
        //            "and a.INT_TBL_CODE_SRL_NUM = b.itc_max_INT_TBL_CODE_SRL_NUM\n" +
        //            "and a.int_tbl_code = c.int_tbl_code\n" +
        //            "and c.INT_TBL_VER_NUM = d.icv_max_INT_TBL_VER_NUM\n" +
        //            "and c.crncy_code = e.crncy_code\n" +
        //            "and c.int_tbl_code = e.int_tbl_code\n" +
        //            "and e.INT_TBL_VER_NUM = f.ivs_max_INT_TBL_VER_NUM\n" +
        //            "and e.INT_SLAB_DR_CR_FLG = f.INT_SLAB_DR_CR_FLG\n" +
        //            "and e.int_tbl_code = f.int_tbl_code\n" +
        //            "and e.crncy_code = f.crncy_code\n" +
        //            "and foracid = ?\n" +
        //            "and e.INT_SLAB_DR_CR_FLG = 'C'";
        private static final String tdAccountBalanceSql = "select g.schm_code, G.clr_bal_amt,t.deposit_period_days, t.maturity_date,\n" +
                "e.interest_rate,  g.foracid,g.acct_name from tbaadm.tam t, tbaadm.eit e, tbaadm.gam g, tbaadm.htd d " +
                "where t.acid=e.entity_id and g.acid=t.acid     \n" +
                "and g.acid=e.entity_id and g.clr_bal_amt > 0 and g.entity_cre_flg!='N' and g.acid=d.acid and g.cif_id=?";
        private static final String tdAccountBalanceSQL = "select g.schm_code, e.tran_date_bal, t.deposit_period_days, " +
                "t.maturity_date," +
                " f.interest_rate, g.foracid, g.acct_name, I.ID_CR_PREF_PCNT from tbaadm.eab e," +
                "tbaadm.gam g,tbaadm.tam t,tbaadm.eit f, tbaadm.itc i where e.acid = g.acid and t.acid = g.acid and g.acid='%s' " +
                "and g.acid=i.entity_id " +
                "and g.schm_type in ('TDA')" +
                "and e.EOD_DATE <= TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS') and " +
                "e.tran_date_bal > 0.0 order by e.eod_date desc";

        private static AccountStatementsPDFHelper.BalanceStatement getBalanceStatement() {
            return null;
        }

        /**
         * This fetches the balance of the account specified by the acid
         * as at the fromDate.
         *
         * @param acid     The account id of the account
         * @param fromDate The date from which to get the initial balance
         * @return The balance of the account as at fromDate
         */
        public static double getInitialBalance(String acid, String fromDate) {
            double initialBalance = -0.1;
            String sql = String.format(lastBalanceSQL, acid,
                    getDateString(fromDate));
            Logger.info(sql);
            try (Connection connection = DB.getConnection("secondary");
                 PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setMaxRows(2);
                try (ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        initialBalance = rs.getDouble(1);
                        Logger.info("Initial balance is: " + initialBalance);
                        return initialBalance;
                    }
                }
            } catch (Exception exc) {
                Logger.error("An error occurred fetching the Term Deposits from finacle: aborting..");
                exc.printStackTrace();
            }
            return initialBalance;
        }

        public static List<models.Transaction> getTransactions(String fromDate, String toDate, String acid, AccountStatementsPDFHelper.BalanceStatement balanceStatement) {
            List<models.Transaction> returned = new ArrayList<models.Transaction>();
            double initialBalance = balanceStatement.openingBalance.doubleValue();
            double totalCredit = 0.0;
            double totalDebit = 0.0;
            int debitCount = 0;
            int creditCount = 0;
            int size = 0;
            try (Connection connection = DB.getConnection("secondary")) {
                //use jodatime
                Calendar tranCalendar = Calendar.getInstance();
                Calendar endDate = Calendar.getInstance();
                endDate.setTime(dt.parse(toDate));


                //for each day
                for (tranCalendar.setTime(dt.parse(fromDate)); tranCalendar.compareTo(endDate) < 1; tranCalendar.add(Calendar.DATE, 1)) {
                    //select all transactions for that day
                    fromDate = dt.format(tranCalendar.getTime());
                    String sSql = String.format(singleDayTransactionSql, acid, getDateString(fromDate));

                    //play.Logger.info(sSql);

                    try (PreparedStatement stmt = connection.prepareStatement(sSql);
                         ResultSet rs = stmt.executeQuery();) {
                        while (rs.next()) {
                            Date postedDate = rs.getDate(1);
                            String tranParticulars = rs.getString(2);
                            String tranAmount = String.format("%,.2f", rs.getDouble(3));
                            String partTranType = rs.getString(4);
                            Date valueDate = rs.getDate(5);
                            String tranId = rs.getString(6);
                            Date tranDate = rs.getDate(7);
                            String tranRmks = rs.getString(8);
                            models.Transaction transaction = new models.Transaction(postedDate, tranParticulars, tranAmount,
                                    valueDate, partTranType, tranId, tranDate, tranRmks);

                            if (transaction.partTranType.equals("C")) {
                                initialBalance += rs.getDouble(3);
                                totalCredit += rs.getDouble(3);
                                creditCount++;
                                transaction.clrBal = initialBalance;
                            } else {
                                initialBalance -= rs.getDouble(3);
                                totalDebit += rs.getDouble(3);
                                debitCount++;
                                transaction.clrBal = initialBalance;
                            }
                            returned.add(transaction);
                            size++;
                        }
                    }
                }
            } catch (Exception exc) {
                Logger.error("An error occurred fetching the Transactions from finacle: aborting..");
                exc.printStackTrace();
                return null;
            }
            try {
                balanceStatement.closingBalance = BigDecimal.valueOf(returned.get(returned.size()-1).clrBal);
                Logger.info("Closing balance = {}", balanceStatement.closingBalance);
            } catch (Exception exc) {
                balanceStatement.closingBalance = balanceStatement.openingBalance;
                Logger.error("An error occurred fetching closing balance..");
                Logger.error("The closing balance returning to opening balance: {}", balanceStatement.openingBalance);
            }
            balanceStatement.creditCount = creditCount;
            balanceStatement.debitCount = debitCount;
            balanceStatement.totalCredit = BigDecimal.valueOf(totalCredit).setScale(2, RoundingMode.DOWN);
            balanceStatement.totalDebit = BigDecimal.valueOf(totalDebit).setScale(2, RoundingMode.DOWN);
            return returned;
        }


        public static List<Transaction> getRecentTransactionsCustomer(int number, String customerId){
            return null;
        }

        /** This returns all the accounts of the customer specified by the inputted customer id
         * @param customerId
         * @return
         */
        public static List<Account> getCustomerAccounts(String customerId){
            return Account.find.where()
                    .eq("customerId", customerId)
                    .eq("ACCT_CLS_FLG", "N")
                    .in("schemeType", new String[]{"SBA", "ODA", "CAA"})
                    .ne("schemeCode", "112")
                    .findList();
        }

        /** this returns all the accounts of the customer specified by the inputted customer id
         * that are of the currency : currencyCode}
         * @param customerId
         * @param currencyCode
         * @return
         */
        public static List<Account> getCustomerAccounts(String customerId, String currencyCode){
            return Account.find.where()
                    .eq("customerId", customerId)
                    .eq("accountCurrencyCode", currencyCode)
                    .in("schemeType", new String[]{"SBA", "ODA", "CAA"})
                    .ne("schemeCode", "112")
                    .findList();
        }

        public static List<Transaction> getRecentTransactions(int number, String accountId){
return null;
        }
        public static List<models.Transaction> getTransactionsToday(String acid, AccountStatementsPDFHelper.BalanceStatement balanceStatement) {
            List<models.Transaction> returned = new ArrayList<models.Transaction>();
            double initialBalance = balanceStatement.openingBalance.doubleValue();
            double totalCredit = balanceStatement.totalCredit.doubleValue();
            double totalDebit = balanceStatement.totalDebit.doubleValue();
            int debitCount = balanceStatement.debitCount;
            int creditCount = balanceStatement.creditCount;
            try (Connection connection = DB.getConnection("secondary")) {
                //for each day
                String sSql = String.format(singleDayTransactionSqlToday, acid);
                //play.Logger.info(sSql);
                try (PreparedStatement stmt = connection.prepareStatement(sSql);
                     ResultSet rs = stmt.executeQuery();) {
                    while (rs.next()) {
                        Date postedDate = rs.getDate(1);
                        String tranParticulars = rs.getString(2);
                        String tranAmount = String.format("%,.2f", rs.getDouble(3));
                        String partTranType = rs.getString(4);
                        Date valueDate = rs.getDate(5);
                        String tranId = rs.getString(6);
                        Date tranDate = rs.getDate(7);
                        String tranRmks = rs.getString(8);
                        models.Transaction transaction = new models.Transaction(postedDate, tranParticulars,
                                tranAmount, valueDate, partTranType, tranId, tranDate, tranRmks);

                        if (transaction.partTranType.equals("C")) {
                            initialBalance += rs.getDouble(3);
                            totalCredit += rs.getDouble(3);
                            creditCount++;
                            transaction.clrBal = initialBalance;
                        } else {
                            initialBalance -= rs.getDouble(3);
                            totalDebit += rs.getDouble(3);
                            debitCount++;
                            transaction.clrBal = initialBalance;
                        }
                        returned.add(transaction);
                    }
                }

            } catch (Exception exc) {
                Logger.error("An error occurred fetching the Transactions from finacle: aborting..");
                exc.printStackTrace();
                return null;
            }
            try {
                balanceStatement.closingBalance = BigDecimal.valueOf(returned.get(returned.size() - 1).clrBal);
            } catch (Exception exc) {
                balanceStatement.closingBalance = balanceStatement.openingBalance;
            }
            //        balanceStatement.closingBalance = BigDecimal.valueOf(initialBalance).setScale(2, RoundingMode.DOWN);
            balanceStatement.creditCount = creditCount;
            balanceStatement.debitCount = debitCount;
            balanceStatement.totalCredit = BigDecimal.valueOf(totalCredit).setScale(2, RoundingMode.DOWN);
            balanceStatement.totalDebit = BigDecimal.valueOf(totalDebit).setScale(2, RoundingMode.DOWN);
            return returned;
        }

        public static List<String> getAccountIds(String cifId) {
            cifId = cifId.toUpperCase();
            List<String> returned = new ArrayList<String>();
            try (Connection connection = DB.getConnection("secondary");
                 PreparedStatement stmt = connection.prepareStatement(fetchAccountsSql)) {
                stmt.setString(1, cifId);
                try (ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        returned.add(rs.getString(3));
                    }
                }
            } catch (Exception exc) {
                Logger.error("An error occurred while fetching account balances: aborting");
                exc.printStackTrace();
                return null;
            }
            return returned;
        }

        public static AccountStatementsPDFHelper.AccountBalance getAccountBalance(String accountId, String toDate) {
            AccountStatementsPDFHelper.AccountBalance accountBalance = null;
            String sql = String.format(accountBalanceSql, accountId, getDateString(toDate));
            Logger.info(sql);
            try (Connection connection = DB.getConnection("secondary");
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        accountBalance = new AccountStatementsPDFHelper.AccountBalance(resultSet.getString(1),
                                resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
                    }
                }
            } catch (Exception ex) {
                Logger.error("An error occurred while fetching the account balance: aborting");
                ex.printStackTrace();
                return null;
            }
            return accountBalance;
        }

        public static List<String> getTDAccountIds(String cifId, String fromDate, String toDate) {
            List<String> returned = new ArrayList<String>();
            String sql = String.format(fetchTDAccountsSql, cifId, getDateString(toDate), getDateString(toDate));
            Logger.info(sql);
            try (Connection connection = DB.getConnection("secondary");
                 PreparedStatement stmt = connection.prepareStatement(sql)) {

                try (ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        returned.add(rs.getString(3));
                    }
                }
            } catch (Exception exc) {
                Logger.error("An error occurred while fetching account balances: aborting");
                exc.printStackTrace();
                return null;
            }
            return returned;
        }

        public static AccountStatementsPDFHelper.TDAccountBalance getTDAccountBalance(String accountId, String toDate) {
            AccountStatementsPDFHelper.TDAccountBalance tdAccountBalance = null;
            String sql = String.format(tdAccountBalanceSQL, accountId, getDateString(toDate));
            Logger.info(sql);
            try (Connection connection = DB.getConnection("secondary");
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        String schemeCode = resultSet.getString(1);
                        String accountBalance = resultSet.getString(2);
                        String tenor = resultSet.getString(3);
                        Date maturityDateObj = resultSet.getDate(4);
                        Logger.info(maturityDateObj.toString());
                        String interestRate = resultSet.getString(5);
                        String accountNumber = resultSet.getString(6);
                        String accountName = resultSet.getString(7);
                        String interest = resultSet.getString(8);
                        String maturityDate = new SimpleDateFormat("dd-MMM-yyyy").format(maturityDateObj);

                        tdAccountBalance = new AccountStatementsPDFHelper.TDAccountBalance(
                                accountBalance, accountName, accountNumber,
                                schemeCode, tenor, maturityDate
                                , interest);
                        return tdAccountBalance;
                    }
                }
            } catch (Exception ex) {
                Logger.error("An error occurred while fetching the account balance: aborting");
                ex.printStackTrace();
                return null;
            }
            return tdAccountBalance;
        }
    }

}
