package utils.notification;

import com.google.inject.Inject;
import models.notification.Alert;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import play.Logger;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;

import java.text.SimpleDateFormat;

/**
 * Created by DELL on 8/8/2016.
 */
public class Alerter extends Thread {
    public static final String HOST_NAME = play.Play.application().configuration().getString("smtp.host");
    public static final int SMTP_PORT = play.Play.application().configuration().getInt("smtp.port");
    public static final String FROM_ADDRESS = play.Play.application().configuration().getString("smtp.from");
    public static final String FROM_NAME = play.Play.application().configuration().getString("smtp.name");
    public static final String PASSWORD = play.Play.application().configuration().getString("smtp.password");
    public static final boolean SSL = play.Play.application().configuration().getBoolean("smtp.ssl");
    public static final String USER = play.Play.application().configuration().getString("smtp.user");


    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
    public static boolean seriousError = false;
    //plain text version of the alert
    private String plainTextMessage;

    private String errorMessage;
    private Alert alert;

    public Alerter(Alert alert) {
        this.alert = alert;
        start();
    }

    public void sendAlert() {
        try {
            Logger.info(alert.toString());

            //set failed to true
            Alert.makeFailed(alert.id, "Trying to send....");
            // emailAlert.isFailed = true;
            // emailAlert.save();
            if (alert.alertType == Alert.AlertType.EMAIL) {
                String message = alert.message;
                HtmlEmail email = new HtmlEmail();
                email.setSmtpPort(SMTP_PORT);
                email.setAuthenticator(new DefaultAuthenticator(USER, PASSWORD));
                email.setSSLOnConnect(SSL);
                email.setHostName(HOST_NAME);

                play.Logger.info("Email: {}", alert.receiver);
                email.addTo(alert.receiver);
                email.setFrom(FROM_ADDRESS, FROM_NAME);
                email.setSubject(alert.subject);
                email.setHtmlMsg(message + "<br/><br/>");
                Logger.info(message);

                // send the email
                Logger.info(email.send());
                Logger.info("id is {} ", alert.id);
                Alert.makeSent(alert.id);

                if (seriousError) {
                    Logger.info("Email not sent");
                } else {
                    Logger.info("Email sent successfully");
                }
            } else {
                //implement sms alert
            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
            errorMessage = "There was an error sending the email: " + ioe.getMessage();
            Alert.makeFailed(alert.id, errorMessage);
            play.Logger.error(errorMessage, ioe);
        }
    }

    public void setPlainTextMessage(String message) {
        plainTextMessage = message;
    }


    @Override
    public void run() {
        //try and send the email alert
        try {
           sendAlert();
        } catch (Exception exc) {
            exc.printStackTrace();
            play.Logger.info(String.format("Error sending alert: %s",
                    exc.getMessage()));
        }
    }
}

