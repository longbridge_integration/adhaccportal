package  utils.notification;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.User;
import models.notification.Alert;
import models.notification.MessageTemplate;
import play.Logger;
import play.libs.Json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Chigozirim on 9/22/2016.
 */
public class AlertService {
    private static AlertService instance = new AlertService();
    private static Map<String, Object> smsConstraints = new HashMap<String, Object>();

    //this stores the id's of all pending alerts to be sent
    private LinkedBlockingQueue<Alert> blockingQueue;


    public int sizeOfQueue() {
        return blockingQueue.size();
    }


    static {
        smsConstraints.put("isSent", false);
        smsConstraints.put("failed", false);
    }

    private AlertService() {
        blockingQueue = new LinkedBlockingQueue<Alert>();
    }


    public static AlertService getInstance() {
        if (instance == null) {
            instance = new AlertService();
        }
        return instance;
    }

    //create and send alert
    public void createAlert(MessageTemplate messageTemplate, User user) {
        try{
            new Alert.AlertBuilder(messageTemplate, user).build().save();
        }catch (Exception exc){
            exc.printStackTrace();
            Logger.info("Error: {}", exc.getMessage());
        }
    }

    public void sendAlert(Alert alert) {
        alert.save();
    }

    /**
     * Checks if the alert queue is empty
     */
    public boolean isQueueEmpty() {
        return blockingQueue.isEmpty();
    }

    public void queueAlert(Alert alert) {
        try {
            blockingQueue.put(alert);
        } catch (InterruptedException e) {
            Logger.error("Error:", e);
        }
    }

    public int queueSize() {
        return blockingQueue.size();
    }

    public LinkedBlockingQueue<Alert> getBlockingQueue() {
        return blockingQueue;
    }

    public Alert popAlert() throws InterruptedException {
        return blockingQueue.take();
    }

}
