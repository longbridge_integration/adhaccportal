package utils.notification;

import com.avaje.ebean.Expr;
import models.notification.Alert;
import play.Logger;
import sharedfunctions.TimeUtil;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is a <code>Runnable</code> which fetches unsent alerts and dispatches them
 * with each sms having it's own thread.
 * Created by chigozirimt on 10/12/15.
 */
public class AlertJob implements ActionListener {
    public static javax.swing.Timer alertTimer;
    public static AlertJob alertJob;
    //fetch the latest alerts that haven't been sent.
    private List<Alert> pendingAlerts;
    //Worker threads
    Thread thread;
    private boolean running;
    private static final int MAX_NUMBER_OF_ALERTS = play.Play.application().configuration().getInt("cmb.alerts.maxNumberOfAlerts");
    private static final int ALERT_WAIT_TIME = play.Play.application().configuration().getInt("cmb.alerts.alertWaitTime");
    private static final boolean ALERT_ENABLED = play.Play.application().configuration().getBoolean("cmb.alerts.alertEnabled");
    public static AlertService alertService = AlertService.getInstance();

    public AlertJob() {
        // if alertService is empty
        if (alertService.isQueueEmpty()) {
            //Add pending email alerts
            List<Alert> eAlerts = Alert.find.where().or(Expr.eq("status", Alert.AlertStatus.FAILED),
                    Expr.eq("status", Alert.AlertStatus.PENDING)).findList();
            play.Logger.info(String.format("There are %s pending alerts", eAlerts.size()));

            for (Alert alert : eAlerts) {
                if (TimeUtil.isToday(alert.createdOn)) {
                    alertService.queueAlert(alert);
                } else {
                    //do not send if not today
                    alert.statusMessage = "Date has passed";
                    alert.status = Alert.AlertStatus.FAILED;
                    alert.save();
                }
            }
        }
    }


    @Override
    public void actionPerformed(ActionEvent ev) {
        if (thread == null) {
            thread = new Thread(new AlertRunnable());
            thread.start();
        } else {
            if (thread.isAlive()) {
                //do nothing
            } else {
                this.thread = new Thread(new AlertRunnable());
                thread.start();
            }
        }
    }

    static class AlertRunnable extends Thread {

        public AlertRunnable() {
//            this.blockingQueue = blockingQueue;
        }

        @Override
        public void run() {
            if (!ALERT_ENABLED) {
                play.Logger.info("Alerts are disabled from the configuration");
                return;
            }

            while (true) {
                try {
                    try {
                        Logger.info("About to sleep for  " + ALERT_WAIT_TIME + " seconds");
                        Thread.sleep(ALERT_WAIT_TIME * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //make sure no alert is currently running
                    play.Logger.debug("Size of alert queue {} ", alertService.queueSize());

                    if (alertService.isQueueEmpty()) {
                        play.Logger.info("No alert is running: check for pending alerts in the db");

                        //fetch the alerts that haven't been sent
                        List<Alert> eAlerts = Alert.find.where().or(Expr.eq("status", "FAILED"), Expr.eq("status", "PENDING")).findList();
                        play.Logger.info("There are {} alerts to be sent", eAlerts.size());

                        if (eAlerts.isEmpty()) {
                            play.Logger.info("No New Alerts to send");
                            return;
                        }

                        ArrayList<Alerter> emailAlertWorkers = new ArrayList();

                        //add all alerts to blocking queue
                        for (Alert alert : eAlerts) {
                            alertService.queueAlert(alert);
                        }

                        //send all alerts in the blocking queue
                        while(!alertService.getBlockingQueue().isEmpty()){
                            new Alerter(alertService.popAlert());
                        }

                    } else {
                        try {
                            while (!alertService.isQueueEmpty()) {
                                Logger.info("Size of blocking queue: {} ", alertService.queueSize());
                                Alert task = alertService.popAlert();
                                // wait for task if necessary
                                play.Logger.info(String.format("Blocking queue size now: {}", alertService.queueSize()));

                                if (task.id == null) {
                                    play.Logger.info("Null alert");
                                    continue;
                                }
                                Logger.info("task {}", task.id);
                                new Alerter(task);
                            }

                        } catch (InterruptedException e) {
                            Logger.error("",e);
                        }
                    }
                } catch (Exception exc) {
                    Logger.error("", exc);
                }
            }
        }

    }
}

