package utils;

import external.Account;
import external.Customer;
import it.innove.play.pdf.PdfGenerator;
import models.Address;
import models.TermDepositScheme;
import org.joda.time.LocalDate;
import play.Logger;
import play.mvc.Result;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static play.mvc.Controller.flash;
import static play.mvc.Results.redirect;
import static utils.finacle.FinacleDataAccessObject.*;


/** The <code>AccountStatementsPDFHelper</code> class contains methods and classes for handling
 * all things about AccountStatements. This includes getting the PDF of the AccountStatement in bytes
 * as well as the <code>Result</code> for displaying within the browser.
 */
public class AccountStatementsPDFHelper {
     /** This returns the Account statement of the customer specified by {@code cifId} as a {@link Result} PDf to be viewed in the
      * browser or downloaded.
     * @param cifId The customer id of the required {@link Customer}
     * @param acid The account id of the {@link Account} to create statement on
     * @param fromDate The {@link java.util.Date} to start checking for transactions from
     * @param toDate The {@link java.util.Date} to stop checking for transactions
     * @return
     * @throws NullPointerException if any of the parameters is null and/or if the AccountStatement fails for any reason to generate
     */
    public static Result getAccountStatement(String cifId, String acid, String fromDate, String toDate) {
        cifId = cifId.toUpperCase();
        BalanceStatement balanceStatement = new BalanceStatement();
        double openingBalance = 0.0;
        double todayOpeningBalance=0.0;
        double totalDebit = 0.0;
        double totalCredit = 0.0;
        double closingBalance = 0.0;
        int debitCount = 0;
        int creditCount = 0;

        balanceStatement.openingBalance = new BigDecimal(openingBalance);
        balanceStatement.totalDebit = new BigDecimal(totalDebit);
        balanceStatement.totalCredit = new BigDecimal(totalCredit);
        balanceStatement.closingBalance = new BigDecimal(closingBalance);
        balanceStatement.creditCount = creditCount;
        balanceStatement.debitCount = debitCount;

        Account account = Account.findByAccountId(acid);
        String currency = account.currencyCode;
        String accountCurrency = account.accountCurrencyCode;
        try {
            //fetch the addresses
            Address customerAddress = CustomerDAO.getCustomerAddress(cifId);
            if (customerAddress == null) {
                throw new NullPointerException();
            }

            customerAddress.accountNumber = account.accountNumber;
            customerAddress.customerName = account.accountName;

            //get initial balance
            openingBalance = AccountStatementDAO.getInitialBalance(acid, fromDate);
            if (openingBalance == -0.1) {
                openingBalance = 0.0;
            }
            balanceStatement.openingBalance = BigDecimal.valueOf(openingBalance);
            List<models.Transaction> transactions = AccountStatementDAO.getTransactions(fromDate, toDate, acid, balanceStatement);

            if (transactions == null) {
                throw new NullPointerException();
            }

            Logger.info("todate: {}, now date: {}", toDate, new Date());
            Date todaysDate = dt.parse(toDate);

            Logger.info("todate: {}, now date: {}", todaysDate, new Date());
            LocalDate tod = LocalDate.fromDateFields(todaysDate);
            if(tod.equals(LocalDate.now())){
                Logger.info("Todate transactions");
                todayOpeningBalance = AccountStatementDAO.getInitialBalance(acid, toDate);
                balanceStatement.openingBalance = BigDecimal.valueOf(todayOpeningBalance);
                List<models.Transaction> todaysTransactions = AccountStatementDAO.getTransactionsToday(acid, balanceStatement);
                if(todaysTransactions!=null) {
                 transactions.addAll(todaysTransactions);
             }
            }

            closingBalance = -0.1;
            Logger.info("We are here");
            try {
                closingBalance = transactions.get(transactions.size() - 1).clrBal;
            } catch (Exception exc) {
                exc.printStackTrace();
                Logger.info("Closing Balance: " + openingBalance);
                closingBalance = openingBalance;
            }
            balanceStatement.openingBalance=new BigDecimal(openingBalance);
            balanceStatement.closingBalance=new BigDecimal(closingBalance);

            List<String> tdAccountIds = AccountStatementDAO.getTDAccountIds(cifId, fromDate, toDate);

            if (tdAccountIds == null) {
                throw new NullPointerException();
            }

            List<TDAccountBalance> tdAccountBalances = new ArrayList<TDAccountBalance>();

            for (String accountId : tdAccountIds) {
                TDAccountBalance tdAccountBalance = AccountStatementDAO.getTDAccountBalance(accountId, toDate);

                try {
                    tdAccountBalance.toString();
                    tdAccountBalances.add(tdAccountBalance);
                } catch (NullPointerException exc) {
                    Logger.error("TD balance is 0.0");
                }
            }

            PdfGenerator pdfGenerator = new PdfGenerator();
            //Load coronation font (Book Antiqua)
            pdfGenerator.loadLocalFonts(Arrays.asList(new String[]{"fonts/BKANT.TTF"}));
            return pdfGenerator.ok(views.html.accountstatements.template.statementtemplate.render(customerAddress, transactions,  tdAccountBalances, balanceStatement,accountCurrency), "http://localhost");

        } catch (Exception exc) {
            exc.printStackTrace();
            flash("danger", "Error generating statement: " + exc.toString() + exc.getMessage());
            return redirect(controllers.authorizer.routes.StatementController.view());
        }
    }

    /** This returns the Account statement of the customer specified by {@code cifId} in PDF format but in the form
     * of a byte array.
     * @param cifId The customer id of the required {@link Customer}
     * @param acid The account id of the {@link Account} to create statement on
     * @param fromDate The {@link java.util.Date} to start checking for transactions from
     * @param toDate The {@link java.util.Date} to stop checking for transactions
     * @return
     * @throws NullPointerException if any of the parameters is null and/or if the AccountStatement fails for any reason to generate
     */
    public static byte[] getAccountStatementBytes(String cifId, String acid, String fromDate, String toDate) throws NullPointerException {
        cifId = cifId.toUpperCase();
        BalanceStatement balanceStatement = new BalanceStatement();
        double openingBalance = 0.0;
        double todayOpeningBalance=0.0;
        double totalDebit = 0.0;
        double totalCredit = 0.0;
        double closingBalance = 0.0;
        int debitCount = 0;
        int creditCount = 0;

        balanceStatement.openingBalance = new BigDecimal(openingBalance);
        balanceStatement.totalDebit = new BigDecimal(totalDebit);
        balanceStatement.totalCredit = new BigDecimal(totalCredit);
        balanceStatement.closingBalance = new BigDecimal(closingBalance);
        balanceStatement.creditCount = creditCount;
        balanceStatement.debitCount = debitCount;

        Account account = Account.findByAccountId(acid);
        String currency = account.currencyCode;
        String accountCurrency = account.accountCurrencyCode;
        try {
            //fetch the addresses
            Address customerAddress = CustomerDAO.getCustomerAddress(cifId);
            if (customerAddress == null) {
                throw new NullPointerException();
            }

            customerAddress.accountNumber = account.accountNumber;
            customerAddress.customerName = account.accountName;

            //get initial balance
            openingBalance = AccountStatementDAO.getInitialBalance(acid, fromDate);
            if (openingBalance == -0.1) {
                openingBalance = 0.0;
//                throw new NullPointerException();
            }
            balanceStatement.openingBalance = BigDecimal.valueOf(openingBalance);
            List<models.Transaction> transactions = AccountStatementDAO.getTransactions(fromDate, toDate, acid, balanceStatement);

            if (transactions == null) {
                throw new NullPointerException();
            }
            LocalDate tod = LocalDate.fromDateFields( dt.parse(toDate));
            if(tod.equals(LocalDate.now())){
                todayOpeningBalance = AccountStatementDAO.getInitialBalance(acid, toDate);
                balanceStatement.openingBalance = BigDecimal.valueOf(todayOpeningBalance);

                List<models.Transaction> todaysTransactions = AccountStatementDAO.getTransactionsToday(acid, balanceStatement);
             Logger.info("Todays transactions: {}", todaysTransactions);
             if(todaysTransactions!=null) {
                 transactions.addAll(todaysTransactions);
            }
            }

            //closingBalance = -0.1;
            try {
                closingBalance = transactions.get(transactions.size()-1).clrBal;
                Logger.info("Closing balance: {}", closingBalance);
            } catch (Exception exc) {
                Logger.info("Exception fetching closing balance: {}",exc.fillInStackTrace());
                closingBalance = openingBalance;
            }

            List<String> tdAccountIds = AccountStatementDAO.getTDAccountIds(cifId, fromDate,toDate);

            if (tdAccountIds == null) {

                throw new NullPointerException();
            }

            List<TDAccountBalance> tdAccountBalances = new ArrayList<TDAccountBalance>();

            for (String accountId : tdAccountIds) {
                TDAccountBalance tdAccountBalance = AccountStatementDAO.getTDAccountBalance(accountId, toDate);
                try {
                    tdAccountBalance.toString();
                    tdAccountBalances.add(tdAccountBalance);
                } catch (NullPointerException exc) {
                    Logger.error("TD balance is 0.0");
                }
            }

            balanceStatement.openingBalance = new BigDecimal(openingBalance);
            balanceStatement.totalDebit = new BigDecimal(totalDebit);
            balanceStatement.totalCredit = new BigDecimal(totalCredit);
            balanceStatement.closingBalance = new BigDecimal(closingBalance);
            balanceStatement.creditCount = creditCount;
            balanceStatement.debitCount = debitCount;

            PdfGenerator pdfGenerator = new PdfGenerator();
            //Load coronation font (Book Antiqua)
            pdfGenerator.loadLocalFonts(Arrays.asList(new String[]{"fonts/BKANT.TTF"}));
            return pdfGenerator.toBytes(views.html.accountstatements.template.statementtemplate.render(customerAddress, transactions, tdAccountBalances, balanceStatement, accountCurrency), "http://localhost");

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /** Data structure for representing an account balance.
     *
     */
    public static class AccountBalance {
        public BigDecimal accountBalance;
        public String accountName;
        public String accountNumber;
        public String schemeType;

        public AccountBalance(String accountBalance, String accountName, String accountNumber, String schemeType) {
            this.accountBalance = new BigDecimal(accountBalance);
            this.accountName = accountName;
            this.accountNumber = accountNumber;
            this.schemeType = schemeType;
        }

        @Override
        public String toString() {
            return "AccountBalance{" +
                    "accountBalance=" + accountBalance +
                    ", accountName='" + accountName + '\'' +
                    ", accountNumber='" + accountNumber + '\'' +
                    '}';
        }
    }

    /** Data structure for storing information about a Term Deposit account.
     *
     */
    public static class TDAccountBalance {
        public BigDecimal accountBalance;
        public String accountName;
        public String accountNumber;
        public String schemeCode;
        public String tenor;
        public String maturityDate;
        public String interestRate;


        public TDAccountBalance(String accountBalance, String accountName,
                                String accountNumber,
                                String schemeCode, String tenor, String maturityDate, String interestRate) {
            this.accountBalance = new BigDecimal(accountBalance);
            this.accountName = accountName;
            this.accountNumber = accountNumber;
            this.schemeCode = schemeCode;
            this.tenor = tenor;
            this.maturityDate = maturityDate;
            this.interestRate = interestRate;
        }

        public String getDescription() {
            TermDepositScheme termDepositScheme = TermDepositScheme.findBySchemeCode(schemeCode);
            if (termDepositScheme.templateName == TermDepositScheme.TDTemplate.CALL_DEPOSIT){
                return String.format("%s %s", termDepositScheme.name, accountNumber);
            } else {

                return String.format("%s %s FOR %s DAYS MATURING ON %s",
                        TermDepositScheme.findBySchemeCode(schemeCode).name, accountNumber, tenor, maturityDate);
            }
        }

        @Override
        public String toString() {
            return "TDAccountBalance{" +
                    "accountBalance=" + accountBalance +
                    ", accountName='" + accountName + '\'' +
                    ", accountNumber='" + accountNumber + '\'' +
                    ", schemeCode='" + schemeCode + '\'' +
                    ", tenor='" + tenor + '\'' +
                    ", maturityDate='" + maturityDate + '\'' +
                    ", interestRate='" + interestRate + '\'' +
                    '}';
        }
    }

    public static class BalanceStatement {
        public BigDecimal openingBalance;
        public BigDecimal totalDebit;
        public BigDecimal totalCredit;
        public BigDecimal closingBalance;
        public int debitCount;
        public int creditCount;
    }
}
