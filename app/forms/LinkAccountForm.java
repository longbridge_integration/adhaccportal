package forms;

import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by Longbridge PC on 8/3/2017.
 */
public class LinkAccountForm {

    @Formats.NonEmpty
    @Constraints.Required(message = "Merchant ID cannot be empty")
    public String merchantid;

    @Formats.NonEmpty
    @Constraints.Required(message = "You must select a bank")
    public String merchantbank;
}
