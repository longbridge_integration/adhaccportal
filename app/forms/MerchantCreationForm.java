package forms;

import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by Longbridge PC on 8/4/2017.
 */
public class MerchantCreationForm {

    @Formats.NonEmpty
    @Constraints.Required(message = "merchant id cannot be empty")
    public String merchantid;

    @Formats.NonEmpty
    @Constraints.Required(message = "CIF ID cannot be empty")
    public String cifid;
}
