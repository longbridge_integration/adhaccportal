import controllers.Application;
import play.GlobalSettings;
import play.Logger;
import play.api.mvc.Handler;
import play.libs.F;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import utils.notification.AlertJob;

import javax.swing.*;
import java.lang.reflect.Method;

import static play.mvc.Results.redirect;

/**
 */
public class Global extends GlobalSettings {
    private AlertJob alertJob;

    @Override
    public void onStart(play.Application application) {
        final boolean alertEnabled = play.Play.application().configuration().getBoolean("cmb.alerts.alertEnabled");
        final int alertWaitTime = play.Play.application().configuration().getInt("cmb.alerts.alertWaitTime");
        super.onStart(application);
        Logger.info("Application has been shutdown but is now started");

        if (alertEnabled) {
            if (AlertJob.alertTimer == null) {
                play.Logger.info("Alert sending is enabled");
                AlertJob.alertTimer = new Timer(alertWaitTime * 1000, new AlertJob());
                AlertJob.alertTimer.start();
            }
        }
    }


    public void onStop(Application app) {
        Logger.info("Application shutdown...");
    }

    @Override
    public F.Promise<Result> onHandlerNotFound(Http.RequestHeader requestHeader) {

        String requestUrl = requestHeader.path();

        play.Logger.debug("in handler , url {}", requestUrl);


        if (requestUrl.endsWith("/")) {
            play.Logger.debug("requrl endswith {}", requestUrl.endsWith("/"));
            play.Logger.debug("requrl new url {}", (requestUrl.substring(0, requestUrl.length() - 1)));
            return F.Promise.<Result>pure(redirect(requestUrl.substring(0, requestUrl.length() - 1)));
        }
        return F.Promise.<Result>pure(Results.notFound(views.html.exceptions.notFound.render(requestHeader.uri())));
    }



    @Override
    public F.Promise<Result> onBadRequest(Http.RequestHeader requestHeader, String s) {
        play.Logger.debug("unknown {}", s);
        play.Logger.debug("{}", requestHeader.toString());
        play.Logger.debug("{}", requestHeader.headers().toString());

        super.onBadRequest(requestHeader, s);

        return F.Promise.<Result>pure(Results.notFound(views.html.exceptions.notFound.render(requestHeader.uri())));
    }


    @Override
    public F.Promise<Result> onError(Http.RequestHeader request, Throwable t) {
        super.onError(request, t);
        Logger.error("Error", t);
        return F.Promise.<Result>pure(Results.badRequest(views.html.exceptions.errorPage.render(t)));
    }

}