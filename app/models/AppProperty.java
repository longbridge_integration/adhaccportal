package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.Transaction;
import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import java.io.IOException;
import java.sql.Timestamp;


/** AppProperty represents a single configuration of the application
 */
@Entity
@EntityConcurrencyMode(ConcurrencyMode.NONE)
public class AppProperty extends Model {
    @Id
    public Long id;

    public String propKey;

    public String propValue;
    public static Finder<Long, AppProperty> find = new Finder(AppProperty.class);

    @Version
    public Timestamp lastUpdate;

    /** Get the app property with they specified key
     *
     * @param key The key of the AppProperty in the database
     * @return  AppProperty with the provided key
     */
    public static String getValue(String key){
        AppProperty property = find.where().eq("propKey",key).findUnique();
        try{return property.propValue;}catch (NullPointerException exc){return "";}
    }

    /** Update the value of the AppProperty with key
     *
     * @param key The key of the AppProperty to be updated
     * @param value The new value you want to update
     */
    public static void updateValue(String key, String value){
        Transaction transaction = Ebean.beginTransaction();
        AppProperty property = find.where().eq("propKey", key).findUnique();
        property.propValue = value;
        property.save();
        transaction.commit();
    }
    @Override
    public String toString() {
        return String.format("AppProperty{%s=%s}%n", propKey, propValue);
    }

    /** Increments AppProperty with key = countOfTransactions by 1
     *
     */
    public void incrementTransactionCount(){
        Transaction transaction1 = Ebean.beginTransaction();
        int lastCountOfTransactions = Integer.parseInt(propValue) + 1;
        propValue = ""+lastCountOfTransactions;
        Ebean.update(this);
        transaction1.commit();
        try {
            transaction1.close();
        } catch (IOException e) {
            play.Logger.error("Unable to close connection");

        }
    }

    /** Increments AppProperty with key = countOfTDs by 1
     *
     */
    private void incrementTDCount(){
        Transaction transaction1 = Ebean.beginTransaction();
        int lastCountOfTDs = Integer.parseInt(propValue) + 1;
        propValue = ""+lastCountOfTDs;
        Ebean.update(this);
        transaction1.commit();
        try {
            transaction1.close();
        } catch (IOException e) {
            play.Logger.error("Unable to close connection");

        }
    }

    /** Gets the time of last update of the AppProperty with propKey = countOfTDs
     *
     * @return
     */
    public static Timestamp getTimeOfLastTDCheck(){
        AppProperty property = find.where().eq("propKey", "countOfTDs").findUnique();
        return property.lastUpdate;
    }

    /** Gets the time of last update of the AppProperty with propKey = countOfTDs
     *
     * @return
     */
    public static void incrementTDCountStatic(){
        AppProperty property = find.where().eq("propKey", "countOfTDs").findUnique();
        property.incrementTDCount();
    }

    public static void incrementTransactionCountStatic(){
        AppProperty property = find.where().eq("propKey", "countOfTransactions").findUnique();
        property.incrementTransactionCount();
    }
}
