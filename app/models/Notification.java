package models;

import javax.persistence.Id;

/**
 * Created by DELL on 9/8/2016.
 */
public class Notification {
    @Id
    public Long id;

    public String message;
    public boolean read;
}
