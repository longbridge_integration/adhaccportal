package  models;


import java.text.SimpleDateFormat;
import java.util.Date;

/** Class data structure for representing a finacle transaction.
 *
 */
public class Transaction {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
    public Date postedDate;
    public Date changeTime;
    public String tranParticular;
    public String partTranType;
    public Date valueDate;
    public String tranAmount;
    public String tranId;
    public Date tranDate;
    public double clrBal;
    public String tranRmks;

    public Transaction(Date postedDate, String tranParticular, String tranAmount, Date valueDate, String partTranType, String tranId, Date tranDate, String tranRmks) {
        this.partTranType = partTranType;
        this.postedDate = postedDate;
        this.tranAmount = tranAmount;
        this.tranParticular = tranParticular;
        this.valueDate = valueDate;
        this.tranId = tranId;
        this.tranDate = tranDate;
        this.tranRmks = tranRmks;
        if(tranParticular.trim().isEmpty()){
            tranParticular = tranParticular + ": " + tranRmks;
        }
    }

    public String getValueDate() {
        return dateFormat.format(valueDate);
    }

    public String getPostedDate() {
        return dateFormat.format(postedDate);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "postedDate=" + postedDate +
                ", tranParticular='" + tranParticular + '\'' +
                ", partTranType='" + partTranType + '\'' +
                ", valueDate=" + valueDate +
                ", tranAmounts='" + tranAmount + '\'' +
                ", tranId='" + tranId + '\'' +
                ", tranDate=" + tranDate +
                ", clrBal=" + clrBal +
                '}';
    }
}