package models;


import com.avaje.ebean.Model;
import external.*;
import models.audittrails.Event;
import models.audittrails.Login;
import models.audittrails.UserHistory;
import models.notification.Alert;
import models.notification.MessageTemplate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.Logger;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.i18n.Messages;
import play.libs.Akka;
import play.libs.F;
import sharedfunctions.TimeUtil;
import utils.AppException;
import utils.Hash;
import utils.finacle.FinacleDataAccessObject;
import utils.notification.AlertService;

import javax.persistence.*;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "user_table")
public class User extends Model {
    private static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    public static Finder<Long, User> find = new Finder<Long, User>(User.class);
    @Id
    public long id;

    @Column(unique = true)
    public String username;

    @Formats.NonEmpty
    public String customerId;
    public String fullname;

    @Constraints.Email
    public String email;


    @Formats.NonEmpty
    public String passwordHash;
    @Formats.NonEmpty
    public boolean isEnabled = false;

    public boolean isFirstLogin =true;


    @Enumerated(EnumType.STRING)
    public Role role;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "user")
    public List<Login> loginAuditTrails;

    @Formats.DateTime(pattern="yyyy-MM-dd HH:mm:ss")
    public LocalDateTime lastSuccessfulLogin;
    @Formats.DateTime(pattern="yyyy-MM-dd HH:mm:ss")
    public LocalDateTime lastUnsuccessfulAttempt;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "user")
    public List<UserHistory> userHistories;

    public String securityAnswerOne; //idofSecurityQuestion|answer
    public String securityAnswerTwo; //idofSecurityQuestion|answer
    public String securityAnswerThree; //idofSecurityQuestion|answer


    public String securityQuestionOne; //idofSecurityQuestion|answer
    public String securityQuestionTwo; //idofSecurityQuestion|answer
    public String securityQuestionThree; //idofSecurityQuestion|answer


    private LocalDateTime lastSuccessfulLoginget(){
        int size = loginAuditTrails.size();
        if(size < 1){
            return null;
        }else{
            return loginAuditTrails.get(size-1).eventDate;
       }
    }

    private LocalDateTime lastUnsuccessfulAttemptget(){
        int size = Login.find.where().eq("username", username).findRowCount();
        if(size < 1){
            return null;
        }else{
            return Login.find.where().eq("username", username).setMaxRows(1).findList().get(0).eventDate;
        }
    }

    /** Authenticates the supplied username and password
     *
     * @param username the username of the user record
     * @param clearPassword the password ot the user record in plain text
     * @return User
     * @throws AppException
     */
    public static User authenticate(String username, String clearPassword) throws AppException {
        User user = find.where().eq("username", username).findUnique();
        if(user == null){
            Logger.debug("user not found");
        }

        if (user != null) {
            Logger.debug("User found {}");
            if (Hash.checkPassword(clearPassword, user.passwordHash)) {
                user.lastSuccessfulLogin = user.lastSuccessfulLoginget();
                user.save();
                return user;
            }
            Logger.error("Login authentication failed for user " + username);
            user.lastUnsuccessfulAttempt = user.lastUnsuccessfulAttemptget();
            user.save();

        }

        return null;
    }


    /**
     * Finds a user by his username
     * @param username
     * @return User
     */
    public static User findByUserName(String username){
        return find.where().eq("username",username).findUnique();
    }

    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }

    public static F.Promise<List<User>> find(){
        return F.Promise.promise(new F.Function0<List<User>>() {
            @Override
            public List<User> apply() throws Throwable {

                return find.all();

            }
        }, getContext()).recover(new F.Function<Throwable, List<User>>() {
            @Override
            public List<User> apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch users. DB probably unreachable");

                return new ArrayList<User>();
            }
        }, getContext());
    }

    public static F.Promise<User> find(final long id){
        return F.Promise.promise(new F.Function0<User>() {
            @Override
            public User apply() throws Throwable {

                return find.byId(id);

            }
        }, getContext()).recover(new F.Function<Throwable, User>() {
            @Override
            public User apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch user. DB probably unreachable {}", throwable);

                return new User();
            }
        }, getContext());
    }

    public  static F.Promise<User> findUser(final long id) {

        return F.Promise.promise(new F.Function0<User>() {
            @Override
            public User apply() throws Throwable {
                return find.where().idEq(id).findUnique();
            }
        }, getContext()).recover(
                new F.Function<Throwable, User>() {

                    @Override
                    public User apply(Throwable t) throws Throwable {

                        Logger.error("Failed to fetch user information.", t);

                        User user = new User();

                        return user;
                    }
                }, getContext());

    }

    public static String getUserName(long id) {
        return find.byId(id).username;
    }

    public String generateRandomPassword(){
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }


    /** This resets the user's password with a randomly generated
     * string. The generated password is returned for use by a Mailer or any
     * other component requiring
     * @return The generated password in plain text
     */
    public String resetPassword(){
        //Generate password
        String newPassword = generateRandomPassword().substring(0, 10);

        try {
            passwordHash = Hash.createPassword(newPassword);
        }catch(Exception exc){
            Logger.error("Error updating password");
            return null;
        }

        isFirstLogin = true;
        UserHistory history = new UserHistory();
        history.event = Event.RESET_PASSWORD;
        history.eventDate = new Date();
        history.initiator = this;
        history.user = this;
        userHistories.add(history);
        update();

        MessageTemplate template = new MessageTemplate();
        template.alertType = Alert.AlertType.EMAIL;
        template.code = "PASS_RESET";
        template.hasAttachment = false;
        template.arguments = "CUSTOMER_NAME";
        template.messageText = "Dear _PARAM, Your internet banking account password has been reset. <br/>" +
                "Your new password is: " + newPassword + " <br/><br/>Note that you will be required to change your password and security questions on first login<br/>" +
                "If you didn't initiate this request please contact customer service";
        template.subject = "Internet Banking Password reset";
        template.title = "Internet Banking Password reset";

        if(MessageTemplate.find.where().eq("code", template.code).findRowCount() > 0){
            template.save();
        }

        //send a mail that the account has been activated
        Alert.AlertBuilder builder = new Alert.AlertBuilder(template, this);
        AlertService.getInstance().sendAlert( builder.build());
        return newPassword;
    }

    /**
     * Enables a user
     */
    public void enableUser(User initiator) {
        this.isEnabled = true;
        //Generate password
        String newPassword = generateRandomPassword().substring(0, 10);
        this.passwordHash = Hash.createPassword(newPassword);
        email = CustomerContact.fetchEmail(customerId);
        UserHistory userHistory = new UserHistory();
        userHistory.user= this;
        userHistory.initiator = initiator;
        userHistory.event = Event.ENABLED;
        userHistory.eventDate = new Date();
        userHistories.add(userHistory);
        this.save();

        MessageTemplate template = new MessageTemplate();
        template.alertType = Alert.AlertType.EMAIL;
        template.code = "NEW_ACC_ACT";
        template.hasAttachment = false;
        template.arguments = "CUSTOMER_NAME|APP_DOMAIN";
        template.messageText = "Dear _PARAM, Your internet banking account has been activated please log in with the following details<br/>" +
                "Website: _PARAM <br/>" +
                "username: "+username+"<br/>password: "+ newPassword+"<br/><br/>Note that you will be required to change your password and security questions on first login";
        template.subject = "Internet Banking Activated";
        template.title = "Internet Banking Activated";

        if(MessageTemplate.find.findRowCount() < 1){
            template.save();
        }

        //send a mail that the account has been activated
        Alert.AlertBuilder builder = new Alert.AlertBuilder(template, this);
        AlertService.getInstance().sendAlert( builder.build());
    }

    /**
     * Disables a user
     */
    public void disableUser() {
        this.isEnabled = false;
        this.save();
    }

    /**
     * Changes a user password
     *
     * @param password
     * @throws utils.AppException AppException
     */
    public void changePassword(String password) throws AppException {
        this.passwordHash = Hash.createPassword(password);
        this.isFirstLogin = false;
        this.save();
    }

    /** this resets the user's password
     *  and then makes isFirstLogin true forcing theuser to change the password before proceeding
     * @param password
     */
    public void resetPassword(String password) {
        this.passwordHash = Hash.createPassword(password);
        this.isFirstLogin = true;
        this.save();
    }

    public F.Promise<Void> asyncSave() {

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                save();
                return null;
            }
        }, getContext());

    }


    public String showLastSuccessfulLogin(){
        try {
            return TimeUtil.getInstance().getRelativeTimeString(lastSuccessfulLogin, TimeUtil.TimeDetail.DATE_AND_TIME_DETAILED);
        }catch (NullPointerException exc){
            return "First Login";
        }
    }

    public String showUnsuccessfulAttempt(){
        try {
            return TimeUtil.getInstance().getRelativeTimeString(lastUnsuccessfulAttempt, TimeUtil.TimeDetail.DATE_AND_TIME_DETAILED);
        }catch(NullPointerException exc){
            return "First Login";
        }
    }

    public static User findbyCustomerId(String customerId){
        return find.where().eq("customerId", customerId).findUnique();
    }
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", isEnabled=" + isEnabled +
                ", isFirstLogin=" + isFirstLogin +
                ", role=" + role +
                '}';
    }

    public List<Account> getUserAccounts(){
        return FinacleDataAccessObject.AccountStatementDAO.getCustomerAccounts(customerId);
    }


    /** numbers, email address. customer id
     */

    /** This gives a {@link List} of Strings of the account ids of the current user
     *
     * @return {@link List} containing all account ids of the {@link external.Customer}
     */
    public List<String> getUserAccountIds(){
        List<Account> accounts = getUserAccounts();
        List<String> accountIds = new ArrayList<>();
        for(Account account: accounts){
           accountIds.add(account.accountId);
        }
        return accountIds;
    }

    /** This gives the last {@code number} {@link external.FinacleTranGen}s that have been carried out
     * on the {@link User}s accounts.
     * @param number
     * @return
     */
    public List<FinacleTranGen> getLastTransactions(int number){
        List<String> accountIds= getUserAccountIds();
        List<FinacleTranToday> finacleTranTodays = FinacleTranToday.find.where().in("accountId", accountIds).eq("postedFlag", "Y")
                .orderBy("postedDate desc").setMaxRows(10).findList();
        int size = finacleTranTodays.size();
        Logger.info("size of trans {}: number: {}", size, number);
        List<FinacleTranGen> returned = FinacleTranGen.getTranGetList(finacleTranTodays);

        if(size >= number){
            return returned;
        }
        List<FinacleTran> finacleTren = FinacleTran.find.where()
                .in("accountId", accountIds)
                .eq("postedFlag", "Y")
                .orderBy("postedDate desc")
                .setMaxRows(number - size).findList()
                ;
        returned.addAll(FinacleTranGen.getTranGenList(finacleTren));

        Logger.info("returned size: {}", returned.size());
        return returned;
    }


    /** This checks the {@code questionId} and {@code questionAnswer} and validates if
     * the answer is correct
     */
    public boolean validateSecurityQuestion(String id, String answer){
        //check if the user has the security question
        int whichQuestion = -1;
        if(securityQuestionOne.equals(id)) {
            Logger.debug(Messages.get("app.password.forgotpassword.securityquestion.is", id));
            whichQuestion = 1;
//        }else if(securityQuestionTwo.equals(id)){
//            Logger.debug(Messages.get("app.password.forgotpassword.securityquestion.is", id));
//            whichQuestion = 2;
//        }else if(securityQuestionThree.equals(id)){
//            Logger.debug(Messages.get("app.password.forgotpassword.securityquestion.is", id));
//            whichQuestion = 3;
//        }
        } else{
            //invalid security question
            Logger.error(Messages.get("app.password.forgotpassword.securityquestion.invalid.question"));
            return false;
        }

        //change answer to lowercase
        answer = answer.toLowerCase().trim();
        String storedAnswer = "";

        //change stored answer to lowercase
//        switch (whichQuestion){
//            case 1:
//                return Hash.checkPassword(answer, securityAnswerOne);
//            case 2:
//                return Hash.checkPassword(answer, securityAnswerTwo);
//            case 3:
//                return Hash.checkPassword(answer, securityAnswerThree);
//            default:
//                Logger.error(Messages.get("app.password.forgotpassword.securityquestion.invalid.question"));
//                return false;
//        }
        switch (whichQuestion){
            case 1:
                return Hash.checkPassword(answer, securityAnswerOne);
            case 2:
                return Hash.checkPassword(answer, securityAnswerTwo);
            case 3:
                return Hash.checkPassword(answer, securityAnswerThree);
            default:
                Logger.error(Messages.get("app.password.forgotpassword.securityquestion.invalid.question"));
                return false;
        }
    }

    /** This takes a username and email address and checks if the user with the
     * username has the same email as the one provided.
     * @param username
     * @param email
     * @return
     */
    public static boolean validateUsernameAndEmail(String username, String email){
        User user = find.where().ieq("username", username).findUnique();

        if(user == null){
            Logger.info("Username not found");
            return false;
        }
        if(email == null){
            Logger.error("Email is null");
            return false;
        }
        if(user.email.equalsIgnoreCase(email)){
            Logger.info("Username and email match the same account");
            return true;
        }else{
            Logger.info("Username and email do not match");
            return false;
        }
    }
}
