package models;


import com.avaje.ebean.Model;
import org.joda.time.LocalDateTime;
import play.Logger;
import play.data.format.Formats;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/** Represents a security question.
 * Created by chigozirim on 11/11/16.
 */
@Entity
public class SecurityQuestion extends Model {
    public enum SecurityQuestionCategory {
        ONE, TWO, THREE
    }

    @Id
    public Long id;

    @Formats.NonEmpty
    @Column(unique = true)
    public String questionText;
    @Enumerated(value = EnumType.STRING)
    public SecurityQuestionCategory securityQuestionCategory;

    public SecurityQuestion(long id, String question) {
        this.id = id;
        this.questionText = question;
    }


    public static Finder<Long, SecurityQuestion> find = new Finder<Long, SecurityQuestion>(SecurityQuestion.class);

    /** This returns all the {@link SecurityQuestion}s stored in the database
     *
     * @return {@link List} of {@link SecurityQuestion}s stored in the database
     */
    public static List<SecurityQuestion> getSecurityQuestions() {
        return find.all();
    }
    public static List<SecurityQuestion> getSecurityQuestionsByCategory(SecurityQuestionCategory securityQuestionCategory) {

        List<SecurityQuestion> securityQuestions = new ArrayList<SecurityQuestion>();
        securityQuestions = find.where().eq("securityQuestionCategory", securityQuestionCategory).findList();
        return securityQuestions;
    }

    /**
     * Checks if security questions exist in the database
     */

    public static boolean isSecurityQuestionExist(){

        return (find.findRowCount()>1);

    }
    /**
     * Creates the security questions presented to the user
     */
    public static void createSecurityQuestions(){

        Logger.info("Creating security questions");
        //Category One Security Questions
        SecurityQuestion one = new SecurityQuestion(1l, "What is the name of your childhood hero?");
        one.securityQuestionCategory = SecurityQuestion.SecurityQuestionCategory.ONE;
        one.save();
        SecurityQuestion two = new SecurityQuestion(2l, "What was the name of your elementary school?");
        two.securityQuestionCategory= SecurityQuestion.SecurityQuestionCategory.ONE;
        two.save();
        //Category Two Security Questions
        SecurityQuestion three =  new SecurityQuestion(3l, "Where did you meet your spouse?");
        three.securityQuestionCategory = SecurityQuestion.SecurityQuestionCategory.TWO;
        three.save();

        SecurityQuestion four =  new SecurityQuestion(4l, "What year did you get your first internship?");
        four.securityQuestionCategory = SecurityQuestion.SecurityQuestionCategory.TWO;
        four.save();

        //Category Three Security Questions
        SecurityQuestion five =  new SecurityQuestion(5l, "What is the last name of your best sports person?");
        five.securityQuestionCategory = SecurityQuestion.SecurityQuestionCategory.THREE;
        five.save();

        SecurityQuestion six  = new SecurityQuestion(6l, "What is the brand of your first car?");
        six.securityQuestionCategory = SecurityQuestion.SecurityQuestionCategory.THREE;
        six.save();


    }




}

