package models.form;

import controllers.routes;
import models.SecurityQuestion;
import models.User;
import play.Logger;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.i18n.Messages;
import utils.AppException;

import static play.mvc.Controller.flash;

/** This form handles the acceptance of input from the reset password page where the user
 * selects one of their security questions and enters the answer.
 * Created by Fortune on 11/11/2016.
 */
public class SecurityQuestionForm {

    @Formats.NonEmpty
    public String username;
    @Formats.NonEmpty
    public String securityQuestion;
    @Formats.NonEmpty
    @Constraints.Required
    public String securityQuestionAnswer;

    public SecurityQuestionForm(){

    }
    public SecurityQuestionForm(String username){
        this.username = username;
    }

    public String validate() throws AppException {
        User user = User.findByUserName(username);
        if(user==null){
            Logger.error(Messages.get("app.user.error.notfound"));
            return Messages.get("app.user.error.notfound");
        }

        if(user.validateSecurityQuestion(securityQuestion, securityQuestionAnswer)){
            //the supplied answer was correct
            //log the user in and change the password
            Logger.info("Password successfully reset");
            user.resetPassword();
            return null;
        }else{
            Logger.info("Security question/answer is incorrect");
            return "Security question/answer is incorrect";
        }

    }

}

