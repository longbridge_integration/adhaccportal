package models.form;

import play.i18n.Messages;
import utils.HostMaster;
import controllers.routes;
import models.User;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;
import play.mvc.Http;
import utils.AppException;

/**
 *
 */
public class ChangePasswordForm {
    @Constraints.Required
    public String securityQuestion1;
    @Constraints.Required
    public String securityAnswer1;
//    @Constraints.Required
//    public String securityQuestion2;
//    @Constraints.Required
//    public String securityAnswer2;
//    @Constraints.Required
//    public String securityQuestion3;
//    @Constraints.Required
//    public String securityAnswer3;

    @Constraints.Required
    @Constraints.MinLength(8)
    public String oldPassword;
    @Constraints.Required
    @Constraints.MinLength(8)
    public String newPassword;
    @Constraints.Required
    @Constraints.MinLength(8)
    public String newPasswordConfirmation;

    /** Validates the new password entered. If the new password passes all validation
     *  it returns null else it returns the error message
     * @return a message specifying the new password isn't accepted or null if the new password is accepted
     */
    public String validate() {
        User user = new HostMaster().getCurrentUser();

        if(user == null){
            play.Logger.error("No user found");
            play.mvc.Results.redirect(routes.Application.logout());
        }

        String ipAddress = Http.Context.current().request().remoteAddress();
        try {
            user = User.authenticate(user.username, oldPassword);
            if(user == null){
                play.Logger.error("Old password supplied is invalid.");
                return "Invalid password supplied.";
            }

            String [] disallowedWords = StringUtils.split(StringUtils.substringBefore(user.username,"@"), ".@");
            boolean isDisallowedMatched = false;
            for(String word : disallowedWords){
                if(newPassword.startsWith(word) || newPassword.endsWith(word)){
                    isDisallowedMatched = true;
                    break;
                }
            }
            if(isDisallowedMatched){
                return "Password cannot start or end with your username";
            }

            if(StringUtils.isAlphanumeric(newPassword)){
                return "Password must contain at least 1 special character (Not an alphabet or number)";
            }

            if(newPassword.equals(newPasswordConfirmation)){
                return null;
            }else{
                return "New password must match with the confirmation entry. Kindly check and try again .";
            }
        } catch (AppException appEx) {
            play.Logger.error("Error occurred while changing user password {}", appEx);
            return "Technical problem occurred. please try again";
        }
    }

    @Override
    public String toString() {
        return "ChangePasswordForm{" +
                "securityQuestion1='" + securityQuestion1 + '\'' +
                ", securityAnswer1='" + securityAnswer1 + '\'' +
                ", oldPassword='" + oldPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                ", newPasswordConfirmation='" + newPasswordConfirmation + '\'' +
                '}';
    }
}
