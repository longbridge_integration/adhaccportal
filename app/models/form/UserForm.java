package models.form;

import external.Customer;
import models.Role;
import models.User;
import play.Logger;
import play.data.format.Formats;
import play.data.validation.Constraints;

/** Form for creating new <b>User</b>s
 */
public class UserForm {

    public long id;

    public String userName;

    @Formats.NonEmpty
    public String password="password123";

    @Constraints.Required
    public Role role;

    public String fullname;
    @Constraints.Required
    public String customerId;


    /** This makes sure the user with the username provided doesn't already exist
     *
     * @return null if the user does not exist and an error message otherwise
     */
    public String validate() {
        userName = customerId;
        User user = User.findByUserName(userName);

        if(user !=null && id != user.id) {
            return "The email provided is already registered.";
        }

        user = User.findbyCustomerId(customerId);

        if(user != null && id != user.id){
            return "An account is already attached to your customer id";
        }

        try {
            fullname = org.apache.commons.lang.StringUtils.capitalize(Customer.fetchFullName(customerId));

        }catch(Exception exc){
            Logger.error("could not get customer name from finacle", exc);
            return "Invalid customer id or could not reach finacle";
        }

        return null;
    }


}
