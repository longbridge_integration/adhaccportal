package models.form;

import models.audittrails.Login;
import models.audittrails.LoginResult;
import models.User;
import models.notification.Alert;
import models.notification.MessageTemplate;
import play.cache.Cache;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.mvc.Http;
import utils.AppException;
import utils.notification.AlertService;

import static play.mvc.Http.Context.Implicit.session;

/** Form capturing the Login of a user at the login page
 */
public class LoginForm {

    @Formats.NonEmpty
    @Constraints.Required
    public String username;

    @Constraints.Required
    public String password;

    /** This validates the username and his password against what is stored in the database.
     *
     * @return null if the validation is confirmed or an error message if not
     */
    public String validate() {
        User user = null;
        String ipAddress = Http.Context.current().request().remoteAddress();

        try {
            user = User.authenticate(username, password);
        } catch (AppException appEx) {
            play.Logger.error("Error occurred while doing user authentication {}", appEx);
            return "Technical problem occurred. please try again";
        }

        Login loginTrail = null;
        if (user == null) {
            try{
                loginTrail=  new Login(LoginResult.USER_NOT_FOUND,ipAddress,username);
                loginTrail.save();

                user = User.findByUserName(username);
                if(user!=null){
                    user.lastUnsuccessfulAttempt = loginTrail.eventDate;
                    user.save();
                }
            }catch(Exception e){
                play.Logger.error("failed to save audit trail {}",e);
                play.Logger.info("LoginTrail {}",loginTrail.toString());
            }
            play.Logger.info("LOGIN ATTEMPT FAILED WITH USERNAME {} ",username);
            return "Invalid username and password combination";
        }
        else if (user.isEnabled != true) {
            play.Logger.info("Disabled User with ID [{}] tried to login",user.id );
            try{
                loginTrail=  new Login(LoginResult.USER_NOT_ENABLED,ipAddress,user);
                loginTrail.save();

                user = User.findByUserName(username);
                if(user!=null){
                    user.lastUnsuccessfulAttempt = loginTrail.eventDate;
                    user.save();
                }
            }catch(Exception e){
                play.Logger.error("failed to save audit trail {}",e);
                play.Logger.info("LoginTrail {}",loginTrail.toString());
            }
            return "User account is not enabled";
        }
        session().clear();
        session().put("auth_user_name",username);
        session().put("auth_user_fullname",user.fullname);
        session().put("auth_user_role",user.role.name());
        session().put("auth_user_id", user.id + "");
        Cache.set(username + "_auth_user", user);
        try{
            loginTrail=  new Login(LoginResult.LOGIN_OK,ipAddress,user);
            loginTrail.save();
        }catch(Exception e){
            play.Logger.error("failed to save audit trail {}",e);
            play.Logger.info("LoginTrail {}",loginTrail.toString());
        }
        play.Logger.info("LOGIN_OK");
        MessageTemplate template = new MessageTemplate();
        template.alertType = Alert.AlertType.EMAIL;
        template.code = "LOGIN_OK";
        template.hasAttachment = false;
        template.arguments = "CUSTOMER_NAME|CURRENT_TIME";
        template.messageText = "Dear _PARAM, Your internet banking account was just logged in to at _PARAM .  <br/>" +
                "<br/>If you did not initiate or authorize the log in, please contact customer care with the details below<br/>" +
                "email: cs@coronationmb.com, customercomplaints@coronationmb.com;<br/>Phone: 08188314939, SMS: 08028893124<br/>";
        template.subject = "Internet Banking Login";
        template.title = "Internet Banking Login";

        if(MessageTemplate.find.where().eq("code", template.code).findRowCount() > 0){
            template.save();
        }

        //send a mail that the account has been activated
        Alert.AlertBuilder builder = new Alert.AlertBuilder(template, user);
        Alert alert = builder.build();
        AlertService.getInstance().sendAlert( alert );
        return null;
    }
}