package models.form;

import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by Fortune on 11/11/2016.
 */
public class ForgotPasswordForm {

    @Formats.NonEmpty
    @Constraints.Required
    public String username;

    public String emailAddress;

}
