package models;

/** Small class data structure for storing the address of a customer from finacle. It also
 * contains the customer's cif_id and the account number of the account
 */
public class Address {

        public String customerName;
        public String addressLine1;
        public String addressLine2;
        public String addressLine3;
        public String accountNumber;
        public String customerId;

    public Address(){}
        public Address(String addressLine1, String addressLine2, String addressLine3, String accountNumber, String customerId, String customerName) {
            this.customerName = customerName;
            this.addressLine1 = addressLine1;
            this.addressLine2 = addressLine2;
            this.addressLine3 = addressLine3;
            this.accountNumber = accountNumber;
            this.customerId = customerId;
        }

        @Override
        public String toString() {
            return "Address{" +
                    "addressLine1='" + addressLine1 + '\'' +
                    ", addressLine2='" + addressLine2 + '\'' +
                    '}';
        }
}
