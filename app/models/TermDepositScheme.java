package models;

import com.avaje.ebean.Model;
import play.Logger;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chigozirim on 16/02/2016.
 */
@Entity
public class TermDepositScheme extends Model{
    public enum TDTemplate{
        BOND_NOTES, TREASURY_NOTES, CALL_DEPOSIT, GOVERNMENT
    }
    public enum RateType{
        BACKEND_PRODUCT, UPFRONT_INTEREST_PRODUCT
    }
    @Id
    public Long id;

    public String name;

    public String title;
    @Column(columnDefinition = "TEXT")
    public String schemeDescription;

    @Column(unique = true)
    public String schemeCode;

    @Enumerated(value = EnumType.STRING)
    public TDTemplate templateName;

    @Enumerated(value = EnumType.STRING)
    public RateType rateType;

    @Column(columnDefinition = "TEXT")
    public String conclusionText;

    @Column(columnDefinition = "TEXT")
    public String introText;

    @Column(columnDefinition = "TEXT")
    public String purposeText;

    public String fileNamePrefix;

    @ManyToOne
    public User modifiedBy;

    @Version
    public Timestamp lastUpdate;

    public static Finder<Long, TermDepositScheme> find = new Finder<Long, TermDepositScheme>(TermDepositScheme.class);

    /** Finds the TermDepositScheme by the scheme code
     *
     * @param schemeCode - The scheme code of the term deposit scheme
     * @return Term Deposit scheme with the specified <code>Scheme Code</code>
     */
    public static TermDepositScheme findBySchemeCode(String schemeCode){
        return find.where().eq("schemeCode", schemeCode).findUnique();
    }


    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }


    public static F.Promise<List<TermDepositScheme>> find(){
        return F.Promise.promise(new F.Function0<List<TermDepositScheme>>() {
            @Override
            public List<TermDepositScheme> apply() throws Throwable {
                List<TermDepositScheme> lists = find.all();
                Logger.info(lists.size() + " sd");
                return lists;

            }
        }, getContext()).recover(new F.Function<Throwable, List<TermDepositScheme>>() {
            @Override
            public List<TermDepositScheme> apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch term deposit schemes. DB probably unreachable");

                return new ArrayList<TermDepositScheme>();
            }
        }, getContext());
    }

    public static F.Promise<TermDepositScheme> find(final long id){
        return F.Promise.promise(new F.Function0<TermDepositScheme>() {
            @Override
            public TermDepositScheme apply() throws Throwable {

                return find.byId(id);

            }
        }, getContext()).recover(new F.Function<Throwable, TermDepositScheme>() {
            @Override
            public TermDepositScheme apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch term deposit schemes. DB probably unreachable {}", throwable);

                return new TermDepositScheme();
            }
        }, getContext());
    }

    @Override
    public String toString() {
        return "TermDepositScheme{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", schemeDescription='" + schemeDescription + '\'' +
                ", schemeCode='" + schemeCode + '\'' +
                ", templateName=" + templateName +
                ", rateType=" + rateType +
                ", conclusionText='" + conclusionText + '\'' +
                ", introText='" + introText + '\'' +
                ", purposeText='" + purposeText + '\'' +
                ", fileNamePrefix='" + fileNamePrefix + '\'' +
                ", modifiedBy=" + modifiedBy +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
