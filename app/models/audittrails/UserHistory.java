package models.audittrails;

import com.avaje.ebean.Model;
import models.User;
import play.data.format.Formats;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

/** This stores a single user history in the database.
 */
@Entity
public class UserHistory extends Model {

    @Id
    public long id;

    @ManyToOne
    public User user;


    @ManyToOne
    public User initiator;

    @Formats.DateTime(pattern="yyyy-MM-dd HH:mm:ss")
    public Date eventDate;

    public Event event;


}
