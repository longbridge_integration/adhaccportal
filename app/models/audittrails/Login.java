package models.audittrails;

import com.avaje.ebean.Model;
import models.User;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.data.format.Formats;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 */
@Entity
@Table(name = "login_audit_trail")
public class Login  extends Model {
    private static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    @Id
    public long id;

    @Enumerated(EnumType.STRING)
    public LoginResult eventType;


    @Formats.DateTime(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(updatable=false)
    public LocalDateTime eventDate = LocalDateTime.now();

    public String ipAddress;

    public String username;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    public User user;

    public static Finder<Long, Login> find = new Finder<Long, Login>(Long.class, Login.class);

    public Login() {
    }

    public Login(LoginResult eventType, String ipAddress, String username) {
        this.eventType = eventType;
        this.ipAddress = ipAddress;
        this.username = username;
    }

    public Login(LoginResult eventType, String ipAddress, User user) {
        this.eventType = eventType;
        this.ipAddress = ipAddress;
        this.user = user;
    }

    public String loginEventDate(){
        return formatter.print(eventDate);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Login{");
        sb.append("id=").append(id);
        sb.append(", eventType=").append(eventType);
        sb.append(", eventDate=").append(eventDate);
        sb.append(", ipAddress='").append(ipAddress).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }




    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }


    public  static F.Promise<List<Login>> find(){


        return F.Promise.promise(new F.Function0<List<Login>>() {
            @Override
            public List<Login> apply() throws Throwable {
                return find.all();
            }
        },getContext()).recover(new F.Function<Throwable, List<Login>>() {
            @Override
            public List<Login> apply(Throwable throwable) throws Throwable {

                play.Logger.error("Failed to fetch faculties from db {}",throwable);

                return new ArrayList<Login>();
            }
        },getContext());
    }

    public  static F.Promise<Login> find(final long id){


        return F.Promise.promise(new F.Function0<Login>() {
            @Override
            public Login apply() throws Throwable {
                return find.byId(id);
            }
        },getContext()).recover(new F.Function<Throwable, Login>() {
            @Override
            public Login apply(Throwable throwable) throws Throwable {

                play.Logger.error("Failed to fetch faculty from db {}",throwable);

                return new Login();
            }
        },getContext());
    }
}
