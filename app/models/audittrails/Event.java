package models.audittrails;

/**This represents the list of Events that are possible for each user.
 */
public enum Event {

    ENABLED,
    DISABLED,
    CREATED,
    CREATED_SCHEME_TYPE,
    MODIFIED_SCHEME_TYPE,
    RESET_PASSWORD

}
