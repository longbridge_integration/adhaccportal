package models.audittrails;

/** This enum contains all the possible Login outcomes for login auditting
 */
public enum LoginResult {
    LOGIN_OK,
    USER_NOT_FOUND,
    USER_NOT_ENABLED,
    LOGOUT_OK
}
