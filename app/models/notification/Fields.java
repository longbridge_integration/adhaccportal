package models.notification;

import external.Account;
import external.Customer;
import models.User;
import play.Logger;
import utils.AppException;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/** <b>Fields</b> is a singleton class which allows retrieval of data from an account. With the class you can retrieve
 * some supported information from the account.
 * is useful in the
 * Created by Chigozirim on 10/7/2016.
 */
public class Fields {
    enum Field {
         ACCOUNT_NAME, CUSTOMER_NAME, ACCOUNT_NUMBER, APP_DOMAIN, CURRENT_TIME
    }

    public static String getFieldValue(User user, String key)  {
        Logger.info("Field key is: " + key);
        try {
            Field field = Field.valueOf(key);
            switch (field) {
                case CUSTOMER_NAME:
                    return customerName(user);
                case APP_DOMAIN:
                    return "ibank.coronationmb.com";
                case CURRENT_TIME:
                    DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss Z");
                    return ZonedDateTime.now().format(FORMATTER);
                default:
                    return null;
            }
        } catch (IllegalArgumentException exc) {
            throw new NullPointerException(String.format("Field %s is invalid",key));
        } catch (NullPointerException exc){
            Logger.info("This field has a null value");
            return null;
        }
    }

    public static String customerName(User user){
        if (user == null){
            Logger.error("Null user");
            return "";
        }
        if( user.fullname != null){
            return user.fullname;
        }else {
            user.fullname = Customer.fetchFullName(user.customerId);
        }
        return (user.fullname==null) ? "": user.fullname;
    }

    /**
     * This returns an array of all the fields from the argument string.
     *
     * @param user   The account to get these fields from
     * @param arguments This is the argument string
     * @return An array containing the Fields specified in the argument string
     */
    public static String[] argumentsToArray(User user, String arguments) {
        String[] returned = new String[]{};
        if (arguments == null) {
            return returned;
        }
        ArrayList<String> list = new ArrayList<>();
        Scanner scanner = new Scanner(arguments);
        scanner.useDelimiter("\\|");

        while (scanner.hasNext()) {
            String key = scanner.next();
            String value = null;
            try{
                value = Fields.getFieldValue(user, key);
                list.add(value);
            }catch (Exception bce){
                Logger.error("", bce);
            }
        }
        System.out.println(list.toString());
        return list.toArray(returned);
    }
}
