package models.notification;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

/**
 * Created by chigozirim on 11/14/16.
 */
@Entity
public class MessageTemplate extends Model{
    @Id
    public Long id;
    @Column(unique = true)
    public String title;
    @Column(unique = true)
    public String code; //module first 3 letters than 3 digit number
    @Column(columnDefinition = "TEXT")
    public String messageText;
    /**pipe separated list of {@link models.notification.Fields}*/
    @Column(columnDefinition = "TEXT")
    public String arguments;
    public boolean hasAttachment;
    public String subject;
    public Alert.AlertType alertType;

    @Transient
    public List<String> argumentValues;
    public static Model.Finder<Long, MessageTemplate> find = new Model.Finder<>(MessageTemplate.class);

    @Override
    public String toString() {
        return "MessageTemplate{" +
                "id=" + id +
                ", messageText='" + messageText + '\'' +
                ", arguments=" + arguments +
                ", hasAttachment=" + hasAttachment +
                '}';
    }

    public static MessageTemplate findMessageTemplateByCode(String code){
        return find.where().eq("code", code).findUnique();
    }

    public String message(String[] args){
        messageText = messageText.replace("_PARAM", "%s");
        return String.format(messageText, args);
    }

    public MessageTemplate(){}


    public static MessageTemplate findMessage(String message){
        MessageTemplate returned = findMessageTemplateByCode(message);
        if(returned==null){
            returned = new MessageTemplate();
            returned.messageText = message;
            returned.hasAttachment = false;
        }
        return returned;
    }


}
