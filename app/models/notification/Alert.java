package models.notification;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import models.User;
import org.joda.time.LocalDateTime;
import play.Logger;

import javax.persistence.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by DELL on 8/8/2016.
 */
@Entity
public class Alert extends Model {
    public enum AlertType {SMS, EMAIL}
    public enum AlertStatus{ PENDING, SENDING, SENT, FAILED}
    private static final java.text.SimpleDateFormat mySQLDateFormat =
            new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Id
    public Long id;
    @Enumerated(value = EnumType.STRING)
    public AlertType alertType;

    @Column(columnDefinition = "TEXT")
    public String message;

    @Enumerated(value = EnumType.STRING)
    public AlertStatus status;
    public String statusMessage;
    public LocalDateTime statusTime;

    @Column(name="created_on" , updatable = false)
    public LocalDateTime createdOn = LocalDateTime.now();
    public String receiver;
    public String receiverName;
    public String subject;

    @Version
    public Timestamp lastUpdate;

    public static Finder<Long, Alert> find = new Finder<Long, Alert>(Alert.class);

    public Alert(){

    }

    public Alert(AlertType alertType, String message, AlertStatus status,  String receiver, String receiverName, String subject) {
        this.alertType = alertType;
        this.message = message;
        this.status = status;
        this.receiver = receiver;
        this.receiverName = receiverName;
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "id=" + id +
                ", alertType=" + alertType +
                ", message='" + message + '\'' +
                ", status=" + status +
                ", statusTime=" + statusTime +
                ", modifiedOn=" + createdOn +
                ", receiver='" + receiver + '\'' +
                ", receiverName='" + receiverName + '\'' +
                ", subject='" + subject + '\'' +
                ", lastUpdate=" + lastUpdate +
                '}';
    }

    /** JDBC method for updating the isSent field to true
     *
     * @param id The id of the EmailAlert to be sent
     */
    public static void makeSent(Long id){
        try(Connection connection = play.db.DB.getConnection("default");
            PreparedStatement statement =
                    connection.prepareStatement("UPDATE `alert` SET `status_time` = ?," +
                            " `status` = 'SENT', `status_message` = 'Successfully sent message' WHERE ( `id` = ?  )")){
            String currentTime = mySQLDateFormat.format(new Date());
            statement.setString(1, currentTime);
            statement.setLong(2, id);
            statement.execute();
        }catch (Exception exc){
            Logger.error("", exc);
        }
    }

    /** JDBC method for updating the failed status of an email
     *
     * @param id The id of the EmailAlert to be sent
     */
    public static void makeFailed(Long id, String failMessage){
        try(Connection connection = play.db.DB.getConnection("default");
            PreparedStatement statement =
                    connection.prepareStatement("UPDATE `alert` SET `status` = 'SENDING'," +
                            " `status_time` = ? , `status_message` = ?  WHERE ( `id` = ?  )")){
            String currentTime = mySQLDateFormat.format(new Date());
            statement.setString(1, currentTime);
            statement.setString(2, failMessage);
            statement.setLong(3, id);
            statement.execute();
        }catch (Exception exc){
            Logger.error("", exc);
        }
    }



    @Override
    protected Object clone() throws CloneNotSupportedException {
        Alert alert = new Alert();
        alert.message = ""+message;
        alert.subject = ""+subject;
        alert.createdOn = createdOn;
        alert.status = status;
        alert.statusMessage = "" + statusMessage;
        alert.statusTime = statusTime;
        alert.alertType = alertType;

        return alert;
    }


    /** Creates a clone of the current <b>EmailAlert</b> and changes the email address it will be
     * sent to. This saves processing time by not having to instantiate all the data afresh.
     * Follows the Prototype creational design pattern.
     * @return
     */
    public Alert cloneAlert(){
        try {
            Alert alert = (Alert) clone();
            Logger.info("clone alert");
            return alert;
        }catch(Exception ec){
            Logger.error("Error:", ec);
            return null;
        }
    }


    public static class AlertBuilder{
        String message = "";
        AlertType alertType;
        User user;
        AlertStatus status = AlertStatus.PENDING;
        String statusMessage;
        String receiver;
        String receiverName;
        String subject;
        String title;



        public AlertBuilder (MessageTemplate messageTemplate, User user) throws NullPointerException{
            if(messageTemplate ==null){
                throw new NullPointerException("MessageTemplate was not found");
            }
            if(user==null){
                throw new NullPointerException("User passed in is null");
            }
            alertType = messageTemplate.alertType;
            String[] arguments = Fields.argumentsToArray(user, messageTemplate.arguments);
            message = messageTemplate.message(arguments);
            subject = messageTemplate.subject;
            title = messageTemplate.title;
            setUser(user);

        }
//
//        public AlertBuilder addAttachment(EmailAlertAttachment attachment){
//            if(emailAttachments==null){
//                emailAttachments = new ArrayList<>();
//            }
//
//            return null;
//        }
        public AlertBuilder setMessage(String message){
            this.message = message;
            return this;
        }

        public AlertBuilder setMessageAugment(String... vars){
            this.message = String.format(message, vars);
            Logger.info(message);
            return this;
        }

        public AlertBuilder setAlertType(AlertType alertType){
            this.alertType = alertType;
            return this;
        }

        public AlertBuilder setUser(User user){
            this.user = user;
            this.receiver = user.email;
            this.receiverName = user.fullname;
            return this;
        }

        public Alert build(){
            Alert alert =
                    new Alert(alertType, message, status, receiver, receiverName, subject);
            return alert;
        }
    }

}