package models;

/** The <b>Role</b> enum stores the different types of
 * roles available to users.
 */
public enum Role {
    STANDARD,
    ADMIN
}
