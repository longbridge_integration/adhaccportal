package controllers.authorizer;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import controllers.usermanagement.*;
import external.*;
import models.Role;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import utils.finacle.FinacleDataAccessObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static utils.AccountStatementsPDFHelper.getAccountStatement;

/**
 * Created by chigozirim on 11/23/15.
 */
@RoleSecuredMultiple(roles = {Role.ADMIN, Role.STANDARD})
@Security.Authenticated(Secured.class)
@With(FirstTimeLogOnAction.class)
public class StatementController extends AlertControllerFrame {
    /** Loads up the AccountStatement selection view
     *
     * @return
     */
    public Result index() {
        play.Logger.info("Number of contacts: " + CustomerContact.numberOfCustomers());
        return view();
    }

    /** This renders the form for entering details to be used in loading up and displaying the AccountStatement
     * to the user
     * @return
     */
    public Result view() {
        getCurrentUser();
        if(currentUser == null){
            return redirect("/");
        }
        if (request().method().equalsIgnoreCase("GET")) {
            return ok(views.html.accountstatements.view.render());
        } else {
            DynamicForm form = Form.form().bindFromRequest();
            // String searchBy = form.get("searchByField");
            String acid = form.get("account");
            String fromDate = form.get("fromDate");
            String toDate = form.get("toDate");
            String cifId = Account.findByAccountId(acid).customerId;

            //you can only view your own account statement
            if(!cifId.equals(currentUser.customerId)){
                flash("danger", "You can only view account statements of your account");
                return redirect(routes.StatementController.view());
            }
            //view the account statement
            return getAccountStatement(cifId, acid, fromDate, toDate);
        }
    }

    public Result viewSingle(String accountId){
         if (request().method().equalsIgnoreCase("GET")) {
            return ok(views.html.accountstatements.viewsingle.render(accountId));
        } else {
            DynamicForm form = Form.form().bindFromRequest();
            // String searchBy = form.get("searchByField");
            String acid = form.get("account");
            String fromDate = form.get("fromDate");
            String toDate = form.get("toDate");
            String cifId = Account.findByAccountId(acid).customerId;

            //you can only view your own account statement
            if(!cifId.equals(currentUser.customerId)){
                flash("danger", "You can only view account statements of your account");
                return redirect(routes.StatementController.view());
            }

            //view the account statement
            return getAccountStatement(cifId, acid, fromDate, toDate);
        }
    }



    /** This gets html <pre><select></pre>data for populate the customer name and customer id of a customer depending on the
     * value of <code>searchBy</code>
     * @param searchBy This is either an  account number or a customer id. If it is an account number
     *                 it is prepended with a -. Else it is treated as a customer id.
     *                 Example: <code>getAssociatedCustomer("C000001");</code> will fetch the customer name using C000001 as the customer id
     *                 while <code>getAssociatedCustomer("-C0000001");</code> will fetch the customer name using C000000001 as the account number
     * @return A Promise of a Result containing a single option tag which has the name of the customer or Account doesn't exist if the account
     * is not found. It could also be Database error if there is a db error.
     */
    public Result getAssociatedCustomer(String searchBy){
        //get the associated customers
        List<Customer> associatedCustomers = FinacleDataAccessObject.CustomerDAO.getAssociatedCustomer(searchBy);

         StringBuilder builder = new StringBuilder();
         for(Customer customer: associatedCustomers){
                        builder.append(String.format("<option value='%s'>%s %s</option>%n",
                                customer.customerId,(customer.firstName==null)?"":customer.firstName,
                                (customer.lastName==null)?"":customer.lastName ));
         }
         return ok(builder.toString());
    }

    public Result getAllAssociatedAccounts(String searchBy){
        //get the associated accounts
        List<Account> associatedAccounts = FinacleDataAccessObject.CustomerDAO.getAllAssociatedAccounts(searchBy);

         StringBuilder builder = new StringBuilder();
         for(Account account: associatedAccounts){
                        builder.append(String.format("<option %s>%s - %s - %s</option>%n",
                                (account.accountId==null)?"":"value='"+account.accountId+"'",
                                (account.accountNumber==null)?"":account.accountNumber,
                                (account.accountName==null)?"":account.accountName ,
                                (account.schemeType==null)?"":account.schemeType ));
         }
         return ok(builder.toString());
    }

}
