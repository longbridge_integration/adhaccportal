package controllers;

import controllers.usermanagement.RoleSecuredMultiple;
import controllers.usermanagement.Secured;
import external.Customer;
import models.Role;
import models.SecurityQuestion;
import models.form.ForgotPasswordForm;
import models.form.SecurityQuestionForm;
import play.Logger;
import play.data.DynamicForm;
import utils.Hash;
import utils.HostMaster;
import models.form.ChangePasswordForm;
import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import utils.AppException;

import java.util.ArrayList;
import java.util.List;

import static models.SecurityQuestion.getSecurityQuestions;
import static models.SecurityQuestion.getSecurityQuestionsByCategory;
import static play.data.Form.form;

/** Controller for changing user password
 *
 */
@RoleSecuredMultiple(roles = {Role.ADMIN, Role.STANDARD})
@Security.Authenticated(Secured.class)
public class Password extends Controller {

    private HostMaster hm ;
    private User currentUser ;

    private User getCurrentUser(){
        if(this.hm == null){
            this.hm = new HostMaster();
        }
        this.currentUser = hm.getCurrentUser();

        return this.currentUser;
    }

    /** This loads the change password form
     *
     * @return Result loading the change password form
     */
    public Result index(){

        Form<ChangePasswordForm> changePasswordForm  = form(ChangePasswordForm.class);
        List<SecurityQuestion> questions = SecurityQuestion.getSecurityQuestions();
        getCurrentUser();
        ChangePasswordForm form = new ChangePasswordForm();
        form.securityAnswer1 = currentUser.securityAnswerOne;
        form.securityQuestion1 = currentUser.securityQuestionOne;
        changePasswordForm = changePasswordForm.fill(form);

        Logger.info(changePasswordForm.toString());
        return ok(views.html.changepassword.index.render(changePasswordForm, questions));
    }




    /** This Action receives the password change input and changes the user's  password
     * accordingly.
     * @return Result with a flash message telling whether password was changed or not.
     */
    public Result changePassword(){
        Form<ChangePasswordForm> changePasswordForm  = form(ChangePasswordForm.class).bindFromRequest();
        List<SecurityQuestion> questions = getSecurityQuestions();

        if(changePasswordForm.hasErrors()){
            play.Logger.error(changePasswordForm.errors().toString());
            play.Logger.error("Form errors {} ",changePasswordForm.errorsAsJson().asText());
            if(changePasswordForm.hasGlobalErrors()){
                play.Logger.error("Form errors {} ", changePasswordForm.globalErrors().stream().map(validationError -> validationError.message()));
            }

            return ok(views.html.changepassword.index.render(changePasswordForm,  questions));
        }

        ChangePasswordForm changePasswordFormData = changePasswordForm.get();

        try {
            getCurrentUser().changePassword(changePasswordFormData.newPassword);
            currentUser.securityAnswerOne = (changePasswordFormData.securityAnswer1);
            currentUser.securityQuestionOne = (changePasswordFormData.securityQuestion1);
            currentUser.save();
            flash("success", "Password changed. Kindly login using new credentials");
            return redirect(routes.Application.logout());

        } catch (AppException e) {
            flash("danger", "Problem encountered during password change. Please contact administrator for assistance.");
            play.Logger.error("Problem changing password {}",e);
            return ok(views.html.changepassword.index.render(changePasswordForm, questions));

        }

    }


}
