package controllers.admin;

import com.fasterxml.jackson.databind.node.ObjectNode;
import external.CustomerContact;
import org.json.simple.JSONObject;
import play.Logger;
import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.*;
import scala.util.parsing.json.JSON;
import utils.HostMaster;
import controllers.usermanagement.FirstTimeLogOnAction;
import controllers.usermanagement.RoleSecured;
import controllers.usermanagement.Secured;

import models.audittrails.Event;
import models.audittrails.Login;
import models.audittrails.UserHistory;
import models.form.UserForm;
import models.Role;
import models.User;
import org.apache.commons.lang3.StringUtils;
import play.data.Form;
import play.libs.F.Function;
import play.libs.F.Promise;
import utils.AppException;
import utils.Hash;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/** Controller for managing and creating users. It is a RoleSecured controller requiring ADMIN role
 * for access.
 */
@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
@With(FirstTimeLogOnAction.class)
public class UserController extends Controller {

    /** Action for rendering the home page of the User Controller
     *
     * @return
     */
    public Promise<Result> index(){
        return User.find().map(new Function<List<User>, Result>() {
            @Override
            public Result apply(List<User> users) throws Throwable {
                return ok(views.html.user.list.render(users));
            }
        });
    }

    /** Renders the form for creating a new user*/
    public Result create(){
        Form<UserForm> userForm  = Form.form(UserForm.class);
        return ok(views.html.user.create.render(userForm));
    }

    /** Fetches the <b>User</b> and renders the edit Form
     *
     * @param id The id of the <b>User</b> in question
     * @return
     */
    public Promise<Result> edit(final long id){

        return User.find(id).map(new Function<User, Result>() {
            @Override
            public Result apply(User user) throws Throwable {
                if(user == null){
                    flash("warning", "The specified user was not found");
                    return redirect(controllers.admin.routes.UserController.index());
                }

                UserForm userForm = new UserForm();
                userForm.userName = user.username;
                userForm.id = user.id;

                Form<UserForm> editForm =  Form.form(UserForm.class).fill(userForm);

                return ok(views.html.user.edit.render(editForm));
            }
        });

    }


    /** Enables the <b>User</b> with the specified id
     *
     * @param id The id of the <b>User</b>
     * @return
     */
    public Promise<Result> enable(final long id){

        return User.find(id).map(new Function<User, Result>() {
            @Override
            public Result apply(User user) throws Throwable {
                if(user == null){
                    flash("warning", "The specified user was not found");
                    return redirect(controllers.admin.routes.UserController.index());
                }
                if(!user.isEnabled){
                    try{
                        user.enableUser(getCurrentUser());
                    }catch(Exception exp){
                        play.Logger.error("Failed to update user {}", exp);
                        flash("warning", "Technical error. Failed to enable user");
                        return redirect(controllers.admin.routes.UserController.index());
                    }
                }
                flash("success", "User has been enabled");
                return redirect(controllers.admin.routes.UserController.index());
            }
        });

    }

    /** Disables the <b>User</b> with the specified id
     *
     * @param id The id of the <b>User</b>
     * @return
     */
    public Promise<Result> disable(final long id){

        play.Logger.info("Attempting to disable User[id={}]",id);

        return User.find(id).map(new Function<User, Result>() {
            @Override
            public Result apply(User user) throws Throwable {
                if(user == null){
                    play.Logger.info("User[id={}] Not Found",id);
                    flash("warning", "The specified user was not found");
                    return redirect(routes.UserController.index());
                }
                if(user.isEnabled !=false){
                    try{
                        user.disableUser();
                        play.Logger.info("User[id={}] Disabled",id);
                    }catch(Exception exp){
                        play.Logger.error("Failed to update user {}", exp);
                        flash("warning", "Technical error. Failed to disable user");
                        return redirect(routes.UserController.index());
                    }
                }
                flash("success","User has been disabled");
                return redirect(routes.UserController.index());
            }
        });

    }


    public Promise<Result> view(final long id){


        return User.find(id).map(new Function<User, Result>() {
            @Override
            public Result apply(User user) throws Throwable {

                if(user == null){
                    flash("warning","The specified user was not found");

                    return redirect(routes.UserController.index());
                }

                List<Login> wrongPasswordLogins = Login.find.where().eq("username",user.username).findList();

                return ok(views.html.user.show.render(user,wrongPasswordLogins));
            }
        });

    }

    public Result save(){

        Form<UserForm> userForm = Form.form(UserForm.class).bindFromRequest();

        if(userForm.hasErrors()){

            return badRequest(views.html.user.create.render(userForm));
        }

        UserForm data =  userForm.get();

        User user = new User();
        user.role = data.role;
        user.username = data.userName;
        user.customerId = data.customerId;
        user.fullname = data.fullname;
        user.email = CustomerContact.fetchEmail(user.customerId);

        if(user.email == null){
            //user does not have a valid email record in the db
            flash("danger", "Can not add user: User does not have a valid email address");
            return badRequest(views.html.user.create.render(userForm));
        }

        try{
            user.passwordHash = Hash.createPassword(data.password);

            user.userHistories = new ArrayList();
            UserHistory userHistory = new UserHistory();
            userHistory.user= user;
            userHistory.initiator = getCurrentUser();
            userHistory.event = Event.CREATED;
            userHistory.initiator= getCurrentUser();
            userHistory.eventDate = new Date();
            user.userHistories.add(userHistory);
            user.save();

        }
        catch (NullPointerException appException){
            flash("danger","Technical Error. Please try again");
            play.Logger.error("{}",appException);
            return badRequest(views.html.user.create.render(userForm));
        }catch(Exception exp){
            flash("danger","Technical Error. Please try again");
            play.Logger.error("{}",exp);
            return badRequest(views.html.user.create.render(userForm));
        }

        flash("success","User created successfully");
        return redirect(routes.UserController.index());
    }


    public Result update(){
        Form<UserForm> userForm = Form.form(UserForm.class).bindFromRequest();
        if(userForm.hasErrors()){
            return badRequest(views.html.user.create.render(userForm));
        }

        UserForm data = userForm.get();
        User user = User.findByUserName(data.userName);

        if(user == null ){
            play.Logger.error("The supplied username was not found");
            flash("warn","The supplied user was not found");
            return redirect(routes.UserController.index());
        }

        user.role = data.role;
        user.save();

        if(StringUtils.isNotBlank(data.password)){
            play.Logger.info("Password is not blank. Begin password change");
            try {
                user.changePassword(data.password);
                play.Logger.info("Password updated successfully");
                flash("success","User password updated");
            } catch (AppException e) {
                play.Logger.error("Problem updating user password {}",e);
                flash("danger","Technical error. Please contact system administrator for assistance");
            }
        }



        return redirect(routes.UserController.index());
    }



    //Dirty fix for play's new routing DI
    private HostMaster hm ;
    private User currentUser ;

    private User getCurrentUser(){
        if(this.hm == null){
            this.hm = new HostMaster();
        }
        this.currentUser = hm.getCurrentUser();
        return this.currentUser;
    }
}
