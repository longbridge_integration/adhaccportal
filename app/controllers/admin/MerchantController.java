package controllers.admin;

import controllers.usermanagement.FirstTimeLogOnAction;
import controllers.usermanagement.RoleSecuredMultiple;
import controllers.usermanagement.Secured;
import forms.LinkAccountForm;
import forms.MerchantCreationForm;
import models.Role;
import play.Logger;
import play.data.Form;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;

/**
 * Created by Longbridge PC on 8/2/2017.
 */
@Security.Authenticated(Secured.class)
@RoleSecuredMultiple(roles = {Role.ADMIN, Role.STANDARD})
@With(FirstTimeLogOnAction.class)
public class MerchantController extends Controller {

    public Promise<Result> index(){
        return Promise.pure(ok(views.html.merchant.index.render()));
    }

    public Promise<Result> linkAccounts(){
        return Promise.pure(ok(views.html.merchant.linkedaccounts.render()));
    }

    public Promise<Result> addMerchant(){
        return Promise.pure(ok(views.html.merchant.createmerchant.render()));
    }

    public Promise<Result> createMerchant(){
        Form<MerchantCreationForm> merchantCreationForm = Form.form(MerchantCreationForm.class).bindFromRequest();
        Logger.info("merchant id: "+merchantCreationForm.get().merchantid);
        Logger.info("cif id: "+merchantCreationForm.get().cifid);
        if(merchantCreationForm.hasErrors()){
            Logger.error("Form has errors {} ", merchantCreationForm.errorsAsJson().toString());
            return Promise.pure(badRequest(views.html.merchant.createmerchant.render()));
        }
        Logger.info("form has no errors");
        flash("success","successfully created");
        return Promise.pure(redirect(routes.MerchantController.index()));
    }

    public Promise<Result> activateMerchant(){
        flash("success","successfully activated");
        return Promise.pure(redirect(routes.MerchantController.index()));
    }

    public Promise<Result> deactivateMerchant(){
        flash("danger","successfully deactivated");
        return Promise.pure(redirect(routes.MerchantController.index()));
    }

    public Promise<Result> validate(){
        Form<LinkAccountForm> linkAccountForm = Form.form(LinkAccountForm.class).bindFromRequest();
        Logger.info("merchant bank: "+linkAccountForm.get().merchantbank);
        Logger.info("merchantid: "+linkAccountForm.get().merchantid);
        if(linkAccountForm.hasErrors()){
            Logger.error("Form has errors {} ", linkAccountForm.errorsAsJson().toString());
            return Promise.pure(badRequest(views.html.merchant.linkedaccounts.render()));
        }
        if(linkAccountForm.get().merchantbank.contains("select a")){
            Logger.error("user did not select a bank");
            flash("danger","You must select a bank");
            return Promise.pure(redirect(routes.MerchantController.linkAccounts()));
        }
        Logger.info("form has no errors");
        flash("success","successfully validated");
        return Promise.pure(redirect(routes.MerchantController.linkAccounts()));
    }
}
