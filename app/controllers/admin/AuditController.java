package controllers.admin;

import com.avaje.ebean.Expr;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.usermanagement.FirstTimeLogOnAction;
import controllers.usermanagement.RoleSecured;
import controllers.usermanagement.Secured;
import external.Account;
import models.Role;
import models.audittrails.Login;
import play.Logger;
import play.libs.F.Function;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;

import java.util.List;
import java.util.Map;



/** Controller for the viewing Audits of work done on the application.
 * The Audit Controller shows all login attempts.
 */
@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
@With(FirstTimeLogOnAction.class)
public class AuditController extends Controller {

    public Promise<Result> index(){
        return loginAudits();
    }

    public Promise<Result> loginAudits(){

        return Login.find().map(new Function<List<Login>, Result>() {
            @Override
            public Result apply(List<Login> logins) throws Throwable {
                return ok(views.html.audits.loginaudits.list.render(logins));
            }
        });

    }


    public Result accountsJSON(){
                // Get parameters
        Map<String, String[]> params = request().queryString();

        //Get the column filter parameters

        Integer totalRecords = Account.find
                                    .where()
                                    .eq("schemeCode", "303")
                                    .findRowCount();

        String filter = params.get("search[value]")[0];
        Integer pageSize = Integer.valueOf(params.get("length")[0]);
        Integer page = Integer.valueOf(params.get("start")[0])
                / pageSize;

        /**
         * Get sorting order and column
         */
        String sortBy = null;
        String order = params.get("order[0][dir]")[0];

        switch (Integer.valueOf(params.get("order[0][column]")[0])) {
    
            case 0:
                sortBy = "id";
                break;
            case 1:
                sortBy = "customerId";
                break;
            case 2:
                sortBy = "accountName";
                break;
            case 3:
                sortBy = "accountNumber";
                break;
            case 4:
                sortBy = "accountBalance";
                break;
            default:
                sortBy = "id";
                break;
        }

        //Get the page to show from db
        PagedList<Account> accountPage = null;

        accountPage = Account.find.where().and(
                Expr.eq("schemeCode", "%" + filter + "%"),
                Expr.or(Expr.ilike("customerId", "%" + filter + "%"),
                        Expr.or(Expr.ilike("accountName", "%" + filter + "%"),
                                Expr.or(Expr.ilike("accountNumber", "%" + filter + "%"),
                                    Expr.or(Expr.ilike("accountBalance", "%" + filter + "%"),
                                        Expr.ilike("id", "%" + filter + "%")))
                                ))).orderBy(sortBy + " " + order + ", id " + order)
                .findPagedList(page, pageSize);


        Integer totalDisplayRecords = accountPage.getTotalRowCount();

        Logger.debug("records fetched " + totalDisplayRecords);

        /**
         * Construct the JSON to return
         */
        ObjectNode result = Json.newObject();

        result.put("recordsTotal", totalRecords);
        result.put("recordsFiltered", totalDisplayRecords);

        ArrayNode an = result.putArray("data");

        for (Account acc : accountPage.getList()) {
            ObjectNode row = Json.newObject();
            row.put("0", acc.currencyCode);
            row.put("1", acc.customerId);
            row.put("2", acc.accountName);
            row.put("3", acc.accountNumber);
            row.put("4", acc.accountBalance);
            an.add(row);
        }

        return ok(result);
    }


    //select all the accounts that aren't closed
    //select the account balance of all of them
    // Dashboard
    // Name                 Graph
    // chigozirim torti
}
