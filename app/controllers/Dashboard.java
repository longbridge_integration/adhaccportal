package controllers;

import controllers.usermanagement.FirstTimeLogOnAction;
import controllers.usermanagement.RoleSecuredMultiple;
import controllers.usermanagement.Secured;
import models.Role;
import play.Logger;
import utils.HostMaster;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;

/** Controller contains the Action for loading up the Dashboard to the user.
 * <i>This controller is authenticated with the <code>Secured</code> class hence will only load actions if a user is logged in. </i>
 */
@Security.Authenticated(Secured.class)
@With(FirstTimeLogOnAction.class)
@RoleSecuredMultiple(roles = {Role.ADMIN, Role.STANDARD})
public class Dashboard extends Controller {
    private HostMaster hm;
    private User currentUser;
    private User getCurrentUser(){
        if(this.hm == null){
            this.hm = new HostMaster();
        }
        this.currentUser = hm.getCurrentUser();
        return this.currentUser;
    }


    /** This loads the appropriate Dashboard view depending on the role of the looged in user.
     *
     * @return Result loading the appropriate Dashboard
     */
    public Result index() {
        Logger.info(request().getHeader("origin"));
        Logger.info(request().getHeader("referrer"));

        Result whichResult = null;
        switch (getCurrentUser().role) {
            case ADMIN:
                whichResult = ok(views.html.dashboard.admin.render(currentUser));
                break;
            case STANDARD:
                whichResult = ok(views.html.dashboard.standard.render(currentUser));
        }
        return whichResult;
    }

    public Result splash(){
        return ok(views.html.dashboard.splash.render());
    }
}