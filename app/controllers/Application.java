package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import external.Account;
import external.Customer;
import external.FinacleTran;
import external.FinacleTranToday;
import models.Role;
import models.SecurityQuestion;
import models.User;
import models.form.ForgotPasswordForm;
import models.form.LoginForm;
import models.form.SecurityQuestionForm;
import org.apache.commons.lang.RandomStringUtils;
import play.Logger;
import play.cache.Cache;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import utils.Hash;
import utils.HostMaster;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import static models.SecurityQuestion.createSecurityQuestions;
import static models.SecurityQuestion.isSecurityQuestionExist;

/**
 * The Application controller handles the login , logout and dashboard actions
 */
@With(HttpsAction.class)
public class Application extends Controller {
    public Result HOME;
    public Result DASHBOARD;

    private HostMaster hm;
    private User currentUser;
    private static final String WELCOME_IMAGE_LOCATION = "img/";
    private static String[] ANTI_PHISHING_IMAGES = new String[]{
            WELCOME_IMAGE_LOCATION + "1.jpg",
            WELCOME_IMAGE_LOCATION + "2.jpg",
            WELCOME_IMAGE_LOCATION + "3.jpg",
            WELCOME_IMAGE_LOCATION + "4.jpg",
            //  WELCOME_IMAGE_LOCATION + "5.jpg",
            WELCOME_IMAGE_LOCATION + "6.jpg",
            WELCOME_IMAGE_LOCATION + "7.jpg"};


    public Application() {
        this.HOME = redirect(routes.Application.index());
        this.DASHBOARD = redirect(routes.Dashboard.index());
    }

    private User getCurrentUser() {
        if (hm == null) {
            hm = new HostMaster();
        }
        this.currentUser = hm.getCurrentUser();
        return this.currentUser;
    }


    /**
     * This returns the URL of one of the welcome images
     * the returns are not ordered and are gotten at random
     *
     * @return
     */
    public static String getRandomWelcomeImage() {
        Random random = new Random();
        return ANTI_PHISHING_IMAGES[random.nextInt(6)];
    }

    /**
     * This <code>Action</code> is called when the home page / is called
     *
     * @return Result that loads the Dashboard or the Login page
     */
    public Result index() {
        Logger.info("Application has started");
        currentUser = getCurrentUser();
        //check if a user is logged in
        if (currentUser != null && currentUser.isEnabled) {
//            return redirect(routes.Dashboard.splash());
            return redirect(routes.Dashboard.index());
        } else {
        play.Logger.info("User account not found or disabled");
        play.Logger.info("Clearing invalid session credentials");
        session().clear();
        }
        return showLogin();
    }

    /**
     * Shows the Login form page
     *
     * @return Result loading the Login form page
     */
    public Result showLogin() {
        Form<LoginForm> loginForm = Form.form(LoginForm.class);
        return ok(views.html.login.render(loginForm));
    }

    /**
     * This attempts to log in the user using the details entered in the
     * login form view.
     * <p>
     * URL:  POST /login
     *
     * @return Result loading the dashboard page if the login is successful
     */
    public Result login() {
        Form<LoginForm> loginForm = Form.form(LoginForm.class).bindFromRequest();
        DynamicForm requestData = Form.form().bindFromRequest();

        if (loginForm.hasErrors()) {
            play.Logger.error("Form has error -- {}", loginForm.errorsAsJson().toString());
            return badRequest(views.html.login.render(loginForm));
        }
        session("auth_user_name", loginForm.get().username);
        session("session_id", "CMBTREL" + (new SimpleDateFormat("MMyyddHHmmss").format(new Date())) + RandomStringUtils.randomNumeric(10));
        return redirect(routes.Dashboard.index());
    }

    /**
     * This logs out the current user and returns to the log in page
     * URL: GET /logout
     *
     * @return Result that loads the log in page
     */
    public Result logout() {
        Logger.debug("Logout initiated");
        currentUser = getCurrentUser();
        if (currentUser == null) {
            Logger.info("No user is logged in");
        } else {
            Cache.remove(currentUser.username + "_auth_user");
            session().clear();
            Logger.info("Cache and Session cleared");
        }
        Logger.info("user ended session");
        flash("success", "Logout Successful");
        return HOME;
    }


    /**
     * This {@link Action} handles an ajax request sent to confirm if the {@link models.User} specified by
     * the {@code customerId} exists. The inputs are passed in a JSON object and the result
     * is also passed in a JSON object containing a message and a boolean variable which is true if the user
     * exists  or false is the user doesn't exist
     * <p>
     * POST    /user/exists
     *
     * @return
     */
    public Result userExists() {
        DynamicForm form = Form.form().bindFromRequest();
        String customerId = form.get("customerId");
        if (customerId == null) {
            return badRequest(createMessage("User not found", false));
        }
        Logger.info(customerId);
        Logger.debug("Customer id {}", customerId);

        if (User.find.where().ieq("customerId", customerId).findRowCount() > 0) {
            //the user exists
            //user found
            return ok(createMessage("User exists", true)).as("application/json");

        } else {
            return ok(createMessage("User not found", false)).as("application/json");
        }
    }

    /**
     * This Action is called when the URL '/user/forgotpassword' is navigated to.
     * It displays a form for the user to enter their email address and account number.
     *
     * @return
     */
    public Result forgetPasswordOne() {
        Form<ForgotPasswordForm> form = Form.form(ForgotPasswordForm.class);
        return ok(views.html.forgotpassword.render(form));
    }

    /**
     * This Action is called when the forgotpassword form is submitted.
     * POST     /user/securityquestion
     * It displays a form for the user to select their security question and the answer
     *
     * @return
     */
    public Result forgetPasswordTwo() {
        Form<ForgotPasswordForm> forgotPasswordForm = Form.form(ForgotPasswordForm.class).bindFromRequest();

        Logger.info(forgotPasswordForm.toString());
        if (forgotPasswordForm.hasErrors()) {
            play.Logger.error("Form has error -- {}", forgotPasswordForm.errorsAsJson().toString());
            return badRequest(views.html.forgotpassword.render(forgotPasswordForm));
        }

        String username = forgotPasswordForm.get().username;
        String email = forgotPasswordForm.get().emailAddress;

        List<SecurityQuestion> questions1 = SecurityQuestion.getSecurityQuestions();

        if (User.validateUsernameAndEmail(username, email)) {
            //found the user
            Form<SecurityQuestionForm> form = Form.form(SecurityQuestionForm.class).bindFromRequest();
            SecurityQuestionForm filler = new SecurityQuestionForm();
            filler.username = username;
            form = form.fill(filler);
            Logger.info(form.toString());
            return ok(views.html.securityquestion.render(form, questions1));
        }
        flash("danger", "Username and email do not match");
        return redirect(controllers.routes.Application.forgetPasswordOne());
    }


    /**
     * This resets the {@link Customer}s password after the security question is submitted
     *
     * @return
     */
    public Result forgotPassword() {
        Form<SecurityQuestionForm> securityQuestionForm = Form.form(SecurityQuestionForm.class).bindFromRequest();

        Logger.info(securityQuestionForm.toString());
        List<SecurityQuestion> questions1 = SecurityQuestion.getSecurityQuestions();

        if (securityQuestionForm.hasErrors()) {

            play.Logger.error("Form has error -- {}", securityQuestionForm.errorsAsJson().toString());
            return badRequest(views.html.securityquestion.render(securityQuestionForm, questions1));
        }

        flash("success", "A new password was sent to your email. Please check your mail");
        return redirect(routes.Application.index());

    }

    private ObjectNode createMessage(String message, boolean successOrFailure) {
        ObjectNode object = Json.newObject();
        object.put("message", message);
        object.put("success", successOrFailure);
        return object;
    }

    public Result getImage(String path) {
        File image = new File("images/" + path);
        try {
            image.toString();
        } catch (Exception e) {
            play.Logger.error("Error:", e);
        }
        return ok(image);
    }


    /**
     * This <code>Result</code> loads the signature file and displays the signature image as
     * a Result
     *
     * @param imageURL - The url of the signature on the server
     * @return - Result showing the signature image
     */
    public Result getSignature(String imageURL) {

        File image = new File("signature/" + imageURL);
        try {
            image.toString();
        } catch (Exception e) {
            play.Logger.error("Error:", e);
        }
        return ok(image);
    }

}
