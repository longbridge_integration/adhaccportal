package controllers.usermanagement;

import controllers.routes;
import utils.HostMaster;
import models.User;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import static play.mvc.Controller.flash;

/** This Action class ensures that a newly created user changes his password before he can start
 * using the system.
 */
public class FirstTimeLogOnAction extends Action.Simple{
    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {

        User user = (new HostMaster().getCurrentUser());

        if(user.isFirstLogin){
            flash("info", "Password change is required");
            play.Logger.info("User hasn't changed his password. Redirecting to password change page");

            return F.Promise.pure(redirect(routes.Password.index()));
        }

        play.Logger.info("Not first login");
        return delegate.call(context);
    }
}
