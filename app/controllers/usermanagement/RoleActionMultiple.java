package controllers.usermanagement;

import controllers.HttpsAction;
import controllers.routes;
import play.Logger;
import play.mvc.With;
import utils.HostMaster;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Role;
import models.User;
import play.libs.F;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.Arrays;

import static play.mvc.Controller.flash;

/**
 */
@With(HttpsAction.class)
public class RoleActionMultiple extends Action<RoleSecuredMultiple> {
    @Override
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {

        User user = (new HostMaster().getCurrentUser());

        if(user == null){
            ObjectNode output = Json.newObject();
            output.put("message", "No user is logged in");
            return F.Promise.pure(ok(output));
        }

        Role[] roles = configuration.roles();

        if(roles == null){
            ObjectNode output = Json.newObject();
            output.put("message", "No role passed in");
            return F.Promise.pure(ok(output));
        }

        if(!Arrays.asList(roles).contains(user.role)){

            play.Logger.info("Role requirement failed. Required profiles - {} , User profile {}", Arrays.toString(roles),user.role);

            String message = "Your current profile does not permit you to carry out this task.";


            if(configuration.isJsonResponse()){

                ObjectNode output = Json.newObject();

                output.put("message",message);

                return F.Promise.<Result>pure(ok(output));
            }

            flash("danger", message);

            return F.Promise.pure(redirect(routes.Dashboard.index()));


        }

        play.Logger.info("User has the required role");
        return delegate.call(ctx);
    }
}
