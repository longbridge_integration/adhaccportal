package controllers.usermanagement;


import controllers.routes;
import models.User;
import play.Logger;
import play.Play;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;

import java.util.Date;

import static play.mvc.Controller.flash;
import static play.mvc.Controller.request;
import static play.mvc.Controller.session;

/** The Secured class provides methods which help to handle authentication of
 * the play application.
 */
public class Secured extends Security.Authenticator {
    private static final String target_origin = "http://127.0.0.1:9000";

    @Override
    public String getUsername(Http.Context ctx) {
        Logger.info(request().getHeader("origin"));
        if(request().hasHeader("origin")){
//            if(!request().getHeader("origin").equals(target_origin)){
//                Logger.error("Invalid origin {}", request().getHeader("origin"));
//                flash("danger", "Invalid origin");
//                session().clear();
//                return null;
//            }
        }

        Logger.info("interception " + request().headers().toString());
        // see if the session is expired
        String previousTick = ctx.session().get("userTime");
        Logger.info("userTime ; {}", previousTick);

        if (previousTick != null && !previousTick.equals("")) {

            long previousT = Long.valueOf(previousTick);
            long currentT = new Date().getTime();

            Logger.debug("userTime ; {} {}", previousT,currentT);
            long timeout = Long.valueOf(Play.application().configuration().getString("app.session.timeout")) * 1000 * 60;
            if ((currentT - previousT) > timeout) {
                // session expired
                flash("danger", "Session has timed out");
                //ctx.response().setCookie("isTimeOut","true");
                ctx.args.put("isTimeOut","true");
                session().clear();
                return null;
            }
        }

        // update time in session
        String tickString = Long.toString(new Date().getTime());
        session("userTime", tickString);

        return ctx.session().get("auth_user_name");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        flash("danger", "kindly login first");
        play.Logger.debug("URL {}", ctx.request().uri().toString());
        session("dest_url", ctx.request().uri().toString());

        ctx.session().put("dest_url",ctx.request().uri().toString() );

        play.Logger.debug("SessionDirect{}",session("dest_url"));

        Http.Context.current().session().put("dest_url", ctx.request().uri().toString());

        return redirect(routes.Application.showLogin());
    }
}