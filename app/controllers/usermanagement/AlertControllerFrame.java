package controllers.usermanagement;

import models.User;
import play.mvc.Controller;
import utils.HostMaster;

/**
 * Created by DELL on 8/18/2016.
 */
public class AlertControllerFrame extends Controller {
    private HostMaster hm;
    protected User currentUser;

    protected User getCurrentUser() {
        if (hm == null) {
            hm = new HostMaster();
        }
        this.currentUser = hm.getCurrentUser();
        return this.currentUser;
    }
}
