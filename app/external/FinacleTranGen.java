package external;

import com.avaje.ebean.Model;
import play.libs.Akka;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*** Created by chigozirim on 11/4/16.
 */
public class FinacleTranGen extends Model{

    public String accountId;

    public String customerId;

    public String narration;

    public String accountNumber;

    public Date postedDate;

    public String amount;

    public String debitOrCredit;

    public String currencyCode;

    public static Finder<Long, FinacleTranGen> find = new Finder<Long, FinacleTranGen>("secondary", FinacleTranGen.class);



    public String debitOrCreditHtml(){
        if(debitOrCredit.contains("D")){
            return "Debit";
        }
        else{
            return "Credit";
        }
    }
    public String debitOrCreditHtmlClass(){
        if(debitOrCredit.contains("D")){
            return "warning";
        }
        else{
            return "info";
        }
    }

    public String getFormattedPostedDate(){
        return new SimpleDateFormat("dd/MM/yyyy hh:mm aaa").format(postedDate);
    }

    public FinacleTranGen(FinacleTran tran){
        this.accountId = tran.accountId;
        this.customerId = tran.customerId;
        this.narration = tran.narration;
        this.postedDate = tran.postedDate;
        this.amount = tran.amount;
        this.debitOrCredit = tran.debitOrCredit;
        this.accountNumber = Account.findByAccountId(accountId).accountNumber;
        this.currencyCode = tran.currencyCode;
    }

    public FinacleTranGen(FinacleTranToday tran){
        this.accountId = tran.accountId;
        this.customerId = tran.customerId;
        this.narration = tran.narration;
        this.postedDate = tran.postedDate;
        this.amount = tran.amount;
        this.debitOrCredit = tran.debitOrCredit;
        this.accountNumber = Account.findByAccountId(accountId).accountNumber;
        this.currencyCode = tran.currencyCode;
    }


    public static List<FinacleTranGen> getTranGenList(List<FinacleTran> tran){
        List<FinacleTranGen> list = new ArrayList<>();
        for(FinacleTran trann: tran){
            list.add(new FinacleTranGen(trann));
        }
        return list;
    }

    public static List<FinacleTranGen> getTranGetList(List<FinacleTranToday> tran){
        List<FinacleTranGen> list = new ArrayList<>();
        for(FinacleTranToday trann: tran){
            list.add(new FinacleTranGen(trann));
        }
        return list;
    }

    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }


}
