package external;

import com.avaje.ebean.Model;
import play.libs.Akka;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/*** Created by chigozirim on 11/4/16.
 */
@Entity
@Table(name = "TBAADM.HTD")
public class FinacleTran extends Model{

    @Column(name = "ACID")
    public String accountId;

    @Column(name = "PSTD_FLG")
    public String postedFlag;

    @Column(name = "CUST_ID")
    public String customerId;

    @Column(name = "TRAN_PARTICULAR")
    public String narration;

    @Column(name = "PSTD_DATE")
    public Date postedDate;

    @Column(name = "TRAN_AMT")
    public String amount;

    @Column(name="PART_TRAN_TYPE")
    public String debitOrCredit;

    @Column(name="REF_CRNCY_CODE")
    public String currencyCode;

    public static Finder<Long, FinacleTran> find = new Finder<Long, FinacleTran>("secondary", FinacleTran.class);


    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }


}
