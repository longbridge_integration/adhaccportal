package external;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import play.Logger;
import play.libs.Akka;
import tyrex.util.ArraySet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

/**  CustomerContact represents a Customer's contact. This includes the Customer's
 * email and phone number.
 */
@Entity
@Table(name = "CRMUSER.PHONEEMAIL")
public class CustomerContact extends Model{

    @Column(name = "ORGKEY")
    public String customerId;

    @Column(name = "PHONENO")
    public String phoneNumber;

    @Column(name = "EMAIL")
    public String email;

    @Column(name = "PHONEOREMAIL")
    public String phoneOrEmail;

    @Column(name = "PREFERREDFLAG")
    public String preferredFlag;

    private static Map<String, Object> constraints = new HashMap<String, Object>();

    static{
    }

    public static Finder<Long, CustomerContact> find = new Finder<Long, CustomerContact>("secondary", CustomerContact.class);

    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }

    /** Fetches the email address of the customer with the specified <code>custid</code>
     *
     * @param custId The customer id/cif id of the customer
     * @return The email address of the customer stored in finacle or null if an exception occurs
     */
    public static String fetchEmail(String custId){
        play.Logger.info(custId);
        constraints = new HashMap<String, Object>();
        constraints.put("PREFERREDFLAG", "Y");
        constraints.put("PHONEOREMAIL", "EMAIL");
        constraints.put("ORGKEY", custId);
        
        List<CustomerContact> c = find.where().and(Expr.ilike("PREFERREDFLAG", "%Y%"),
                Expr.and(Expr.ilike("PHONEOREMAIL", "%EMAIL%"),
                        Expr.ilike("ORGKEY", custId.trim()))).findList();
        if(c == null || c.isEmpty()){
            return null;
        }
//    Logger.error("email: {}", c.email);
       try{
           if(validateEmail(c.get(0).email)){
               return c.get(0).email;
           }else{
               Logger.error("Invalid email: " + c.get(0).email);
               return null;
           }
       }catch(Exception e){
           return null;
       }

    }


    /** Fetches all the email addresses of the customer with the specified <code>custid</code>
     *
     * @param custId The customer id/cif id of the customer
     * @return A list of all email addresses of the customer stored in finacle or null if an exception occurs
     */
    public static List<String> fetchAllEmails(String custId){
        play.Logger.info(custId);
         constraints = new HashMap<String, Object>();
        constraints.remove("PREFERREDFLAG");
        constraints.put("PHONEOREMAIL", "EMAIL");
        constraints.put("ORGKEY", custId);
        List<CustomerContact> customerContacts = find.where().allEq(constraints).orderBy("PREFERREDFLAG desc").findList();

        Set<String> emails = new ArraySet();

            for(CustomerContact customerContact: customerContacts){

                Logger.info("Contact: {}", customerContact.email);
                try{
                    if(validateEmail(customerContact.email)){
                        emails.add(customerContact.email);
                    }else{
                        Logger.error("Invalid email: " + customerContact.email);
                    }
                }catch(Exception e){
                    Logger.error("Error: " , e);
                }
            }
        return new ArrayList<>(emails);
    }

    /** Validates the specified email address according to some rules
     *
     * @param email the email to validate
     * @return true if the email is valid or false otherwise
     */
    public static boolean validateEmail(String email){
        if(email.contains("NO_EMAIL") || email.contains("UPDATE_THIS") || email.contains("NOEMAIL")){
            return false;
        }else{
            return true;
        }
    }


    /** Fetches the phone number of the customer with the specified <code>custid</code>
     *
     * @param custId The customer id/cif id of the customer
     * @return The phone number of the customer stored in finacle else null
     */
    public static String fetchPhone(String custId){
        constraints = new HashMap<String, Object>();
        constraints.put("PHONEOREMAIL", "PHONE");
        constraints.put("PREFERREDFLAG", "Y");
        CustomerContact c = find.where().allEq(constraints).findUnique();
        try{
            return c.phoneNumber;
        }catch(Exception e){
            return null;
        }
    }

    /** Fetches the number of emails currently stored in
     * finacle.
     * @return Number of emails stored in finacle
     */
    public static Set<CustomerContact> getAllCustomerEmails(){
        constraints = new HashMap<String, Object>();
        constraints.put("PHONEOREMAIL", "EMAIL");
        constraints.put("PREFERREDFLAG", "Y");
        return new HashSet(find.where().allEq(constraints).not(Expr.or(Expr.ilike("EMAIL", "NO_EMAIL%"), Expr.ilike("EMAIL", "UPDATE_THIS%"))).findList());
    }

    /** Fetches the number of phone numbers currently stored in
     * finacle.
     * @return Number of phone numbers stored in finacle
     */
    public static Set<CustomerContact> getAllCustomerPhones(){
        constraints = new HashMap<String, Object>();
        constraints.put("PREFERREDFLAG", "Y");
        constraints.put("PHONEOREMAIL", "PHONE");
        return new HashSet(find.where().allEq(constraints).findList());
    }


    public static int numberOfEmails(){
        return 0;
    }

    public static int numberOfPhones(){
        return 0;
    }

    /** Fetches the number of unique customer contacts in finacle*/
    public static int numberOfCustomers() {
        int returned = -1;
        try(Connection connection= play.db.DB.getConnection("secondary");
        PreparedStatement statement = connection.prepareStatement("select count(distinct orgkey) from crmuser.phoneemail")){
            try(ResultSet rs = statement.executeQuery()){
                while(rs.next()){
                    returned = rs.getInt(1);
                }
            }
        }catch(Exception exc){
            play.Logger.error("An error occurred while fetching the current number of customers");
            returned = -1;
        }
        return returned;
    }

    @Override
    public String toString() {
        return "Crmuser{" +
                "customerId='" + customerId + '\'' +
                ", phoneOrEmail='" + phoneOrEmail + '\'' +
                ", phone='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        CustomerContact contact = (CustomerContact) obj;
        if(contact.phoneOrEmail.equals(phoneOrEmail)){
            if(contact.phoneOrEmail.equals("PHONE")){
                return contact.phoneNumber.equals(phoneNumber);
            }else{
                return contact.email.equals(email);
            }
        }else{
            return false;
        }
    }
}
