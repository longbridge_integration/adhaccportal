package external;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/** External Entity representing the frequency of AccountStatement to be received by
 * a customer.
 * Created by chigozirim on 15/02/2016.
 */
public final class AccountStatementDetail {
    public String customerId;
    public String email;
    public String accountId;
    public String psFrequencyType;
    public String psNextDueDate;

    private static String getDateString(String date) {
        return String.format("%s/%s/%s", date.substring(5, 7), date.substring(8, 10), date.substring(0, 4));
    }
    private static final SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");

    private static final String accountDetailSQL = "SELECT G.CIF_ID, E.EMAIL, G.ACID, " +
            "A.PS_FREQ_TYPE,A.PS_NEXT_DUE_DATE FROM TBAADM.AST A, TBAADM.GAM G, CRMUSER.PHONEEMAIL E WHERE " +
            " G.ACID = A.ACID AND G.CIF_ID = E.ORGKEY AND E.PREFERREDFLAG = 'Y'" +
            " AND E.PHONEOREMAIL = 'EMAIL' AND A.PS_NEXT_DUE_DATE = TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS')";

    private static final String accountDetailNextSQL = "SELECT G.CIF_ID, E.EMAIL, G.ACID, " +
            "A.PS_FREQ_TYPE,A.PS_NEXT_DUE_DATE FROM TBAADM.AST A, TBAADM.GAM G, CRMUSER.PHONEEMAIL E WHERE " +
            " G.ACID = A.ACID AND G.CIF_ID = E.ORGKEY AND E.PREFERREDFLAG = 'Y'" +
            " AND E.PHONEOREMAIL = 'EMAIL' " +
            " AND (A.PS_NEXT_DUE_DATE = TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS')" +
            " OR A.PS_NEXT_DUE_DATE = TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS'))" +
            " AND A.PS_FREQ_HLDY_STAT = 'N'";

    private static final String accountDetailPreviousSQL = "SELECT G.CIF_ID, E.EMAIL, G.ACID, " +
            "A.PS_FREQ_TYPE,A.PS_NEXT_DUE_DATE FROM TBAADM.AST A, TBAADM.GAM G, CRMUSER.PHONEEMAIL E WHERE " +
            " G.ACID = A.ACID AND G.CIF_ID = E.ORGKEY AND E.PREFERREDFLAG = 'Y'" +
            " AND E.PHONEOREMAIL = 'EMAIL' " +
            " AND (A.PS_NEXT_DUE_DATE = TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS')" +
            " OR A.PS_NEXT_DUE_DATE = TO_DATE('%s 00:00:00', 'MM/DD/YYYY HH24:MI:SS'))" +
            " AND A.PS_FREQ_HLDY_STAT = 'P'";

    /** Fetches the acids of all accounts that are to get statements on <code>date</code>
     *
     * @param date The date
     * @return A list containing the account ids
     */
    public static List<AccountStatementDetail> getAllDateAccounts(final Date date){
        String sql = String.format(accountDetailSQL, getDateString(dt.format(date)));
        List<AccountStatementDetail> returned = null;

        try(Connection connection = play.db.DB.getConnection("secondary");
            PreparedStatement statement = connection.prepareStatement(sql)){
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    AccountStatementDetail return1 = new AccountStatementDetail();
                    return1.customerId = resultSet.getString(1);
                    return1.email = resultSet.getString(2);
                    return1.accountId = resultSet.getString(3);
                    return1.psFrequencyType = resultSet.getString(4);
                    return1.psNextDueDate = resultSet.getString(5);
                    returned.add(return1);
                }
            }
            return returned;
        }catch (Exception exc){
            exc.printStackTrace();
            return null;
        }
    }

    /** Fetches the acids of all accounts that are to get statements on the day before <code>date</code>
     * as well as the one before it where their next flag is set
     * @param date The date
     * @return A list containing the account ids
     */
    public static List<AccountStatementDetail> getAllNextDaysAccounts(final Date date){
        Calendar sunday = Calendar.getInstance();
        sunday.setTime(date);
        sunday.add(Calendar.DATE, -1);
        Calendar saturday = Calendar.getInstance();
        saturday.setTime(date);
        saturday.add(Calendar.DATE, -2);
        String sql = String.format(accountDetailNextSQL, getDateString(dt.format(saturday.getTime())),
                getDateString(dt.format(sunday.getTime())));
        List<AccountStatementDetail> returned = null;

        try(Connection connection = play.db.DB.getConnection("secondary");
            PreparedStatement statement = connection.prepareStatement(sql)){
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    AccountStatementDetail return1 = new AccountStatementDetail();
                    return1.customerId = resultSet.getString(1);
                    return1.email = resultSet.getString(2);
                    return1.accountId = resultSet.getString(3);
                    return1.psFrequencyType = resultSet.getString(4);
                    return1.psNextDueDate = resultSet.getString(5);
                    returned.add(return1);
                }
            }
            return returned;
        }catch (Exception exc){
            exc.printStackTrace();
            return null;
        }
    }

    /** Fetches the acids of all accounts that are to get statements on the day after <code>date</code>
     * as well as the next day where their previous flag is set
     * @param date The date
     * @return A list containing the account ids
     */
    public static List<AccountStatementDetail> getAllPreviousDaysAccounts(final Date date){
        Calendar sunday = Calendar.getInstance();
        sunday.setTime(date);
        sunday.add(Calendar.DATE, 2);
        Calendar saturday = Calendar.getInstance();
        saturday.setTime(date);
        saturday.add(Calendar.DATE, 1);

        String sql = String.format(accountDetailPreviousSQL,getDateString(dt.format(saturday.getTime())),
                getDateString(dt.format(sunday.getTime())));

        List<AccountStatementDetail> returned = null;

        try(Connection connection = play.db.DB.getConnection("secondary");
            PreparedStatement statement = connection.prepareStatement(sql)){
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    AccountStatementDetail return1 = new AccountStatementDetail();
                    return1.customerId = resultSet.getString(1);
                    return1.email = resultSet.getString(2);
                    return1.accountId = resultSet.getString(3);
                    return1.psFrequencyType = resultSet.getString(4);
                    return1.psNextDueDate = resultSet.getString(5);
                    returned.add(return1);
                }
            }
            return returned;
        }catch (Exception exc){
            exc.printStackTrace();
            return null;
        }
    }

    /** Checks if the customer requires AccountStatements daily
     *
      * @return <code>true</code> if yes
     */
    public boolean isDaily(){
        return psFrequencyType.equals("D");
    }
/** Checks if the customer requires AccountStatements monthly
 *
 * @return <code>true</code> if yes
 */
    public boolean isMonthly(){

        return psFrequencyType.equals("M");
    }

    /** Checks if the customer requires AccountStatements weekly
     *
     * @return <code>true</code> if yes
     */
    public boolean isWeekly(){
        return psFrequencyType.equals("W");
    }

    /** Checks if the customer requires AccountStatements quarterly
     *
     * @return <code>true</code> if yes
     */
    public boolean isQuarterly(){
        return psFrequencyType.equals("Q");
    }

    /** Checks if the customer requires AccountStatements halfYearly
     *
     * @return <code>true</code> if yes
     */
    public boolean isHalfYearly(){
        return psFrequencyType.equals("H");
    }

    /** Checks if the customer requires AccountStatements yearly
     *
     * @return <code>true</code> if yes
     */
    public boolean isYearly(){
        return psFrequencyType.equals("Y");
    }
}
