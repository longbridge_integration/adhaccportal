package external;

import com.avaje.ebean.Model;
import play.libs.Akka;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/** Account represents a customer Account in finacle
 */
@Entity
@Table(name = "TBAADM.GAM")
public class Account extends Model{

    @Column(name = "FORACID")
    public String accountNumber;

    @Column(name = "ACID")
    public String accountId;

    @Column(name = "ACCT_NAME")
    public String accountName;

    @Column(name = "CIF_ID")
    public String customerId;

    @Column(name = "CLR_BAL_AMT")
    public String accountBalance;

    @Column(name = "SCHM_TYPE")
    public String schemeType;

    @Column(name = "SCHM_CODE")
    public String schemeCode;

    @Column(name= "CRNCY_CODE")
    public String currencyCode;

    @Column(name="ACCT_CRNCY_CODE")
    public String accountCurrencyCode;

    public static Finder<Long, Account> find = new Finder<Long, Account>("secondary", Account.class);


    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }

    public static Account findByAccountId(String acId){
        return find.where().eq("ACID",acId).findUnique();
    }



    public String obfuscateAccNumber(){
        return "******" + accountNumber.substring(6, 9);
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber='" + accountNumber + '\'' +
                ", accountName='" + accountName + '\'' + "}";
    }
}
