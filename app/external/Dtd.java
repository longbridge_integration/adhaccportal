package external;

import com.avaje.ebean.Model;
import play.libs.Akka;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/** This represents a single Daily transaction part in finacle.
 * Created by chigozirim on 10/12/15.
 */
@Entity
@Table(name = "TBAADM.DTD")
public class Dtd extends Model{

    @Column(name = "TRAN_ID")
    public String tranId;

    @Column(name = "TRAN_DATE")
    public Date tranDate;

    @Column(name = "RCRE_TIME")
    public Date creationDate;

    @Column(name = "ACID")
    public String accountId;

    @Column(name = "CUST_ID")
    public String customerId;

    @Column(name = "PART_TRAN_TYPE")
    public String partTranType;

    @Column(name = "TRAN_AMT")
    public String amount;

    @Column(name = "TRAN_PARTICULAR")
    public String tranParticulars;

    @Column(name = "PSTD_FLG")
    public String posted;

    @Column(name = "PSTD_DATE")
    public Date postedDate;

    @Column(name = "TRAN_TYPE")
    public String tranType;

    @Column(name = "TRAN_SUB_TYPE")
    public String tranSubType;

    public static Finder<Long, Dtd> find = new Finder<Long, Dtd>("secondary", Dtd.class);

    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }





    @Override
    public String toString() {
        return String.format("DailyTransaction{tranId='%s',tranDate='%s',account id='%s',partTranType='%s', amount='%s'%ntranParticulars='%s'%n}",
                tranId, tranDate, accountId, partTranType, amount, tranParticulars);
    }
}
