package external;

import com.avaje.ebean.Model;
import play.data.format.Formats;
import play.libs.Akka;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import java.util.Date;

/*** Created by chigozirim on 11/4/16.
 */
@Entity
@Table(name = "TBAADM.DTD")
public class FinacleTranToday extends Model{

    @Column(name = "ACID")
    public String accountId;

    @Column(name = "PSTD_FLG")
    public String postedFlag;

    @Column(name = "CUST_ID")
    public String customerId;

    @Column(name = "TRAN_PARTICULAR")
    public String narration;

//    @Column(name="LCHG_TIME")
//    public Date changeTime;

    @Column(name = "PSTD_DATE")
    public Date postedDate;

    @Column(name = "TRAN_AMT")
    public String amount;

    @Column(name="PART_TRAN_TYPE")
    public String debitOrCredit;

    @Column(name = "REF_CRNCY_CODE")
    public String currencyCode;

    public static Finder<Long, FinacleTranToday> find = new Finder<Long, FinacleTranToday>("secondary", FinacleTranToday.class);


    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }


}
