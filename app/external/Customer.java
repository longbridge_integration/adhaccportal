package external;

import com.avaje.ebean.Model;
import org.apache.commons.codec.digest.Md5Crypt;
import play.Logger;
import play.Play;
import play.api.libs.json.Json;
import play.libs.Akka;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/** Entity representing a Customer in finacle.
 * Created by chigozirim on 10/12/15.
 */
@Entity
@Table(name = "CRMUSER.ACCOUNTS")
public class Customer extends Model{

    @Column(name = "ORGKEY")
    public String customerId;

    @Column(name = "CUST_LAST_NAME")
    public String lastName;

    @Column(name= "CUST_FIRST_NAME")
    public String firstName;

    @Column(name="CUST_MIDDLE_NAME")
    public String middleName;

    @Column(name = "PREFERREDNAME")
    public String preferredName;


    public static Finder<Long, Customer> find = new Finder<Long, Customer>("secondary", Customer.class);

    private static scala.concurrent.ExecutionContext getContext(){
        return  Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }

    /** Gets the first name of the Customer
     *
     * @param custId The <code>cif_id</code> of the <b>Customer</b>
     * @return
     */
    public static String fetchFirstName(String custId){ return find.where().eq("ORGKEY",custId).findUnique().lastName; }

    public static String fetchFullName(String custId){
        Customer cust = null;
        try{
            cust = find.where().eq("ORGKEY", custId).findUnique();
        }catch (NullPointerException exc){
            Logger.error("", exc);
            return "";
        }
        Logger.info("Customer name: {} {} {}", cust.firstName, cust.lastName,cust.middleName);
        return cust.firstName + " " + cust.lastName;
    }

    /** Fetch the <b>Customer</b> with the specified customer id
     *
     * @param custId <code>cif_id</code> of the specified customer
     * @return
     */
    public static Customer findByCustId(String custId){return find.where().eq("ORGKEY", custId).findUnique();}
    @Override
    public String toString() {
        return "Crmuser{" +
                "customerId='" + customerId + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        Customer customer = (Customer) obj;
        return customer.customerId.equals(this.customerId);
    }
}
